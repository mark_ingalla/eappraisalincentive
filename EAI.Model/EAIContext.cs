﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.Linq;
using System.Text;
using EAI.Domain;
using System.Web;

namespace EAI.Model
{
    public class EAIContext : DbContext
    {
        public DbSet<Position> Positions { get; set; }
        public DbSet<Department> Departments { get; set; }

        public DbSet<Employee> Employees { get; set; }
        public DbSet<User> Users { get; set; }

        public DbSet<AppraisalHeader> AppraisalHeaders { get; set; }
        public DbSet<AppraisalItem> AppraisalItems { get; set; }

        public DbSet<EmployeeAppraisal> EmployeeAppraisals { get; set; }
        public DbSet<EmployeeAppraisalItem> EmployeeAppraisalItems { get; set; }
        public DbSet<EmployeeAppraisalHeader> EmployeeAppraisalHeaders { get; set; }

        public DbSet<EmployeeBonusPointRequest> EmployeeBonusPointRequests { get; set; }
        public DbSet<EmployeeBonusPoints> EmployeeBonusPoints { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<Employee>()
                .HasOptional(e => e.ImmediateSupervisor)
                .WithMany(e => e.SupervisorChildren)
                .HasForeignKey(e => e.ImmediateSupervisorId);

            modelBuilder.Entity<Employee>()
                .HasOptional(e => e.BusinessUnitHead)
                .WithMany(e => e.BusinessUnitHeadChildren)
                .HasForeignKey(e => e.BusinessUnitHeadId);

            modelBuilder.Entity<EmployeeAppraisal>()
                .HasRequired(x => x.ImmediateSupervisor)
                .WithMany()
                .HasForeignKey(x => x.ImmediateSupervisorId).WillCascadeOnDelete(false);
            modelBuilder.Entity<EmployeeAppraisal>()
                .HasRequired(x => x.BusinessUnitHead)
                .WithMany()
                .HasForeignKey(x => x.BusinessUnitHeadId).WillCascadeOnDelete(false);

            modelBuilder.Entity<EmployeeAppraisal>()
                .HasOptional(x => x.Reviewee)
                .WithMany()
                .HasForeignKey(x => x.RevieweeId).WillCascadeOnDelete(false);

            //modelBuilder.Entity<EmployeeAppraisalItem>()
            //   .HasOptional(x => x.Header)
            //   .WithMany()
            //   .HasForeignKey(x => x.HeaderId).WillCascadeOnDelete(false);

          
        }

        public override int SaveChanges()
        {
            if (HttpContext.Current.User != null)
            {
                var currentuser = Users.FirstOrDefault(x => x.Username == HttpContext.Current.User.Identity.Name);
                foreach (var entry in ChangeTracker.Entries<IBaseModel>())
               // foreach (var entry in ChangeTracker.Entries())
                {
                    switch (entry.State)
                    {
                        case EntityState.Added:
                            entry.Entity.LastUpdatedDate = DateTime.Now;
                            if (currentuser != null)
                                entry.Entity.LastUpdatedByUserId = currentuser.UserId;
                            break;

                        case EntityState.Modified:


                            entry.Entity.LastUpdatedDate = DateTime.Now;
                            if (currentuser != null)
                                entry.Entity.LastUpdatedByUserId = currentuser.UserId;
                            break;

                        default:
                            break;

                    }
                }
            }
            try
            {
                return base.SaveChanges();
            }
            catch (DbEntityValidationException dbEx)
            {
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        Trace.TraceInformation("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                    }
                }
            }
            return 0;

        }
    }
}
