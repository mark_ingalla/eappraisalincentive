﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EAI.Domain;
using EAI.Model.DTO;

namespace EAI.Model.Contracts
{
    public interface IReportsRepository : IRepository<EmployeeAppraisal>
    {
        IList<RptQuarterlyAppraisal> QuarterlyAppraisalIncentives(ReportFilter filter);
        IList<RptBonusAdjustment> BonusAdjustments(ReportFilter filter);
        IList<RptBonusPointRequest> BonusPointRequests(ReportFilter filter);
        IList<RptBonusAdjustment> BonusPointStatus(ReportFilter filter);
        IList<RptMonthlyBonusPointCredit> MontlyCreditBonusPoints(ReportFilter filter);
    }
}
