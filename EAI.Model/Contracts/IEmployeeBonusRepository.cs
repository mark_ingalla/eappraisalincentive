﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EAI.Domain;
using EAI.Model.DTO;

namespace EAI.Model.Contracts
{
    public interface IEmployeeBonusRepository : IRepository<EmployeeBonusPoints>
    {
        IList<EmployeeBonusInfo> GetEmployeeBonusList();
        EmployeeBonusDetails GetEmployeeBonusDetail(int id);
        void AddBonusAdjustment(EmployeeBonusDetails detail);
        void SaveBonusAdjustment(EmployeeBonusDetails detail);
        void AddBonusRequest(BonusRequestDetails details);
        IList<EmployeeAppraisalInfo> GetEmployeesForMonthlyCredit(DateTime dateTime);

        IList<BonusRequestInfo> GetBonusRequests();

        int GetMaximumCashableBonusPoint(DateTime transactiondate, int employeeid);

        void ProcessMonthlyCredit(DateTime dateTime);


    }
}
