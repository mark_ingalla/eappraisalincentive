﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EAI.Domain;
using EAI.Model.DTO;

namespace EAI.Model.Contracts
{
    public interface IUserRepository : IRepository<User>
    {
        bool ValidateUser(string username, string password);
        int GetUserId(string username);
        bool ChangePassword(int userid, string newpassword, string oldpassword);
        UserDetails GetUserDetails(string username);
        void ChangeTheme(int id,string theme);

    }
}
