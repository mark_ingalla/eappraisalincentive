﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EAI.Domain;
using EAI.Model.DTO;

namespace EAI.Model.Contracts
{
    public interface IAppraisalRepository : IRepository<AppraisalItem>
    {
        IList<AppraisalHeaderDetails> GetAppraisalHeaders();
        int AddAppraisalHeader(AppraisalHeaderDetails appraisalHeaderDetails);
        AppraisalHeaderDetails GetAppraisalHeader(int id);
        void SaveAppraisalHeader(AppraisalHeaderDetails details);

        
        IList<AppraisalItemDetails> GetAppraisalItemsById(int id);
        IList<AppraisalItemDetails> GetAppraisalItems();
        AppraisalItemDetails GetAppraisalItem(int id);
        void SaveAppraisalItem(AppraisalItemDetails details);
         
        int AddAppraisalItem(AppraisalItemDetails appraisalItemDetails);

        void EnableDisableHeader(int id);
        void EnableDisableItem(int id);
    }
}
