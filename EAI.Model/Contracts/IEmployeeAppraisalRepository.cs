﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EAI.Domain;
using EAI.Model.DTO;
using EAI.Model.Lookup;

namespace EAI.Model.Contracts
{
    public interface IEmployeeAppraisalRepository : IRepository<EmployeeAppraisal>
    {
        IList<EmployeeAppraisalDetails> GetEmployeeAppraisalsBySupervisorId(int id);
        IList<EmployeeAppraisalInfo> GetEmployeeAppraisalsInfoBySupervisorId(int id);
        EmployeeAppraisalDetails GetEmployeeAppraisal(int id);
        void AddEmployeeAppraisal(EmployeeAppraisalDetails details);
        void SaveEmployeeAppraisal(EmployeeAppraisalDetails details);

        IList<EmployeeAppraisalItemDetails> GetAppraisalItems();
        IList<EmployeeAppraisalItemDetails> GetEmployeeAppraisalItems(int id);

        void UpdateStatus(EmployeeAppraisalDetails appraisalDetails, EmployeeAppraisalStatus status);

        int GetActiveUserForEmployeeAppraisal(int id);
        IList<EmployeeAppraisalDetails> GetEmployeeAppraisalNeedAction(int employeeid);

    }
}
