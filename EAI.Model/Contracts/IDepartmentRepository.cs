﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EAI.Domain;
using EAI.Model.DTO;

namespace EAI.Model.Contracts
{
    public interface IDepartmentRepository : IRepository<Department>
    {
        IList<DepartmentDetails> GetAllDepartments();
        int AddDepartment(DepartmentDetails positionDetails);
        DepartmentDetails GetDepartmentDetail(int id);
        void Save(DepartmentDetails details);
        void Delete(int id);
    }
}
