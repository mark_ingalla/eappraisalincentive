﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EAI.Domain;
using EAI.Model.DTO;

namespace EAI.Model.Contracts
{
    public interface IPositionRepository :  IRepository<Position>
    {
        IList<PositionDetails> GetAllPositions();
        int AddPosition(PositionDetails positionDetails);
        PositionDetails GetPositionDetail(int id);
        void Save(PositionDetails details);
        void Delete(int id);
    }
}
