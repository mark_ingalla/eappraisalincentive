﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EAI.Domain;
using EAI.Model.DTO;

namespace EAI.Model.Contracts
{
    public interface IEmployeeRepository : IRepository<Employee>
    {

        IList<EmployeeDetails> GetEmployees();
        IList<EmployeeListInfo> GetEmployeesUnderSupervisor(int id);
        EmployeeDetails GetEmployee(int id);
        EmployeeListInfo GetEmployeeInfo(int id);
        EmployeeListInfo GetEmployeeByUserName(string username);
        EmployeeDetails GetEmployeeByNo(string empno);
        EmployeeDetails GetEmployeeByIDandNo(int id,string empno);
        IList<EmployeeListInfo> GetEmployeeAllList(bool? active);
        IList<EmployeeListInfo> GetEmployeeList();
        IList<EmployeeListInfo> GetActiveEmployees();
        void AddEmployee(EmployeeDetails employeeDetails);
        void UpdateEmployee(EmployeeDetails employeeDetails);
        int AddUser(UserDetails userDetails);


        void Inactivate(int id);
        void Activate(int id);

    }
}
