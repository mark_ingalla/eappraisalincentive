﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EAI.Model.DTO
{
    public class ReportFilter
    {
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
    }
}
