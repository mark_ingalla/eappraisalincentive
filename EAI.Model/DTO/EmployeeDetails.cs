﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace EAI.Model.DTO
{

    public class EmployeeDetails
    {
        [ScaffoldColumn(false)]
        public int ID { get; set; }

        public int UserId { get; set; }

        [Required]
        [DisplayName("Employee No.")]
        public string EmployeeNo { get; set; }

        [Required]
        [DisplayName("Employee Name")]
        public string EmployeeName { get; set; }

        //[Required]
        //[DisplayName("First Name")]
        //public string FirstName { get; set; }

        //[Required]
        //[DisplayName("Last Name")]
        //public string LastName { get; set; }

        [Required]
        [DisplayName("Position")]
        public int PositionId { get; set; }

        [Required]
        [DisplayName("Department")]
        public int DepartmentId { get; set; }

        [Required]
        [DisplayName("Email")]
        public string EmailAddress { get; set; }


        [DisplayName("Immediate Supervisor")]
        public int? ImmediateSupervisorId { get; set; }

        [DisplayName("Business Unit Head")]
        public int? BusinessUnitHeadId { get; set; }

        [DisplayName("Date Joined")]
        [DataType(DataType.Date)]
        public DateTime DateJoined {get;set;}

        [DisplayName("Date Confirmed")]
        [DataType(DataType.Date)]
        public DateTime? DateConfirmed { get; set; }

        [DisplayName("Date Resigned")]
        [DataType(DataType.Date)]
        public DateTime? DateResigned { get; set; }

        public string Remarks { get; set; }

        public UserDetails User { get; set; }

        public string Grade { get; set; }

        public bool IsActive { get; set; }
    }
}