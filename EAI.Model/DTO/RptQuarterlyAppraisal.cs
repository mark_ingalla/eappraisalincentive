﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EAI.Model.DTO
{
    public class RptQuarterlyAppraisal
    {
        public string EmployeeNo { get; set; }
        public string EmployeeName { get; set; }
        public string Position { get; set; }
        public string Department { get; set; }
        public DateTime DateJoined { get; set; }
        public DateTime? DateConfirmed { get; set; }
        public int OverallRating { get; set; }
    }
}
