﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EAI.Model.DTO
{
    public class RptBonusAdjustment
    {
        public string EmployeeNo { get; set; }
        public string EmployeeName { get; set; }
        public string TransactionNo { get; set; }        
        public DateTime TransactionDate { get; set; }
        public decimal CreditDebitPoints { get; set; }
        public string TransactionDescription { get; set; }
        public decimal BonusPointBalance { get; set; }
        public string UpdatedBy { get; set; }
    }
}
