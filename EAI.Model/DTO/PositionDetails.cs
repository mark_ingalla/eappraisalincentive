﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace EAI.Model.DTO
{
    public class PositionDetails
    {
        [ScaffoldColumn(false)]
        public int ID { get; set; }

        public string Name { get; set; }
    }
}
