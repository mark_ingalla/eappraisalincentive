﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EAI.Model.DTO
{
    public class EmployeeBonusDetails
    {
        public int ID { get; set; }
        public int EmployeeId { get; set; }
        public string TransactionNo { get; set; }
        public DateTime TransactionDate { get; set; }
        public string Description { get; set; }
        public decimal BonusPoint { get; set; }
    }
}
