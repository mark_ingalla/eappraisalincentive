﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EAI.Model.DTO
{
    public class RptBonusPointRequest
    {
        public string EmployeeNo { get; set; }
        public string EmployeeName { get; set; }
        public int MonthsofService { get; set; }
        public string TransactionNo { get; set; }
        public DateTime TransactionDate { get; set; }
        public int MaximumCashableBonusPoint { get; set; }
        public int CashoutBonusPoint { get; set; }
        
    }
}
