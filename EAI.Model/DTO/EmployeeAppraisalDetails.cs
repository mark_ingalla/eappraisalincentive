﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EAI.Model.DTO
{
    public class EmployeeAppraisalDetails
    {
        public int ID { get; set; }
        public int EmployeeId { get; set; }
        public string EmployeeName { get; set; }
        public string EmployeeComment { get; set; }
        public DateTime? EmployeeSignDate { get; set; }
        public int SupervisorId { get; set; }
        public string SupervisorComment { get; set; }
        public DateTime? SupervisorDate { get; set; }
        public int BusinessUnitHeadId { get; set; }
        public string BusinessUnitHeadComment { get; set; }
        public DateTime? BusinessUnitHeadDate { get; set; }
        public DateTime AppraisalDate { get; set; }
        public int OverallRating { get; set; }
        public int Status { get; set; }
        public string StatusName { get; set; }
        public string TransactionNo { get; set; }

        public string ApprovalComment { get; set; }

        public IList<EmployeeAppraisalItemDetails> EmployeeAppraisalItems { get; set; }

    }
}
