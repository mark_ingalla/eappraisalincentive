﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EAI.Model.DTO
{
    public class EmployeeAppraisalInfo
    {
        public int ID { get; set; }
        public string EmployeeName { get; set; }
        public string Department { get; set; }
        public string Position { get; set; }
        public DateTime AppraisalDate { get; set; }
        public int Rating { get; set; }
    }
}
