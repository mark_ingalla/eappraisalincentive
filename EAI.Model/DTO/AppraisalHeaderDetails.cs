﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace EAI.Model.DTO
{
    public class AppraisalHeaderDetails
    {
        [ScaffoldColumn(false)]
        public int Id { get; set; }
        [DataType(DataType.MultilineText)]
        [Display(Name="Name")]
        public string Description { get; set; }
        [Display(Name="Active")]
        public bool IsActive { get; set; }
        public int? Rank { get; set; }
    }
}
