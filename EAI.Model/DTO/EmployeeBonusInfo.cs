﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EAI.Model.DTO
{
    public class EmployeeBonusInfo
    {
        public int ID { get; set; }
        public string TransactionNo { get; set; }
        public DateTime TransactionDate { get; set; }
        public string EmployeeName { get; set; }
        public decimal BonusPoints { get; set; }
    }
}
