﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EAI.Model.DTO
{
    public class RptMonthlyBonusPointCredit
    {
        public string EmployeeNo { get; set; }
        public string EmployeeName { get; set; }
        public string TransactionNo { get; set; }
        public DateTime TransactionDate { get; set; }
        public int LastQuarterOverallRating { get; set; }        
        public decimal BonusPointBalance { get; set; }
        
    }
}
