﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EAI.Model.DTO
{
    public class EmployeeAppraisalItemInfo
    {
        public int ID { get; set; }
        public string Description { get; set; }
    }
}
