﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EAI.Model.DTO
{
    public class EmployeeAppraisalMailDTO
    {
        public int Id { get; set; }
        public string ToEmail { get; set; }
        public string ToName { get; set; }
        public string Url { get; set; }
        public string AppraisalNo { get; set; }
        public string AppraisalEmployeeName { get; set; }
    }
}
