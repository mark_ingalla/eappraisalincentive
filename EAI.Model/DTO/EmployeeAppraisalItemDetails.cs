﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EAI.Model.DTO
{
    public class EmployeeAppraisalItemDetails
    {
        public EmployeeAppraisalItemInfo ItemInfo { get; set; }
        public string Description { get; set; }
        public int AppraisalId { get; set; }
        public int Rating { get; set; }
        public string Comments { get; set; }
        public int ParentId { get; set; }
        public IList<EmployeeAppraisalItemDetails> Items { get; set; }
    }
}
