﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace EAI.Model.DTO
{
    public class AppraisalItemDetails
    {
        [ScaffoldColumn(false)]
        public int Id { get; set; }
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }
         [ScaffoldColumn(false)]
        public int HeaderId { get; set; }
        public AppraisalHeaderDetails Header { get; set; }
        [Display(Name = "Active")]
        public bool IsActive { get; set; }
        public int? Rank { get; set; }
    }
}
