﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EAI.Model.DTO
{
    public class EmployeeListInfo
    {
        public int Id { get; set; }
        public string EmployeeNo { get; set; }
        public string Name { get; set; }
        public string Position { get; set; }
        public string Department { get; set; }
        public DateTime DateJoined { get; set; }
        public DateTime? DateResigned{ get; set; }
        public DateTime? DateConfirmed { get; set; }
        public bool IsActive { get; set; }
        public string Grade { get; set; }
        public decimal BonusPointBalance { get; set; }
        public string UserName { get; set; }
        public string Role { get; set; }
    }
}
