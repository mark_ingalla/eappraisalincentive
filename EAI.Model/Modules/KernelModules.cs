﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using EAI.Model.Contracts;
using EAI.Model.Repositories;
using Ninject.Modules;

namespace EAI.Model.Modules
{
    public class EntityModules : NinjectModule
    {
        public override void Load()
        {
            Bind<DbContext>().To<EAIContext>();
            Bind<IUserRepository>().To<UserRepository>();
            Bind<IEmployeeAppraisalRepository>().To<EmployeeAppraisalRepository>();
            Bind<IEmployeeBonusRepository>().To<EmployeeBonusRepository>();
            Bind<IAppraisalRepository>().To<AppraisalRepository>();
            Bind<IPositionRepository>().To<PositionRepository>();
            Bind<IDepartmentRepository>().To<DepartmentRepository>();
            Bind<IEmployeeRepository>().To<EmployeeRepository>();
            Bind<IReportsRepository>().To<ReportsRepository>();
        }
    }

    
}
