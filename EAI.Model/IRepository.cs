﻿using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace EAI.Model
{
    public interface IRepository<T> where T : class, new()
    {
        void CommitChanges();
        void Delete(Expression<Func<T, bool>> expression);
        void Delete(T item);
        void DeleteAll() ;
        T Single(Expression<Func<T, bool>> expression);
        void Add(T item);
        void Add(IEnumerable<T> items);
        void Update(T item);
    }
}
