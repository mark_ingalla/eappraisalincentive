﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using EAI.Domain;
using EAI.Model.Contracts;
using EAI.Model.DTO;

namespace EAI.Model.Repositories
{
    public class PositionRepository : Repository<Position>, IPositionRepository
    {

        private readonly EAIContext _context;

        public PositionRepository(DbContext ctx) : base(ctx)
        {
            _context = (EAIContext) ctx;
        }

        public IList<PositionDetails> GetAllPositions()
        {
            var query = _context.Positions.Select(x => new PositionDetails
                                                           {
                                                               ID = x.PositionId,
                                                               Name = x.Name
                                                           });
            return query.ToList();
        }

        public int AddPosition(PositionDetails positionDetails)
        {
            var position = new Position
                               {
                                   Name = positionDetails.Name,
                                   LastUpdatedDate = DateTime.Now
                               };
            _context.Positions.Add(position);
            _context.SaveChanges();

            return position.PositionId;
        }

        public PositionDetails GetPositionDetail(int id)
        {
            var position = _context.Positions.FirstOrDefault(x => x.PositionId == id);
            return new PositionDetails
                       {
                           ID = position.PositionId,
                           Name = position.Name
                       };
        }

        public void Save(PositionDetails details)
        {
            var position = _context.Positions.FirstOrDefault(x=>x.PositionId == details.ID);
            position.Name = details.Name;
            _context.SaveChanges();


        }

        public void Delete(int id)
        {
            var position = _context.Positions.FirstOrDefault(x => x.PositionId == id);
            _context.Positions.Remove(position);
            _context.SaveChanges();
        }
    }
}
