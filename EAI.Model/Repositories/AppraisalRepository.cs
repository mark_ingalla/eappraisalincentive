﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using EAI.Domain;
using EAI.Model.Contracts;
using EAI.Model.DTO;

namespace EAI.Model.Repositories
{
    public class AppraisalRepository : Repository<AppraisalItem>,IAppraisalRepository
    {
        private readonly EAIContext _context;

        public AppraisalRepository(DbContext ctx) : base(ctx)
        {
            _context = (EAIContext)ctx;
        }

        #region Implementation of IAppraisalRepository

        public IList<AppraisalHeaderDetails> GetAppraisalHeaders()
        {
            var query = _context.AppraisalHeaders.OrderBy(x=>x.Rank).Select(x => new AppraisalHeaderDetails
                                                                  {
                                                                       Id =x.AppraisalHeaderId,
                                                                       Description = x.Name,
                                                                       IsActive = x.IsActive,
                                                                       Rank = x.Rank
                                                                  }).ToList();
            return query;
        }

        public int AddAppraisalHeader(AppraisalHeaderDetails appraisalHeaderDetails)
        {
            var appraisalheader = new AppraisalHeader
                                      {
                                          Name = appraisalHeaderDetails.Description,
                                          LastUpdatedDate = DateTime.Now,
                                          IsActive = true,
                                          Rank = appraisalHeaderDetails.Rank
                                      };
            _context.AppraisalHeaders.Add(appraisalheader);
            _context.SaveChanges();
            return appraisalheader.AppraisalHeaderId;
        }

        public AppraisalHeaderDetails GetAppraisalHeader(int id)
        {
            var appraisalHeader = _context.AppraisalHeaders.Find(id);
            return new AppraisalHeaderDetails
                       {
                           Id = appraisalHeader.AppraisalHeaderId,
                           IsActive = appraisalHeader.IsActive,
                           Description = appraisalHeader.Name,
                           Rank = appraisalHeader.Rank
                       };
        }

        public void SaveAppraisalHeader(AppraisalHeaderDetails details)
        {
            var appraisalHeader = _context.AppraisalHeaders.Find(details.Id);
            appraisalHeader.Name = details.Description;
            appraisalHeader.IsActive = details.IsActive;
            appraisalHeader.Rank = details.Rank;
            _context.SaveChanges();
        }

        public AppraisalItemDetails GetAppraisalItem(int id)
        {
            var appraisalItem = _context.AppraisalItems.Find(id);
            return new AppraisalItemDetails
                       {
                           Description = appraisalItem.Description,
                           HeaderId = appraisalItem.AppraisalHeaderId,
                           Id = appraisalItem.AppraisalItemId,
                           IsActive = appraisalItem.IsActive
                       };
        }

        public void SaveAppraisalItem(AppraisalItemDetails details)
        {
            var appraisalItem = _context.AppraisalItems.Find(details.Id);
            appraisalItem.Description = details.Description;
            appraisalItem.IsActive = details.IsActive;
            appraisalItem.Rank = details.Rank;
            _context.SaveChanges();
        }

        public IList<AppraisalItemDetails> GetAppraisalItemsById(int id)
        {
            var query = _context.AppraisalItems.Include(x => x.Header).Where(x=>x.Header.AppraisalHeaderId == id)
                .OrderBy(x=>x.Rank)
                .Select(x => new AppraisalItemDetails
            {
                Id = x.AppraisalItemId,
                Description = x.Description,
                IsActive = x.IsActive,
                Rank = x.Rank,
                Header = new AppraisalHeaderDetails
                {
                    Id = x.Header.AppraisalHeaderId,
                    Description = x.Header.Name,
                    IsActive = x.IsActive,
                    Rank = x.Header.Rank
                }
            }).ToList();
            return query;
        }

        public IList<AppraisalItemDetails> GetAppraisalItems()
        {
            var query = _context.AppraisalItems.Include(x=>x.Header).OrderBy(x=>x.Rank).Select(x => new AppraisalItemDetails
            {
                Id = x.AppraisalItemId,
                Description  = x.Description,
                IsActive = x.IsActive,
                Header = new AppraisalHeaderDetails
                             {
                                 Id = x.Header.AppraisalHeaderId,
                                 Description = x.Header.Name,
                                 IsActive = x.IsActive,
                                 Rank = x.Rank
                             }
            }).ToList();
            return query;
        }

        public int AddAppraisalItem(AppraisalItemDetails appraisalItemDetails)
        {

            var header = _context.AppraisalHeaders.Find(appraisalItemDetails.HeaderId);


            var appraisalitem = new AppraisalItem
                                    {
                                        Description = appraisalItemDetails.Description,
                                        Header = header,
                                        LastUpdatedDate = DateTime.Now,
                                        IsActive = true,
                                        Rank = appraisalItemDetails.Rank
                                    };
            _context.AppraisalItems.Add(appraisalitem);
            _context.SaveChanges();
            return header.AppraisalHeaderId;
        }

        public void EnableDisableHeader(int id)
        {
            var header = _context.AppraisalHeaders.Find(id);
            header.IsActive = !header.IsActive;
            _context.SaveChanges();
        }

        public void EnableDisableItem(int id)
        {
            var item = _context.AppraisalItems.Find(id);
            item.IsActive = !item.IsActive;
            _context.SaveChanges();
        }

        #endregion
    }
}
