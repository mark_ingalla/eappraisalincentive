﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Text;
using EAI.Domain;
using EAI.Model.Contracts;
using EAI.Model.DTO;
using EAI.Model.Lookup;

namespace EAI.Model.Repositories
{
    public class EmployeeAppraisalRepository : Repository<EmployeeAppraisal>, IEmployeeAppraisalRepository
    {
        private readonly EAIContext _ctx;
        private const int OverAllAppraisalId = 10000;

        public EmployeeAppraisalRepository(DbContext ctx) : base(ctx)
        {
            _ctx = (EAIContext) ctx;
        }

        #region Implementation of IEmployeeAppraisalRepository

        public IList<EmployeeAppraisalDetails> GetEmployeeAppraisalsBySupervisorId(int id)
        {
            var query = _ctx.EmployeeAppraisals.Include(c => c.Employee).Where(c => c.ImmediateSupervisorId == id)
                .ToList().Select(c => new EmployeeAppraisalDetails
                                          {
                                              AppraisalDate = c.AppraisalDate,
                                              BusinessUnitHeadId = c.BusinessUnitHead.EmployeeId,
                                              //EmployeeAppraisalItems = c.AppraisalItems.Select(item=>new EmployeeAppraisalItemDetails
                                              //                                                           {
                                                                                                              
                                              //                                                           }).ToList(),
                                              EmployeeId = c.Employee.EmployeeId,
                                              ID = c.EmployeeAppraisalId,
                                              SupervisorId = c.ImmediateSupervisor.EmployeeId,
                                              OverallRating = c.OverallRating
                                              
                                              
                                          });
            return query.ToList();
        }

        public IList<EmployeeAppraisalInfo> GetEmployeeAppraisalsInfoBySupervisorId(int id)
        {
            var query = _ctx.EmployeeAppraisals.Include(c => c.Employee).Where(c => c.ImmediateSupervisorId == id || c.BusinessUnitHeadId == id)
               .ToList().Select(c => new EmployeeAppraisalInfo
               {
                   AppraisalDate = c.AppraisalDate,
                   Department = c.Employee.Department.Name,
                   EmployeeName = c.Employee.EmployeeName,
                   ID = c.EmployeeAppraisalId,
                   Position = c.Employee.Position.Name

               });
            return query.ToList();
        }

        public EmployeeAppraisalDetails GetEmployeeAppraisal(int id)
        {
            var employeeAppraisal = _ctx.EmployeeAppraisals.Find(id);
            return new EmployeeAppraisalDetails
                       {
                           ID = employeeAppraisal.EmployeeAppraisalId,
                           AppraisalDate = employeeAppraisal.AppraisalDate,
                           EmployeeId = employeeAppraisal.EmployeeId,
                           OverallRating = employeeAppraisal.OverallRating,
                           TransactionNo = employeeAppraisal.AppraisalNo,
                           Status = employeeAppraisal.Status
                       };
        }

        public void AddEmployeeAppraisal(EmployeeAppraisalDetails details)
        {

            var employee = _ctx.Employees.Find(details.EmployeeId);

            //var businesshead = _ctx.Employees.Find(details.BusinessUnitHeadId);
            //var supervisor = _ctx.Employees.Find(details.SupervisorId);

            //find existing appraisal and produce error

            var quartermonths = new[] { 10, 11, 12 };

            switch (details.AppraisalDate.Month)
            {
                case 1:
                case 2:
                case 3:
                    quartermonths = new[] {1, 2, 3};
                    break;
                case 4:
                case 5:
                case 6:
                    quartermonths = new[] { 4,5, 6 };
                    break;
                case 7:
                case 8:
                case 9:
                    quartermonths = new[] { 7, 8, 9 };
                    break;

                default:
                    break;
            }

            var dbemployeeappraisal =
                _ctx.EmployeeAppraisals.ToList().Where(
                    x => x.EmployeeId == details.EmployeeId && quartermonths.Contains(x.AppraisalDate.Month)
                         && x.AppraisalDate.Year == details.AppraisalDate.Year);

            if (dbemployeeappraisal.Count() > 0)
            {
                throw new ValidationException("Employee already have an appraisal for this quarter");
            }
                

            var employeeappraisal = new EmployeeAppraisal
                                        {
                                            AppraisalDate = details.AppraisalDate,
                                            //auto generate
                                            AppraisalNo = "0000",
                                            //BusinessUnitHead = businesshead,
                                            //ImmediateSupervisor = supervisor,
                                            BusinessUnitHeadId = details.BusinessUnitHeadId,
                                            ImmediateSupervisorId = details.SupervisorId,
                                            Employee = employee,
                                            LastUpdatedDate = DateTime.Now,
                                            OverallRating = details.OverallRating
                                        };


            if (employeeappraisal.AppraisalHeaders == null)
                employeeappraisal.AppraisalHeaders = new List<EmployeeAppraisalHeader>();
            foreach (var appraisalheader in details.EmployeeAppraisalItems)
            {
                var overall = appraisalheader.Items.FirstOrDefault(x => x.AppraisalId == OverAllAppraisalId);
                var employeeappraisalheader = new EmployeeAppraisalHeader
                                                  {
                                                      AppraisalHeaderId = overall.ParentId,
                                                      Comments = overall.Comments,
                                                      Rating = overall.Rating,
                                                      LastUpdatedDate = DateTime.Now,

                                                  };
                if (employeeappraisalheader.AppraisalItems == null)
                    employeeappraisalheader.AppraisalItems = new List<EmployeeAppraisalItem>();


                foreach (var appraisalitem in appraisalheader.Items.Where(x => x.AppraisalId != OverAllAppraisalId))
                {
                    var employeeappraisalitem = new EmployeeAppraisalItem
                                                    {
                                                        AppraisalItemId = appraisalitem.AppraisalId,
                                                        Comments = appraisalitem.Comments,
                                                        Rating = appraisalitem.Rating,
                                                        LastUpdatedDate = DateTime.Now
                                                    };
                    employeeappraisalheader.AppraisalItems.Add(employeeappraisalitem);

                }

                employeeappraisal.AppraisalHeaders.Add(employeeappraisalheader);

            }

            _ctx.EmployeeAppraisals.Add(employeeappraisal);
            _ctx.SaveChanges();
            employeeappraisal.AppraisalNo = employeeappraisal.EmployeeAppraisalId.ToString("00000");
            _ctx.SaveChanges();

        }




        public void SaveEmployeeAppraisal(EmployeeAppraisalDetails details)
        {
            var employeeappraisal = _ctx.EmployeeAppraisals.Find(details.ID);
            employeeappraisal.OverallRating = details.OverallRating;
            employeeappraisal.AppraisalDate = details.AppraisalDate;

            foreach (var appraisalheader in details.EmployeeAppraisalItems)
            {
                var appraisalheader1 = appraisalheader.Items.FirstOrDefault(x=>x.AppraisalId == OverAllAppraisalId);
                var dbAppraisalheader =
                    employeeappraisal.AppraisalHeaders.FirstOrDefault(
                        x => x.AppraisalHeaderId == appraisalheader1.ParentId);
               

                foreach (var appraisalitems in appraisalheader.Items.Where(x=>x.AppraisalId != OverAllAppraisalId))
                {
                    var appraisalitems1 = appraisalitems;
                    var dbAppraisalItem =
                        dbAppraisalheader.AppraisalItems.FirstOrDefault(
                            x => x.AppraisalItemId == appraisalitems1.AppraisalId);
                    if (dbAppraisalItem != null)
                    {
                        dbAppraisalItem.Comments = appraisalitems1.Comments;
                        dbAppraisalItem.Rating = appraisalitems1.Rating;

                    }

                }

                if (dbAppraisalheader == null) continue;
                dbAppraisalheader.Comments = appraisalheader1.Comments;
                dbAppraisalheader.Rating = appraisalheader1.Rating;
                
            }
          
            _ctx.SaveChanges();


        }

        public IList<EmployeeAppraisalItemDetails> GetAppraisalItems()
        {
            //var query = _ctx.AppraisalHeaders.Include(x => x.AppraisalItems).Where(x=>x.IsActive).OrderBy(x=>x.Rank)
            //    .ToList().Select(x => new EmployeeAppraisalItemDetails
            //                              {
            //                                  AppraisalId = x.AppraisalHeaderId,
            //                                  Description = "[Overall] " + x.Name,
            //                                  Items = x.AppraisalItems.Where(y=>y.IsActive).Select(y => new EmployeeAppraisalItemDetails
            //                                                                           {
            //                                                                               AppraisalId =
            //                                                                                   y.AppraisalItemId,
            //                                                                               Description = y.Description,
            //                                                                               ParentId = x.AppraisalHeaderId
            //                                                                           }).ToList()
            //                              });

            

            var query = _ctx.AppraisalHeaders.Include(x => x.AppraisalItems).Where(x => x.IsActive).OrderBy(x => x.Rank)
               .ToList().Select(x => new EmployeeAppraisalItemDetails
               {
                   AppraisalId = x.AppraisalHeaderId,
                   Description = x.Name,
                   Items = x.AppraisalItems.Where(y => y.IsActive).Select(y => new EmployeeAppraisalItemDetails
                   {
                       AppraisalId =
                           y.AppraisalItemId,
                       Description = y.Description,
                       ParentId = x.AppraisalHeaderId
                   }).ToList()
               });
            var myquery = query.ToList();
            foreach (var item in query.ToList())
            {
                var header = _ctx.AppraisalHeaders.Find(item.AppraisalId);
                var overallheaders = new EmployeeAppraisalItemDetails
                                         {
                                             AppraisalId = 10000,
                                             Description = "[OVERALL] " + header.Name,
                                             ParentId = header.AppraisalHeaderId
                                         };
                //item.Items.Add(overallheaders);
                EmployeeAppraisalItemDetails item1 = item;
                var itemquery = myquery.Find(x => x.AppraisalId == item1.AppraisalId);
                
                itemquery.Items.Add(overallheaders);
            }

            return myquery;
            //return query.ToList();
        }

        public IList<EmployeeAppraisalItemDetails> GetEmployeeAppraisalItems(int id)
        {
            var query =
                _ctx.EmployeeAppraisalHeaders.Include(x => x.AppraisalItems).Include(x=>x.AppraisalHeader).Include(x => x.EmployeeAppraisal).Where(
                    x => x.EmployeeAppraisal.EmployeeAppraisalId == id).OrderBy(x=>x.AppraisalHeader.Rank).ToList().Select(
                        x => new EmployeeAppraisalItemDetails
                                 {
                                     AppraisalId = x.AppraisalHeaderId,
                                   //  Description = "[Overall] " + x.AppraisalHeader.Name, 
                                     Description = x.AppraisalHeader.Name,
                                     //Comments = x.Comments,
                                     //Rating = x.Rating,
                                     Items = x.AppraisalItems.OrderBy(y=>y.AppraisalItem.Rank).Select(y=>new EmployeeAppraisalItemDetails
                                                                            {
                                                                                AppraisalId = y.AppraisalItemId,
                                                                                Description = y.AppraisalItem.Description,
                                                                                Comments = y.Comments,
                                                                                ParentId = y.Header.AppraisalHeaderId,
                                                                                Rating = y.Rating
                                                                            }).ToList()
                                 });


            var myquery = query.ToList();
            foreach (var item in query.ToList())
            {
                //var header = _ctx.AppraisalHeaders.Find(item.AppraisalId);
                EmployeeAppraisalItemDetails item2 = item;
                var header =
                    _ctx.EmployeeAppraisalHeaders.FirstOrDefault(
                        x => x.EmployeeAppraisal.EmployeeAppraisalId == id && x.AppraisalHeaderId == item2.AppraisalId);
                var overallheaders = new EmployeeAppraisalItemDetails
                {
                    AppraisalId = OverAllAppraisalId,
                    Description = "[OVERALL] " + header.AppraisalHeader.Name,
                    ParentId = header.AppraisalHeaderId,
                    Rating = header.Rating,
                    Comments = header.Comments
                };
                //item.Items.Add(overallheaders);
                EmployeeAppraisalItemDetails item1 = item;
                var itemquery = myquery.Find(x => x.AppraisalId == item1.AppraisalId);

                itemquery.Items.Add(overallheaders);
            }

            return myquery;
            //return query.ToList();

        }

        public void UpdateStatus(EmployeeAppraisalDetails appraisalDetails, EmployeeAppraisalStatus status)
        {
            var employeeappraisal = _ctx.EmployeeAppraisals.Find(appraisalDetails.ID);
           
           
            
            switch (status)
            {
                case EmployeeAppraisalStatus.PendingStaffAcknowledgement:
                    employeeappraisal.RevieweeSignDate = DateTime.Now;
                    employeeappraisal.RevieweeComment = appraisalDetails.EmployeeComment;
                    break;
                case EmployeeAppraisalStatus.PendingSupervisorApproval:
                    employeeappraisal.ImmediateSupervisorSignDate = DateTime.Now;
                    employeeappraisal.ImmediateSupervisorComment = appraisalDetails.SupervisorComment;
                    break;
                case EmployeeAppraisalStatus.PendingBusinessHeadApproval:
                    employeeappraisal.BusinessUnitHeadSignDate = DateTime.Now;
                    employeeappraisal.BusinessUnitHeadComment = appraisalDetails.BusinessUnitHeadComment;
                    break;
                default:
                    break;
            }
            employeeappraisal.Status = (int)status;

            

            _ctx.SaveChanges();
        }

        public int GetActiveUserForEmployeeAppraisal(int id)
        {
            var employeeAppraisal = _ctx.EmployeeAppraisals.Find(id);

            switch (employeeAppraisal.Status)
            {
                case (int) EmployeeAppraisalStatus.PendingStaffAcknowledgement:
                    return employeeAppraisal.EmployeeId;
                case (int)EmployeeAppraisalStatus.PendingSupervisorApproval:
                    return employeeAppraisal.ImmediateSupervisorId;
                case (int)EmployeeAppraisalStatus.PendingBusinessHeadApproval:
                    return employeeAppraisal.BusinessUnitHeadId;
                default:
                    break;
            }
            return 0;
        }

        public IList<EmployeeAppraisalDetails> GetEmployeeAppraisalNeedAction(int employeeid)
        {
            
            var employeeAppraisalDetails = _ctx.EmployeeAppraisals.ToList()
                        .Where(
                            x =>
                            x.EmployeeId == employeeid &&
                            x.Status == (int) EmployeeAppraisalStatus.PendingStaffAcknowledgement
                            || (x.ImmediateSupervisorId == employeeid && x.Status == (int) EmployeeAppraisalStatus.PendingSupervisorApproval)
                            || (x.BusinessUnitHeadId == employeeid && x.Status == (int) EmployeeAppraisalStatus.PendingBusinessHeadApproval))
                        .Select(x => new EmployeeAppraisalDetails
                                         {
                                             ID = x.EmployeeAppraisalId,
                                             TransactionNo = x.AppraisalNo,
                                             AppraisalDate = x.AppraisalDate,
                                             OverallRating = x.OverallRating,
                                             EmployeeName = x.Employee.EmployeeName,
                                             StatusName = Enum.GetName(typeof(EmployeeAppraisalStatus),x.Status)
                                         }).ToList();
                
            
            return employeeAppraisalDetails;

        }

        #endregion
    }
}
