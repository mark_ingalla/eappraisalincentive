﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using EAI.Domain;
using EAI.Model.Contracts;
using EAI.Model.DTO;
using EAI.Model.Lookup;

namespace EAI.Model.Repositories
{
    public class EmployeeRepository : Repository<Employee>,IEmployeeRepository
    {
        private readonly EAIContext _ctx;

        public EmployeeRepository(DbContext ctx) : base(ctx)
        {
            _ctx = (EAIContext) ctx;
        }

        public IList<EmployeeDetails> GetEmployees()
        {
            var query = _ctx.Employees.Where(x => x.IsActive).ToList();
            return query.Select(x=>new EmployeeDetails
                                       {
                                           
                                           ID = x.EmployeeId,
                                           EmployeeName = x.EmployeeName,
                                           DateJoined = x.DateJoined
                                       }).ToList();
        }

        public IList<EmployeeListInfo> GetActiveEmployees()
        {
            var query = _ctx.Employees.Include(e => e.Position).Include(e => e.Department).ToList().Where(e=>!e.DateResigned.HasValue && e.IsActive);
            return query.Select(x => new EmployeeListInfo
            {
                Id = x.EmployeeId,
                EmployeeNo = x.EmployeeNo,
                Name = x.EmployeeName,
                Position = x.Position.Name,
                Department = x.Department.Name,
                DateJoined = x.DateJoined
            }).ToList();
        }

        public IList<EmployeeListInfo> GetEmployeesUnderSupervisor(int id)
        {
            var query = _ctx.Employees.Where(e=>e.ImmediateSupervisorId == id && e.IsActive).ToList();
            return query.Select(x => new EmployeeListInfo
            {
                Id = x.EmployeeId,
                EmployeeNo = x.EmployeeNo,
                Name = x.EmployeeName,
                Position = x.Position.Name,
                Department = x.Department.Name,
                DateJoined = x.DateJoined
            }).ToList();
        }

       

        public EmployeeDetails GetEmployee(int id)
        {
            var employee = _ctx.Employees.Include(x => x.User).FirstOrDefault(x => x.EmployeeId == id);
            if (employee == null) return null;
            return new EmployeeDetails
                       {
                           BusinessUnitHeadId = employee.BusinessUnitHeadId,
                           DateJoined = employee.DateJoined,
                           DateResigned = employee.DateResigned,
                           DateConfirmed = employee.DateConfirmed,
                           DepartmentId = employee.DepartmentId,
                           EmailAddress = employee.EmailAddress,
                           EmployeeNo = employee.EmployeeNo,
                           ID = employee.EmployeeId,
                           ImmediateSupervisorId = employee.ImmediateSupervisorId,
                           EmployeeName = employee.EmployeeName,
                           PositionId = employee.PositionId,
                           User = new UserDetails
                                      {
                                         // Password = employee.User.Password,
                                          RoleId = employee.User.Role,
                                          UserId = employee.User.UserId,
                                          Username = employee.User.Username
                                      },
                            Remarks = employee.Remarks,
                            UserId = employee.User.UserId,
                            Grade = employee.Grade,
                            IsActive = employee.IsActive

                       };
        }

        public EmployeeListInfo GetEmployeeInfo(int id)
        {
            var query =
                _ctx.Employees.Include(x => x.Position).Include(x => x.Department).FirstOrDefault(
                    x => x.EmployeeId == id);
            if (query == null) return new EmployeeListInfo();
            return new EmployeeListInfo
                       {
                           Name = query.EmployeeName,
                           Position = query.Position.Name,
                           Department = query.Department.Name,
                           DateJoined = query.DateJoined,
                           DateConfirmed = query.DateConfirmed,
                           EmployeeNo = query.EmployeeNo,
                           Grade = query.Grade,
                           BonusPointBalance = query.BonusPoints.Sum(x=>x.BonusPoints)
                       };
        }

        public EmployeeListInfo GetEmployeeByUserName(string username)
        {
            var query =
              _ctx.Employees.Include(x => x.Position).Include(x => x.Department).FirstOrDefault(
                  x => x.User.Username == username);
            if (query == null) return new EmployeeListInfo();
            return new EmployeeListInfo
            {
                Name = query.EmployeeName,
                Position = query.Position.Name,
                Department = query.Department.Name,
                DateJoined = query.DateJoined,
                EmployeeNo = query.EmployeeNo,
                UserName = query.User.Username,
                Role = Enum.GetName(typeof(Role),query.User.Role)
             
            };
        }

        public EmployeeDetails GetEmployeeByNo(string empno)
        {
            var employee = _ctx.Employees.Include(x => x.User).FirstOrDefault(x => x.EmployeeNo == empno);
            if (employee == null) return null;
            return new EmployeeDetails
            {
                EmailAddress = employee.EmailAddress,
                EmployeeNo = employee.EmployeeNo,
                EmployeeName = employee.EmployeeName,
                ID = employee.EmployeeId,
                UserId = employee.User.UserId

            };
        }

        public EmployeeDetails GetEmployeeByIDandNo(int id, string empno)
        {
            var employee = _ctx.Employees.Include(x => x.User).FirstOrDefault(x => x.EmployeeNo == empno && x.EmployeeId != id);
            if (employee == null) return null;
            return new EmployeeDetails
            {
                EmailAddress = employee.EmailAddress,
                EmployeeNo = employee.EmployeeNo,
                EmployeeName = employee.EmployeeName,
                ID = employee.EmployeeId,
                UserId = employee.User.UserId

            };
        }

        public IList<EmployeeListInfo> GetEmployeeAllList(bool? active)
        {
            if (!active.HasValue || !active.Value)
            {
                var query = _ctx.Employees.Include(e => e.Position).Include(e => e.Department).ToList();
                return query.Select(x => new EmployeeListInfo
                                             {
                                                 Id = x.EmployeeId,
                                                 EmployeeNo = x.EmployeeNo,
                                                 Name = x.EmployeeName,
                                                 Position = x.Position.Name,
                                                 Department = x.Department.Name,
                                                 DateJoined = x.DateJoined,
                                                 DateResigned = x.DateResigned,
                                                 IsActive = x.IsActive
                                             }).ToList();
            }

            var query2 = _ctx.Employees.Include(e => e.Position).Include(e => e.Department).Where(e=>e.IsActive).ToList();
            return query2.Select(x => new EmployeeListInfo
            {
                Id = x.EmployeeId,
                EmployeeNo = x.EmployeeNo,
                Name = x.EmployeeName,
                Position = x.Position.Name,
                Department = x.Department.Name,
                DateJoined = x.DateJoined,
                DateResigned = x.DateResigned,
                IsActive = x.IsActive
            }).ToList();


        }

        public IList<EmployeeListInfo> GetEmployeeList()
        {
            var query = _ctx.Employees.Include(e=>e.Position).Include(e=>e.Department).Where(e=>e.IsActive).ToList();
            return query.Select(x => new EmployeeListInfo
            {
               Id = x.EmployeeId,
               EmployeeNo = x.EmployeeNo,
               Name = x.EmployeeName,
               Position = x.Position.Name,
               Department = x.Department.Name,
               DateJoined = x.DateJoined,
               DateResigned = x.DateResigned
            }).ToList();
        }

        public void AddEmployee(EmployeeDetails employeeDetails)
        {

            //var userid = AddUser(employeeDetails.User);
            var user = new User
                           {
                               Username = employeeDetails.User.Username,
                               Password = employeeDetails.User.Password,
                               Role = employeeDetails.User.RoleId,
                               LastUpdatedDate = DateTime.Now
                           };

            var employee = new Employee();
            employee.EmployeeName = employeeDetails.EmployeeName;
            employee.EmployeeNo = employeeDetails.EmployeeNo;
            employee.EmailAddress = employeeDetails.EmailAddress;
            employee.Grade = employeeDetails.Grade;
            
            if (employeeDetails.BusinessUnitHeadId > 0)
                employee.BusinessUnitHeadId = employeeDetails.BusinessUnitHeadId;

            if (employeeDetails.ImmediateSupervisorId > 0)
            employee.ImmediateSupervisorId= employeeDetails.ImmediateSupervisorId;

            employee.PositionId = employeeDetails.PositionId;
            employee.DepartmentId= employeeDetails.DepartmentId;

            employee.DateJoined = employeeDetails.DateJoined;
            employee.DateResigned = employeeDetails.DateResigned;
            employee.DateConfirmed = employeeDetails.DateConfirmed;

            employee.User = user;
           // employee.UserId = userid;
            employee.Remarks = employeeDetails.Remarks;
            employee.LastUpdatedDate = DateTime.Now;
            employee.IsActive = true;
            _ctx.Employees.Add(employee);
            _ctx.SaveChanges();
        }

        public void UpdateEmployee(EmployeeDetails employeeDetails)
        {
            //var userid = AddUser(employeeDetails.User);
            var user = _ctx.Users.Find(employeeDetails.UserId);

            if (!string.IsNullOrWhiteSpace(employeeDetails.User.Password))
            {
                user.Username = employeeDetails.User.Username;
                user.Password = employeeDetails.User.Password;
            }
            user.Role = employeeDetails.User.RoleId;
            user.LastUpdatedDate = DateTime.Now;


            var employee = _ctx.Employees.Find(employeeDetails.ID);
            employee.EmployeeName = employeeDetails.EmployeeName;
            employee.EmployeeNo = employeeDetails.EmployeeNo;
            employee.EmailAddress = employeeDetails.EmailAddress;

            if (employeeDetails.BusinessUnitHeadId > 0)
                employee.BusinessUnitHeadId = employeeDetails.BusinessUnitHeadId;

            if (employeeDetails.ImmediateSupervisorId > 0)
                employee.ImmediateSupervisorId = employeeDetails.ImmediateSupervisorId;

            employee.PositionId = employeeDetails.PositionId;
            employee.DepartmentId = employeeDetails.DepartmentId;

            employee.DateJoined = employeeDetails.DateJoined;
            employee.DateResigned = employeeDetails.DateResigned;
            employee.DateConfirmed = employeeDetails.DateConfirmed;

            employee.Remarks = employeeDetails.Remarks;
            employee.LastUpdatedDate = DateTime.Now;

            employee.Grade = employeeDetails.Grade;

           // employee.IsActive = employeeDetails.IsActive;
            _ctx.SaveChanges();
        }

        public int AddUser(UserDetails userDetails)
        {
            var user = new User
                           {
                               Username = userDetails.Username,
                               Password = userDetails.Password,
                               Role = userDetails.RoleId,
                               LastUpdatedDate = DateTime.Now
                           };
            _ctx.Users.Add(user);
            _ctx.SaveChanges();
            return user.UserId;
        }

        public void Inactivate(int id)
        {
            var employee = _ctx.Employees.Find(id);
            employee.IsActive = false;
            _ctx.SaveChanges();
        }

        public void Activate(int id)
        {
            var employee = _ctx.Employees.Find(id);
            employee.IsActive = true;
            _ctx.SaveChanges();
        }
    }
}
