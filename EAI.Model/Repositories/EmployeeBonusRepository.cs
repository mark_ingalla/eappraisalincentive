﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using EAI.Domain;
using EAI.Model.Contracts;
using EAI.Model.DTO;
using EAI.Model.Lookup;

namespace EAI.Model.Repositories
{
    public class EmployeeBonusRepository : Repository<EmployeeBonusPoints>, IEmployeeBonusRepository
    {
        private readonly EAIContext _ctx;

        public EmployeeBonusRepository(DbContext ctx) : base(ctx)
        {
            _ctx = (EAIContext) ctx;
        }

        #region Implementation of IEmployeeBonusRepository

        public IList<EmployeeBonusInfo> GetEmployeeBonusList()
        {
            var query = _ctx.EmployeeBonusPoints.Include(e => e.Employee).Where(e => !e.IsSystemGenerated).Select(x => new EmployeeBonusInfo
                                                                                          {
                                                                                              BonusPoints =
                                                                                                  x.BonusPoints,
                                                                                              EmployeeName =
                                                                                                  x.Employee.EmployeeName,
                                                                                              ID =
                                                                                                  x.
                                                                                                  EmployeeBonusPointsId,
                                                                                              TransactionDate =
                                                                                                  x.TransactionDate,
                                                                                              TransactionNo =
                                                                                                  x.TransactionNo
                                                                                          });
            return query.ToList();
        }

        public EmployeeBonusDetails GetEmployeeBonusDetail(int id)
        {
            var employeebonusdetail = _ctx.EmployeeBonusPoints.Find(id);
            return new EmployeeBonusDetails
                       {
                           BonusPoint = employeebonusdetail.BonusPoints,
                           Description = employeebonusdetail.TransactionDescription,
                           EmployeeId = employeebonusdetail.Employee.EmployeeId,
                           ID = employeebonusdetail.EmployeeBonusPointsId,
                           TransactionDate = employeebonusdetail.TransactionDate,
                           TransactionNo = employeebonusdetail.TransactionNo
                       };
        }

        public void AddBonusAdjustment(EmployeeBonusDetails detail)
        {
            var employee = _ctx.Employees.Find(detail.EmployeeId);
            var newbonusdetail = new EmployeeBonusPoints
                                     {
                                         AddedOn = DateTime.Now,
                                         BonusPoints = detail.BonusPoint,
                                         Employee = employee,
                                         LastUpdatedDate = DateTime.Now,
                                         TransactionDate = detail.TransactionDate,
                                         TransactionDescription = detail.Description,
                                         TransactionNo = "0000",
                                         IsSystemGenerated = false

                                     };
            _ctx.EmployeeBonusPoints.Add(newbonusdetail);
            _ctx.SaveChanges();
            newbonusdetail.TransactionNo = newbonusdetail.EmployeeBonusPointsId.ToString("00000");
            _ctx.SaveChanges();
        }

        public void SaveBonusAdjustment(EmployeeBonusDetails detail)
        {
            var bonusadjustment = _ctx.EmployeeBonusPoints.Find(detail.ID);

            bonusadjustment.BonusPoints = detail.BonusPoint;
            bonusadjustment.TransactionDate = detail.TransactionDate;
            bonusadjustment.TransactionDescription = detail.Description;

            _ctx.SaveChanges();
        }

        public void AddBonusRequest(BonusRequestDetails details)
        {
            var employee = _ctx.Employees.Find(details.EmployeeId);
            var newbonusdetail = new EmployeeBonusPointRequest
            {
                
                Employee = employee,
                LastUpdatedDate = DateTime.Now,
                TransactionDate = DateTime.Now,
                TransactionNo = "0000",
                BonusPointCashOut = details.BonusPointRequested

            };
            _ctx.EmployeeBonusPointRequests.Add(newbonusdetail);
            _ctx.SaveChanges();
            newbonusdetail.TransactionNo = newbonusdetail.EmployeeBonusPointRequestId.ToString("00000");
            _ctx.SaveChanges();
        }

        public IList<EmployeeAppraisalInfo> GetEmployeesForMonthlyCredit(DateTime dateTime)
        {
            var quartermonths = new[] { 10, 11, 12 };
            var quarteryear = dateTime.Year - 1;


            if (dateTime.Month <= 6 && dateTime.Month >= 4)
            {
                quartermonths = new[] { 1, 2, 3 };
                quarteryear = dateTime.Year;
            }
            else if (dateTime.Month <= 9 && dateTime.Month >= 7)
            {
                quartermonths = new[] { 4, 5, 6 };
                quarteryear = dateTime.Year;
            }
            else if (dateTime.Month <= 12 && dateTime.Month >= 10)
            {
                quartermonths = new[] { 7, 8, 9 };
                quarteryear = dateTime.Year;
            }


            //we only get those employees having appraisal on the last quarter AND no systemgenerated points for this month
            var query = _ctx.EmployeeAppraisals.Include(p=>p.Employee.BonusPoints).ToList().Where(p => quartermonths.Contains(p.AppraisalDate.Month)
                && p.AppraisalDate.Year == quarteryear && p.Status == (int) EmployeeAppraisalStatus.Complete
                && (p.Employee.BonusPoints.Any(b=>!b.IsSystemGenerated && b.TransactionDate.Month != dateTime.Month
                && b.TransactionDate.Year != dateTime.Year) || p.Employee.BonusPoints.Count == 0)
                ).Select(x => new EmployeeAppraisalInfo
                {
                    EmployeeName = x.Employee.EmployeeName,
                    Rating = x.OverallRating,
                    ID = x.Employee.EmployeeId,
                    AppraisalDate = x.AppraisalDate
                   
                });

            return query.ToList();
        }

        public IList<BonusRequestInfo> GetBonusRequests()
        {
            var query = _ctx.EmployeeBonusPointRequests.Include(x => x.Employee)
                .Select(x => new BonusRequestInfo
                                 {
                                     BonusPointsRequested = x.BonusPointCashOut,
                                     EmployeeName = x.Employee.EmployeeName,
                                     ID = x.EmployeeBonusPointRequestId,
                                     TransactionDate = x.TransactionDate,
                                     TransactionNo = x.TransactionNo
                                 });
            return query.ToList();
        }

        public int GetMaximumCashableBonusPoint(DateTime transactiondate, int employeeid)
        {
            var employee = _ctx.Employees.Find(employeeid);
            var monthsOfService = transactiondate.Subtract(employee.DateJoined).Days / (365.25 / 12);
            var employeeBonusPoints = employee.BonusPoints.Sum(x => x.BonusPoints);
            decimal cashablelimit = 0;
            //refers to june of each year)


            if (transactiondate.Month != ConfigurationValues.FinancialEndYear)
            {
                cashablelimit = ConfigurationValues.MonthEndCashOutLimit;
            }
            else
            {
                if (monthsOfService <= 36)
                {
                    cashablelimit = employeeBonusPoints * ((decimal)ConfigurationValues.FinancialYearEndCashOutLimit0To36 / 100);
                }
                if (monthsOfService > 36 && monthsOfService <= 60)
                {
                    cashablelimit = employeeBonusPoints * ((decimal)ConfigurationValues.FinancialYearEndCashOutLimit37To60 / 100);
                }
                if (monthsOfService > 60)
                {
                    cashablelimit = employeeBonusPoints * ((decimal)ConfigurationValues.FinancialYearEndCashOutLimitabove60 / 100); ;
                }
            }


            //you cant exceed your available points
            return (int) (cashablelimit >= employeeBonusPoints ? employeeBonusPoints : cashablelimit);
        }

        public void ProcessMonthlyCredit(DateTime dateTime)
        {
            var employees = GetEmployeesForMonthlyCredit(dateTime);
            foreach (var employeeAdj in from employeeAppraisalInfo in employees
                                        let employee = _ctx.Employees.Find(employeeAppraisalInfo.ID)
                                        select new EmployeeBonusPoints
                                                   {
                                                       BonusPoints = GetBonusPoint(employeeAppraisalInfo.Rating) * (GetBonusPointProRated(employee.DateConfirmed,dateTime) / 3),
                                                       IsSystemGenerated = true, 
                                                       TransactionDescription = "Monthly Credit Bonus Points Processing", 
                                                       TransactionDate = dateTime,
                                                       AddedOn = DateTime.Now, 
                                                       LastUpdatedDate = DateTime.Now, 
                                                       Employee = employee, 
                                                       TransactionNo = "0000"
                                                   })
            {
                _ctx.EmployeeBonusPoints.Add(employeeAdj);
                _ctx.SaveChanges();
                employeeAdj.TransactionNo = employeeAdj.EmployeeBonusPointsId.ToString("00000");
                _ctx.SaveChanges();
            }
        }

        private static int GetBonusPoint(int rating)
        {
            switch (rating)
            {
                case 1:
                    return ConfigurationValues.BonusPointPerMonthPerformance1;
                case 2:
                    return ConfigurationValues.BonusPointPerMonthPerformance2;
                case 3:
                    return ConfigurationValues.BonusPointPerMonthPerformance3;
                default:
                    return ConfigurationValues.BonusPointPerMonthPerformance4;
            }
        }


        public decimal GetBonusPointProRated(DateTime? confirmedDate,DateTime quarterEndDate)
        {

            if (!confirmedDate.HasValue) return 0;

            var quartermonths = new[] { 10, 11, 12 };
           


            if (quarterEndDate.Month >= 1 && quarterEndDate.Month <= 3)
            {
                quartermonths = new[] { 1, 2, 3 };
             
            }
            else if (quarterEndDate.Month >= 4 && quarterEndDate.Month <= 6)
            {
                quartermonths = new[] { 4, 5, 6 };
               
            }
            else if (quarterEndDate.Month >= 7 && quarterEndDate.Month <= 9)
            {
                quartermonths = new[] { 7, 8, 9 };
               
            }

            var year = quarterEndDate.Year;

            var fullbonusdate = new DateTime(year, quartermonths[0], 5);
            if (confirmedDate <= fullbonusdate)
            {
                return 3;
            }

            var bonus25Rangedate1 = new DateTime(year, quartermonths[0], 6);
            var bonus25Rangedate2 = new DateTime(year, quartermonths[0], 17);

            if (confirmedDate >= bonus25Rangedate1 && confirmedDate <= bonus25Rangedate2)
            {
                return  (decimal) 2.5;
            }


            var bonus20Rangedate1 = new DateTime(year, quartermonths[0], 18);
            var bonus20Rangedate2 = new DateTime(year, quartermonths[1], 5);

            if (confirmedDate >= bonus20Rangedate1 && confirmedDate <= bonus20Rangedate2)
            {
                return (decimal) (2.0);
            }

            var bonus15Rangedate1 = new DateTime(year, quartermonths[1], 6);
            var bonus15Rangedate2 = new DateTime(year, quartermonths[1], 17);

            if (confirmedDate >= bonus15Rangedate1 && confirmedDate <= bonus15Rangedate2)
            {
                return (decimal)( 1.5);
            }


            var bonus10Rangedate1 = new DateTime(year, quartermonths[1], 18);
            var bonus10Rangedate2 = new DateTime(year, quartermonths[2], 5);

            if (confirmedDate >= bonus10Rangedate1 && confirmedDate <= bonus10Rangedate2)
            {
                return (decimal)( 1.0);
            }

            var bonus05Rangedate1 = new DateTime(year, quartermonths[2], 6);
            var bonus05Rangedate2 = new DateTime(year, quartermonths[2], 17);


            if (confirmedDate >= bonus05Rangedate1 && confirmedDate <= bonus05Rangedate2)
            {
                return (decimal)( 0.5);
            }

            return 0;

        }

        #endregion
    }
}
