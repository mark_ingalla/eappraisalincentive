﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using EAI.Domain;
using EAI.Model.Contracts;
using EAI.Model.DTO;

namespace EAI.Model.Repositories
{
    public class UserRepository : Repository<User>,IUserRepository
    {

        private readonly EAIContext _context;

        public UserRepository(DbContext ctx) : base(ctx)
        {
            _context = (EAIContext)ctx;
        }

        #region Implementation of IUserRepository

        public bool ValidateUser(string username, string password)
        {
            var user = _context.Users.FirstOrDefault(x => x.Username == username && x.Password == password);
            return user != null;
        }

        public int GetUserId(string username)
        {
            var user = _context.Users.FirstOrDefault(x => x.Username == username);
            return user != null ? user.UserId : 0;
        }

        public bool ChangePassword(int userid, string newpassword, string oldpassword)
        {
            var user = _context.Users.Find(userid);
            try
            {
                if (user.Password == oldpassword)
                {
                    user.Password = newpassword;
                    _context.SaveChanges();
                }
            }
            catch (Exception)
            {

                return false;
            }
            return true;
        }

        public UserDetails GetUserDetails(string username)
        {
            var user = _context.Users.FirstOrDefault(x => x.Username == username);
            return new UserDetails
                       {
                           Username = user.Username,
                           Theme = user.Theme,
                           UserId = user.UserId
                       };

        }

        public void ChangeTheme(int id, string theme)
        {
            var user = _context.Users.Find(id);
            user.Theme = theme;
            _context.SaveChanges();
        }

        #endregion
    }
}
