﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using EAI.Domain;
using EAI.Model.Contracts;
using EAI.Model.DTO;

namespace EAI.Model.Repositories
{
    public class ReportsRepository : Repository<EmployeeAppraisal>,IReportsRepository
    {

        private readonly EAIContext _context;

        public ReportsRepository(DbContext ctx) : base(ctx)
        {
            _context = (EAIContext) ctx;
        }

        #region Implementation of IReportsRepository

        public IList<RptQuarterlyAppraisal> QuarterlyAppraisalIncentives(ReportFilter filter)
        {
            var query =
                _context.EmployeeAppraisals.Include(x => x.Employee).ToList().Where(
                    x => x.AppraisalDate.Date >= filter.FromDate.Date && x.AppraisalDate.Date <= filter.ToDate.Date)
                    .Select(x => new RptQuarterlyAppraisal
                                     {
                                         EmployeeName = x.Employee.EmployeeName,
                                         DateConfirmed = x.Employee.DateConfirmed,
                                         DateJoined = x.Employee.DateJoined,
                                         Department = x.Employee.Department.Name,
                                         EmployeeNo = x.Employee.EmployeeNo,
                                         OverallRating = x.OverallRating,
                                         Position = x.Employee.Position.Name

                                     }).ToList();
            return query;
        }

        public IList<RptBonusAdjustment> BonusAdjustments(ReportFilter filter)
        {
            var query = _context.EmployeeBonusPoints.Include(x => x.Employee).Include(x => x.LastUpdatedBy).ToList().
                Where(
                    x => x.TransactionDate.Date >= filter.FromDate.Date && x.TransactionDate.Date <= filter.ToDate.Date)
                .Select(x => new RptBonusAdjustment
                                 {
                                     CreditDebitPoints = x.BonusPoints,
                                     EmployeeName = x.Employee.EmployeeName,
                                     EmployeeNo = x.Employee.EmployeeNo,
                                     TransactionDate = x.TransactionDate,
                                     TransactionDescription = x.TransactionDescription,
                                     TransactionNo = x.TransactionNo,
                                     UpdatedBy = x.LastUpdatedBy.Username
                                 });
            return query.ToList();
        }

        public IList<RptBonusPointRequest> BonusPointRequests(ReportFilter filter)
        {
            var query = _context.EmployeeBonusPointRequests.Include(x => x.Employee).ToList().
                Where(
                    x => x.TransactionDate.Date >= filter.FromDate.Date && x.TransactionDate.Date <= filter.ToDate.Date)
                .Select(x => new RptBonusPointRequest
                {
                    CashoutBonusPoint = x.BonusPointCashOut,                    
                    EmployeeName = x.Employee.EmployeeName,
                    EmployeeNo = x.Employee.EmployeeNo,
                    TransactionDate = x.TransactionDate,
                    TransactionNo = x.TransactionNo,
                    MonthsofService =  (int) (DateTime.Now.Subtract(x.Employee.DateJoined).Days / (365.25 / 12))
                    
                    
                });
            return query.ToList();
        }

        public IList<RptBonusAdjustment> BonusPointStatus(ReportFilter filter)
        {
            var query = _context.EmployeeBonusPoints.Include(x => x.Employee).Include(x => x.LastUpdatedBy).ToList().
                Where(
                    x => x.TransactionDate.Date >= filter.FromDate.Date && x.TransactionDate.Date <= filter.ToDate.Date)
                .Select(x => new RptBonusAdjustment
                {
                    CreditDebitPoints = x.BonusPoints,
                    EmployeeName = x.Employee.EmployeeName,
                    EmployeeNo = x.Employee.EmployeeNo,
                    TransactionDate = x.TransactionDate,
                    TransactionDescription = x.TransactionDescription,
                    TransactionNo = x.TransactionNo,
                    BonusPointBalance = x.Employee.BonusPoints.Sum(y=>y.BonusPoints)
                });
            return query.ToList();
        }

        public IList<RptMonthlyBonusPointCredit> MontlyCreditBonusPoints(ReportFilter filter)
        {
            var query = _context.EmployeeBonusPoints.Include(x => x.Employee).Include(x => x.LastUpdatedBy).ToList().
                Where(
                    x => x.TransactionDate.Date >= filter.FromDate.Date && x.TransactionDate.Date <= filter.ToDate.Date && x.IsSystemGenerated)
                .Select(x => new RptMonthlyBonusPointCredit
                {
                    
                    EmployeeName = x.Employee.EmployeeName,
                    EmployeeNo = x.Employee.EmployeeNo,
                    TransactionDate = x.TransactionDate,
                    TransactionNo = x.TransactionNo,
                    BonusPointBalance = x.Employee.BonusPoints.Sum(y => y.BonusPoints),
                    LastQuarterOverallRating = x.Employee.Appraisals.LastOrDefault() != null ? x.Employee.Appraisals.LastOrDefault().OverallRating : 0
                });
            return query.ToList();
        }

        #endregion


    }
}
