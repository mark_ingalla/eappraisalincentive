﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using EAI.Domain;
using EAI.Model.Contracts;
using EAI.Model.DTO;

namespace EAI.Model.Repositories
{
    public class DepartmentRepository : Repository<Department>, IDepartmentRepository
    {
        private readonly EAIContext _context;

        public DepartmentRepository(DbContext ctx)
            : base(ctx)
        {
            _context = (EAIContext) ctx;
        }

        public IList<DepartmentDetails> GetAllDepartments()
        {
            var query = _context.Departments.Select(x => new DepartmentDetails
                                                           {
                                                               ID = x.DepartmentId,
                                                               Name = x.Name
                                                           });
            return query.ToList();
        }

        public int AddDepartment(DepartmentDetails DepartmentDetails)
        {
            var department = new Department()
                               {
                                   Name = DepartmentDetails.Name,
                                   LastUpdatedDate = DateTime.Now
                               };
            _context.Departments.Add(department);
            _context.SaveChanges();

            return department.DepartmentId;
        }

        public DepartmentDetails GetDepartmentDetail(int id)
        {
            var position = _context.Departments.FirstOrDefault(x => x.DepartmentId == id);
            return new DepartmentDetails
                       {
                           ID = position.DepartmentId,
                           Name = position.Name
                       };
        }

        public void Save(DepartmentDetails details)
        {
            var department = _context.Departments.FirstOrDefault(x=>x.DepartmentId == details.ID);
            department.Name = details.Name;
            _context.SaveChanges();


        }

        public void Delete(int id)
        {
            var department = _context.Departments.FirstOrDefault(x => x.DepartmentId == id);
            _context.Departments.Remove(department);
            _context.SaveChanges();
        }
    }
}
