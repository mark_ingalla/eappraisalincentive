﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EAI.Model.Lookup
{
    public enum EmployeeAppraisalStatus
    {        
        PendingStaffAcknowledgement = 1,
        PendingSupervisorApproval = 2,
        PendingBusinessHeadApproval = 3,
        Complete = 4
    }
}
