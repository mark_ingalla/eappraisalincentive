﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EAI.Model.Lookup
{
    public  enum Role
    {
        Admin = 1,
        HR = 2,
        User = 3
    }
}
