﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace EAI.Model.Lookup
{
    public static class ConfigurationValues
    {
        public static int FinancialEndYear
        {
            get { return Convert.ToInt32(ConfigurationManager.AppSettings["FinancialYearMo"]); }
        }

        public static int BonusPointPerMonthPerformance1
        {
            get { return Convert.ToInt32(ConfigurationManager.AppSettings["BonusPerformance1"]); }
        }
        public static int BonusPointPerMonthPerformance2
        {
            get { return Convert.ToInt32(ConfigurationManager.AppSettings["BonusPerformance2"]); }
        }

        public static int BonusPointPerMonthPerformance3
        {
            get { return Convert.ToInt32(ConfigurationManager.AppSettings["BonusPerformance3"]); }
        }

        public static int BonusPointPerMonthPerformance4
        {
            get { return Convert.ToInt32(ConfigurationManager.AppSettings["BonusPerformance4"]); }
        }

        public static int MonthEndCashOutLimit
        {
            get { return Convert.ToInt32(ConfigurationManager.AppSettings["MonthEndCashOutLimit"]); }
        }

        public static int FinancialYearEndCashOutLimit0To36
        {
            get { return Convert.ToInt32(ConfigurationManager.AppSettings["FinancialYearEndCashOutLimit0to36"]); }
        }
        public static int FinancialYearEndCashOutLimit37To60
        {
            get { return Convert.ToInt32(ConfigurationManager.AppSettings["FinancialYearEndCashOutLimit37to60"]); }
        }
        public static int FinancialYearEndCashOutLimitabove60
        {
            get { return Convert.ToInt32(ConfigurationManager.AppSettings["FinancialYearEndCashOutLimitabove60"]); }
        }
        
            
            
        
    }
}
