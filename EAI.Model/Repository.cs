﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Objects;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace EAI.Model
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Web;

        public class Repository<T> : IRepository<T> where T : class,new()
        {
            readonly DbContext _yourApplicationDbContext;

            public Repository(DbContext ctx)
            {
                _yourApplicationDbContext = ctx;
                //if (_yourApplicationDbContext == null)
                //{
                //    _yourApplicationDbContext = new yourApplicationDbContext();
                //}
            }

            public void CommitChanges()
            {
                _yourApplicationDbContext.SaveChanges();
            }

            public void Delete(Expression<Func<T, bool>> expression) 
            {
                var query = All().Where(expression);
                foreach (var item in query)
                {
                    Delete(item);
                }
            }

            public void Delete(T item)
            {
                _yourApplicationDbContext.Set<T>().Remove(item);
            }

            public void DeleteAll()
            {
                var query = All();
                foreach (var item in query)
                {
                    Delete(item);
                }
            }

            public T Single(Expression<Func<T, bool>> expression) 
            {
                return All().FirstOrDefault(expression);
            }

            public IQueryable<T> All() 
            {
                return _yourApplicationDbContext.Set<T>().AsQueryable();
            }

            public void Add(T item)
            {
                _yourApplicationDbContext.Set<T>().Add(item);
            }

            public void Add(IEnumerable<T> items) 
            {
                foreach (var item in items)
                {
                    Add(item);
                }
            }

            public void Update(T item)
            {
                //nothing needed here
            }
        }
    
}
