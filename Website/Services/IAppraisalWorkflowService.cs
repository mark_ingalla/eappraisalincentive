﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using EAI.Model.DTO;

namespace Website.Services
{
    public interface IAppraisalWorkflowService
    {
        void Push(EmployeeAppraisalDetails appraisalDetails);
    }
}