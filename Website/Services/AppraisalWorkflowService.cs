﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using EAI.Model.Contracts;
using EAI.Model.DTO;
using EAI.Model.Lookup;
using Mvc.Mailer;
using Website.Mailers;
using System.Web.Mvc;

namespace Website.Services
{
    public class AppraisalWorkflowService : IAppraisalWorkflowService
    {

        private readonly IEmployeeAppraisalRepository _employeeAppraisalRepository;
        private readonly IEmployeeRepository _employeeRepository;
        private readonly IAppraisalNotifier _appraisalNotifier;

        public AppraisalWorkflowService(IEmployeeAppraisalRepository employeeAppraisalRepository,
            IEmployeeRepository employeeRepository,
            IAppraisalNotifier appraisalNotifier)
        {
            _employeeAppraisalRepository = employeeAppraisalRepository;
            _employeeRepository = employeeRepository;
            _appraisalNotifier = appraisalNotifier;
        }

        #region Implementation of IAppraisalWorkflowService

        public void Push(EmployeeAppraisalDetails appraisalDetails)
        {

            var employee = _employeeRepository.GetEmployee(appraisalDetails.EmployeeId);
            var supervisor = _employeeRepository.GetEmployee(employee.ImmediateSupervisorId.HasValue ? employee.ImmediateSupervisorId.Value : 0);
            var businessunithead = _employeeRepository.GetEmployee(employee.BusinessUnitHeadId.HasValue ? employee.BusinessUnitHeadId.Value : 0);
            var appraisalMailDTO = new EmployeeAppraisalMailDTO
            {
                AppraisalEmployeeName = employee.EmployeeName,
                AppraisalNo =  appraisalDetails.TransactionNo,
                Id = appraisalDetails.ID,
                Url = UrlHelperExtensions.Abs(new UrlHelper(HttpContext.Current.Request.RequestContext), "/EmployeeAppraisals/ApproveAppraisal/" + appraisalDetails.ID.ToString())
                
                
                
            };
            switch (appraisalDetails.Status)
            {
                case (int) EmployeeAppraisalStatus.PendingStaffAcknowledgement:
                    if (supervisor != null)
                    {
                        appraisalMailDTO.ToEmail = supervisor.EmailAddress;
                        appraisalMailDTO.ToName = supervisor.EmployeeName;
                        _employeeAppraisalRepository.UpdateStatus(appraisalDetails,
                                                                  EmployeeAppraisalStatus.PendingSupervisorApproval);
                        _appraisalNotifier.NotifySupervisor(appraisalMailDTO).Send();
                    }
                    break;
                case (int)EmployeeAppraisalStatus.PendingSupervisorApproval:
                    if (businessunithead != null)
                    {
                        appraisalMailDTO.ToEmail = businessunithead.EmailAddress;
                        appraisalMailDTO.ToName = businessunithead.EmployeeName;
                        _employeeAppraisalRepository.UpdateStatus(appraisalDetails,
                                                                  EmployeeAppraisalStatus.PendingBusinessHeadApproval);
                        _appraisalNotifier.NotifyBusinessUnitHead(appraisalMailDTO).Send();
                    }
                    break;
                case (int)EmployeeAppraisalStatus.PendingBusinessHeadApproval:
                    _employeeAppraisalRepository.UpdateStatus(appraisalDetails,
                                                                  EmployeeAppraisalStatus.Complete);
                    break;
                    
                default:
                    
                        appraisalMailDTO.ToEmail = employee.EmailAddress;
                        appraisalMailDTO.ToName = employee.EmployeeName;
                        _employeeAppraisalRepository.UpdateStatus(appraisalDetails,
                                                                  EmployeeAppraisalStatus.PendingStaffAcknowledgement);
                        _appraisalNotifier.NotifyEmployee(appraisalMailDTO).Send();
                    
                    break;
                    
            }
        }

        #endregion
    }
}