﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Website.ViewModels
{
    public class EmployeeListInfoPageViewModel
    {
        public int Id { get; set; }
        public string EmployeeNo { get; set; }
        public string Name { get; set; }
        public string Position { get; set; }
        public string Department { get; set; }

        [Display(Name = "Date Joined")]
        public string DateJoined { get; set; }

        [Display(Name = "Date Confirmed")]
        public string DateConfirmed { get; set; }


        public string Grade { get; set; }

        [Display(Name = "Bonus Points Balance")]
        public decimal BonusPointBalance { get; set; }
    }
}