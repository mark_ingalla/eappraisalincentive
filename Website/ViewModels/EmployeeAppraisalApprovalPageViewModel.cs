﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using EAI.Model.DTO;
using EAI.Model.Lookup;

namespace Website.ViewModels
{
    public class EmployeeAppraisalApprovalPageViewModel
    {
        public int Id { get; set; }
        
        public string Comment { get; set; }
        public string AppraisalNo { get; set; }
        public string EmployeeName { get; set; }
        public string Status { get; set; }
    


        public EmployeeAppraisalApprovalPageViewModel(){}

        public void UpdateModel(EmployeeAppraisalDetails details)
        {
            switch (details.Status)
            {
                case (int) EmployeeAppraisalStatus.PendingStaffAcknowledgement:
                    details.EmployeeComment = Comment;
                  
                    break;
                case (int)EmployeeAppraisalStatus.PendingSupervisorApproval:
                    details.SupervisorComment = Comment;
                 
                    break;
                case (int)EmployeeAppraisalStatus.PendingBusinessHeadApproval:
                    details.BusinessUnitHeadComment = Comment;
             
                    break;
                default:

                    break;
                    
            }
        }
    }
}