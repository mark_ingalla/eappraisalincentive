﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EAI.Model.Contracts;
using EAI.Model.DTO;

namespace Website.ViewModels
{
    public class EmployeeBonusAdjustmentInputPageViewModel
    {
        private readonly IEmployeeRepository _employeeRepository;
        private readonly IEmployeeAppraisalRepository _employeeAppraisalRepository;
        private readonly IAppraisalRepository _appraisalRepository;
        private readonly IEmployeeBonusRepository _employeeBonusRepository;

        public int ID { get; set; }
        [Display(Name = "Transaction No")]
        public string TransactionNo { get; set; }

        [Display(Name="Transaction Date")]
        public DateTime TransactionDate { get; set; }

        [Display(Name = "Employee Name")]
        public int SelectedEmployeeId { get; set; }

        [Display(Name = "Transaction Description")]
        public string TransactionDescription { get; set; }

        [Display(Name = "Credit/Debit Bonus Points")]
        public decimal CreditDebitBonusPoint { get; set; }

        public IList<SelectListItem> Employees { get; set; }


        public EmployeeBonusAdjustmentInputPageViewModel()
        {
        }

        public EmployeeBonusAdjustmentInputPageViewModel(IEmployeeRepository employeeRepository
            ,IEmployeeBonusRepository employeeBonusRepository)
        {
            _employeeRepository = employeeRepository;
            _employeeBonusRepository = employeeBonusRepository;

            Employees = _employeeRepository.GetEmployeeList().Select(x => new SelectListItem
            {
                Text = x.Name,
                Value = x.Id.ToString()
            }).ToList();
        }

        public void UpdateModel(EmployeeBonusDetails detail)
        {
            detail.EmployeeId = SelectedEmployeeId;
            detail.BonusPoint = CreditDebitBonusPoint;
            detail.TransactionDate = TransactionDate;
            detail.Description = TransactionDescription;
            detail.TransactionNo = TransactionNo;
            detail.ID = detail.ID;


        }

        public void GetModel(EmployeeBonusDetails bonusDetails)
        {
            ID = bonusDetails.ID;
            SelectedEmployeeId = bonusDetails.EmployeeId;
            TransactionNo = bonusDetails.TransactionNo;
            TransactionDate = bonusDetails.TransactionDate;
            TransactionDescription = bonusDetails.Description;
            CreditDebitBonusPoint = bonusDetails.BonusPoint;

        }
    }
}