﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EAI.Model.Contracts;
using EAI.Model.DTO;
using EAI.Model.Lookup;
using Website.Framework;

namespace Website.ViewModels
{
    public class EmployeeInputPageViewModel
    {
        private readonly IPositionRepository _positionRepository;
        private readonly IDepartmentRepository _departmentRepository;
        private readonly IEmployeeRepository _employeeRepository;

        #region Properties

        public int ID { get; set; }
        public int UserId { get; set; }
        //[Required]
        [Display(Name = "User ID")]
        public string UserName { get; set; }
        public int SelectedRole { get; set; }

         //[Required]
         [DataType(DataType.Password)]
         [Display(Name = "User Password")]
        public string Password { get; set; }

        //[Required]
        [Compare("Password", ErrorMessage = "The new password and confirmation password do not match.")]
        [DataType(DataType.Password)]
        [Display(Name = "Reconfirm Password")]
         public string ConfirmPassword { get; set; }

       //  //[Required]
         [DataType(DataType.EmailAddress)]
         [Display(Name = "Email Address")]
        public string Email { get; set; }

         //[Required]
        [Display(Name = "Employee No")]
        public string EmployeeNo { get; set; }

      [Display(Name = "Employee Name")]
        public string EmployeeName { get; set; }

        [Display(Name = "Position")]
        public int SelectedPosition { get; set; }
        [Display(Name = "Department")]
        public int SelectedDepartment { get; set; }
        public string Grade { get; set; }

        [Display(Name = "Date Joined")]
        public DateTime DateJoined { get; set; }

        [Display(Name = "Date Confirmed")]
        public DateTime? DateConfirmed { get; set; }

        [Display(Name = "Date Resigned")]
        public DateTime? DateResigned { get; set; }

        [Display(Name = "Immediate Supervisor")]
        public int? SelectedSupervisor { get; set; }
        [Display(Name = "Business Unit Head")]
        public int? SelectedBusinessHead { get; set; }
        public string Remarks { get; set; }

        
        public IList<SelectListItem> Positions { get; set; }
        public IList<SelectListItem> Departments { get; set; }
        public IList<SelectListItem> Supervisors { get; set; }
        public IList<SelectListItem> BusinessHeads { get; set; }
       

        public SelectList Roles { get; set; }
        public SelectList Grades { get; set; }

        #endregion

        public EmployeeInputPageViewModel()
        {
        }

        public EmployeeInputPageViewModel(IPositionRepository positionRepository,IDepartmentRepository departmentRepository,IEmployeeRepository employeeRepository)
        {
            _positionRepository = positionRepository;
            _departmentRepository = departmentRepository;
            _employeeRepository = employeeRepository;

            Positions = _positionRepository.GetAllPositions().Select(x => new SelectListItem
                                                                              {
                                                                                  Text = x.Name,
                                                                                  Value = x.ID.ToString()
                                                                              }).ToList();

            Departments = _departmentRepository.GetAllDepartments().Select(x => new SelectListItem
                                                                            {
                                                                                Text = x.Name,
                                                                                Value = x.ID.ToString()
                                                                            }).ToList();

            BusinessHeads = _employeeRepository.GetActiveEmployees().Select(x => new SelectListItem
                                                                            {
                                                                                Text = x.Name,
                                                                                Value = x.Id.ToString()
                                                                            }).ToList();
            Supervisors = _employeeRepository.GetActiveEmployees().Select(x => new SelectListItem
                                                                            {
                                                                                Text = x.Name,
                                                                                Value = x.Id.ToString()
                                                                            }).ToList();

            Roles = Role.User.ToSelectList();

            Grades = EAI.Model.Lookup.Grade.D.ToSelectListString();
        }

        public void UpdateModel(EmployeeDetails employeeDetails)
        {

            employeeDetails.EmployeeName = EmployeeName;            
            employeeDetails.PositionId = SelectedPosition;
            employeeDetails.DepartmentId = SelectedDepartment;
            employeeDetails.EmailAddress = Email;
            employeeDetails.Grade = Grade;

            employeeDetails.BusinessUnitHeadId = SelectedBusinessHead;
            employeeDetails.ImmediateSupervisorId = SelectedSupervisor;
            employeeDetails.DateJoined = DateJoined;
            employeeDetails.DateResigned = DateResigned;
            employeeDetails.DateConfirmed = DateConfirmed;
            employeeDetails.EmployeeNo = EmployeeNo;

            var user = new UserDetails
                           {
                               Password = Password,
                               Username = UserName,
                               RoleId = SelectedRole,
                               UserId = UserId
                           };
            employeeDetails.User = user;

            employeeDetails.Remarks = Remarks;


        }

        public void GetModel(EmployeeDetails employeeDetails)
        {
            ID = employeeDetails.ID;
            EmployeeName = employeeDetails.EmployeeName;
             SelectedPosition = employeeDetails.PositionId;
             SelectedDepartment = employeeDetails.DepartmentId;
             Email = employeeDetails.EmailAddress;

             SelectedBusinessHead = employeeDetails.BusinessUnitHeadId;
             SelectedSupervisor = employeeDetails.ImmediateSupervisorId;
             DateJoined = employeeDetails.DateJoined;
             DateResigned = employeeDetails.DateResigned;
            DateConfirmed = employeeDetails.DateConfirmed;
             EmployeeNo = employeeDetails.EmployeeNo;


    //        Password = employeeDetails.User.Password;
            UserName = employeeDetails.User.Username;
            SelectedRole = employeeDetails.User.RoleId;

            Remarks = employeeDetails.Remarks;
            Grade = employeeDetails.Grade;
        }
        




    }
}