﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Website.ViewModels
{
    public class EmployeeDeletePageViewModel
    {
        public int Id { get; set; }

        [Display(Name="Employee No")]
        public string EmployeeNo { get; set; }

        [Display(Name = "Employee Name")]
        public string EmployeeName { get; set; }
    }
}