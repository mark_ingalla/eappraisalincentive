﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using EAI.Model.DTO;

namespace Website.ViewModels
{
    public class EmployeeListPageViewModel
    {
        public IList<EmployeeDetails> Employees { get; set; }
    }
}