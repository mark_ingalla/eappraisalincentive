﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EAI.Model.Contracts;
using EAI.Model.DTO;

namespace Website.ViewModels
{
    public class EmployeeAppraisalInputPageViewModel
    {

        private readonly IEmployeeRepository _employeeRepository;
        private readonly IEmployeeAppraisalRepository _employeeAppraisalRepository;
        private readonly IAppraisalRepository _appraisalRepository;

        public int ID { get; set; }

        [Display(Name="Employee")]
        public int SelectedEmployeeId { get; set; }

        [Display(Name = "Appraisal No.")]
        public string AppraisalNo { get; set; }

        [Display(Name = "Appraisal Date")]
        public DateTime AppraisalDate { get; set; }

        [Display(Name = "Overall Rating")]
        public int OverallRating { get; set; }

        public string Position {  get; private set; }
        public string Department { get; private set; }

        public int Status { get; set; }

        public IList<SelectListItem> Employees { get; set; }


        public IList<EmployeeAppraisalItemDetails> Dimensions { get; set; }

        public EmployeeAppraisalInputPageViewModel()
        {

        }

        public EmployeeAppraisalInputPageViewModel(int userid, IEmployeeRepository employeeRepository,
            IEmployeeAppraisalRepository employeeAppraisalRepository,
            IAppraisalRepository appraisalRepository)
        {
            _employeeRepository = employeeRepository;
            _employeeAppraisalRepository = employeeAppraisalRepository;
            _appraisalRepository = appraisalRepository;
            Employees = _employeeRepository.GetEmployeesUnderSupervisor(userid).Select(x => new SelectListItem
            {
                Text = x.Name,
                Value = x.Id.ToString()
            }).ToList();
            //Employees = _employeeRepository.GetEmployeeList().Select(x => new SelectListItem
            //{
            //    Text = x.Name,
            //    Value = x.Id.ToString()
            //}).ToList();
            
        }

        

        public void CreateNewAppraisal()
        {
            
            //Dimensions = _appraisalRepository.GetAppraisalItems().Select(x => new EmployeeAppraisalItemDetails
            //                                                                      {
            //                                                                          ItemInfo = new EmployeeAppraisalItemInfo
            //                                                                                         {
            //                                                                                             ID = x.Header.Id,
            //                                                                                             Description = x.Header.Id.ToString() + ")." + x.Header.Name
            //                                                                                         },
            //                                                                         AppraisalId = x.Id,
            //                                                                          Description = x.Description
                                                                                       
            //                                                                      }).ToList();
            Dimensions = _employeeAppraisalRepository.GetAppraisalItems();
            AppraisalDate = DateTime.Now;
        }

        public void UpdateModel(EmployeeAppraisalDetails  appraisalDetails,IEmployeeRepository employeeRepository)
        {
            

            var selectedemployee = employeeRepository.GetEmployee(SelectedEmployeeId);

            appraisalDetails.EmployeeId = SelectedEmployeeId;
            appraisalDetails.AppraisalDate = AppraisalDate;

            //auto generate value from employee table
            if (selectedemployee.BusinessUnitHeadId.HasValue)
                appraisalDetails.BusinessUnitHeadId = (int) selectedemployee.BusinessUnitHeadId;
            if (selectedemployee.ImmediateSupervisorId.HasValue)
                appraisalDetails.SupervisorId = (int)selectedemployee.ImmediateSupervisorId;


            appraisalDetails.EmployeeAppraisalItems = Dimensions;
            appraisalDetails.OverallRating = OverallRating;
        }

        public void GetModel(EmployeeAppraisalDetails appraisalDetails)
        {
            SelectedEmployeeId = appraisalDetails.EmployeeId;
            AppraisalDate = appraisalDetails.AppraisalDate;
            Dimensions = _employeeAppraisalRepository.GetEmployeeAppraisalItems(appraisalDetails.ID);
            OverallRating = appraisalDetails.OverallRating;
            Status = appraisalDetails.Status;
        }
    }
}