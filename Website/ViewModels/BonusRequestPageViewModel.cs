﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using EAI.Model.Contracts;
using EAI.Model.DTO;

namespace Website.ViewModels
{
    public class BonusRequestPageViewModel
    {
        private readonly IEmployeeRepository _employeeRepository;
        private readonly IEmployeeBonusRepository _employeeBonusRepository;

        public string Name { get; set; }
        public int EmployeeId { get; set; }
        public string TransactionNo { get; set; }
        public DateTime TransactionDate { get; set; }

        [Display(Name = "Bonus Points to Cash-out")]
        public int BonusPointRequested { get; set; }

        [Display(Name = "Maximum Cashable Bonus Points")]
        public int MaximumCashableBonusPoint { get; set; }

        public BonusRequestPageViewModel()
        {
        }

        public BonusRequestPageViewModel(IEmployeeRepository  employeeRepository,IEmployeeBonusRepository employeeBonusRepository,
            DateTime transactiondate,int employeeid)
        {
            _employeeRepository = employeeRepository;
            _employeeBonusRepository = employeeBonusRepository;
            EmployeeId = employeeid;
            TransactionDate = transactiondate;

            var employee = _employeeRepository.GetEmployeeInfo(EmployeeId);
            Name = employee.Name;
            MaximumCashableBonusPoint = _employeeBonusRepository.GetMaximumCashableBonusPoint(TransactionDate,
                                                                                              EmployeeId);

        }

         

        public void UpdateModel(BonusRequestDetails details)
        {
            details.BonusPointRequested = BonusPointRequested;
            details.EmployeeId = EmployeeId;
            details.TransactionDate = TransactionDate;
        }
    }
}