﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EAI.Model.Contracts;

namespace Website.Controllers
{
    
    public class BaseController : Controller
    {
        private readonly IUserRepository _userRepository;

        public BaseController(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public int UserId { get; set; }
        public string Theme { get; set; }


        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);
            if (User.Identity.IsAuthenticated)
            {
                var user = _userRepository.GetUserDetails(User.Identity.Name);
                UserId = user.UserId;
                Theme = user.Theme ?? "default";

                if (filterContext.HttpContext.Request.QueryString["theme"] != null)
                {
                    _userRepository.ChangeTheme(UserId, filterContext.HttpContext.Request.QueryString["theme"]);
                    Theme = filterContext.HttpContext.Request.QueryString["theme"];
                    //if (filterContext.HttpContext.Request.Cookies["theme"] != null)
                    //    filterContext.HttpContext.Response.Cookies.Remove("theme");

                    //var cookie = new HttpCookie("theme",
                    //                            filterContext.HttpContext.Request.QueryString["theme"] ?? "default");
                    //filterContext.HttpContext.Response.Cookies.Add(cookie);

                }
            }
            
            
                
            
        }

        
    }
}
