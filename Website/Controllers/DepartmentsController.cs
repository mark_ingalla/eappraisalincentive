﻿using System.Web.Mvc;
using EAI.Model.Contracts;
using EAI.Model.DTO;
using Telerik.Web.Mvc;

namespace Website.Controllers
{
    [Authorize(Roles = "Admin")]
    public class DepartmentsController : BaseController
    {

        private readonly IDepartmentRepository _departmentRepository;
        private readonly IUserRepository _userRepository;

        public DepartmentsController(IDepartmentRepository departmentRepository,IUserRepository userRepository) : base(userRepository)
        {
            _departmentRepository = departmentRepository;
            _userRepository = userRepository;
        }
        //
        // GET: /Positions/

        public ActionResult Index()
        {
            return View();
        }

        [GridAction]
        public ActionResult SelectIndex()
        {
            var data = _departmentRepository.GetAllDepartments();
            return View(new GridModel(data));
        }

        

        [AcceptVerbs(HttpVerbs.Post)]
        [GridAction]
        public ActionResult InsertDepartment()
        {
            
            var department = new DepartmentDetails();
            
            if (TryUpdateModel(department))
            {
                _departmentRepository.AddDepartment(department);
            }
            //Rebind the grid
            return View(new GridModel(_departmentRepository.GetAllDepartments()));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [GridAction]
        public ActionResult SaveDepartment(int id)
        {
            var department = _departmentRepository.GetDepartmentDetail(id);

            TryUpdateModel(department);
            _departmentRepository.Save(department);
            return View(new GridModel(_departmentRepository.GetAllDepartments()));
        }


        [AcceptVerbs(HttpVerbs.Post)]
        [GridAction]
        public ActionResult DeleteDepartment(int id)
        {
            var department = _departmentRepository.GetDepartmentDetail(id);
            if (department != null)
            {
                //Delete the record
                _departmentRepository.Delete(id);
            }

            //Rebind the grid
            return View(new GridModel(_departmentRepository.GetAllDepartments()));
        }
        //
        // GET: /Positions/Details/5

        public ActionResult Details(int id)
        {
            return View();
        }

        //
        // GET: /Positions/Create

        public ActionResult Create()
        {
            return View();
        } 

        //
        // POST: /Positions/Create

        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
        
        //
        // GET: /Positions/Edit/5
 
        public ActionResult Edit(int id)
        {
            return View();
        }

        //
        // POST: /Positions/Edit/5

        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here
 
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Positions/Delete/5
 
        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /Positions/Delete/5

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here
 
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
