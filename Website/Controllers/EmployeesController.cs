﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EAI.Model.Contracts;
using EAI.Model.DTO;
using Mvc.Mailer;
using Telerik.Web.Mvc;
using Website.Mailers;
using Website.ViewModels;

namespace Website.Controllers
{
    public class EmployeesController : BaseController
    {

        private readonly IEmployeeRepository _employeeRepository;
        private readonly IPositionRepository _positionRepository;
        private readonly IDepartmentRepository _departmentRepository;
        private readonly IUserRepository _userRepository;

        private readonly IUserMailer _userMailer;

        //private IUserMailer _userMailer = new UserMailer();
        //public IUserMailer UserMailer
        //{
        //    get { return _userMailer; }
        //    set { _userMailer = value; }
        //}


        public EmployeesController(IEmployeeRepository employeeRepository,IPositionRepository positionRepository,
            IDepartmentRepository departmentRepository,IUserRepository userRepository,
            IUserMailer userMailer) : base(userRepository)
        {
            _employeeRepository = employeeRepository;
            _positionRepository = positionRepository;
            _departmentRepository = departmentRepository;
            _userRepository = userRepository;

            _userMailer = userMailer;
        }

        //
        // GET: /Employees/
        [Authorize(Roles = "Admin")]
        public ActionResult Index()
        {
            var model = new EmployeeListPageViewModel();
            return View(model);
        }

        [GridAction]
        public ActionResult SelectEmployees(bool? active)
        {
            return View(new GridModel(_employeeRepository.GetEmployeeAllList(active)));
        }

        //
        // GET: /Employees/Details/5

        public ActionResult Details(int id)
        {
            return View();
        }

        //
        // GET: /Employees/Create
        [ActionName("create-employee")]
        [Authorize(Roles = "Admin")]
        public ActionResult Create()
        {
            var model = new EmployeeInputPageViewModel(_positionRepository, _departmentRepository, _employeeRepository);
            
            return View("Create",model);
        } 

        //
        // POST: /Employees/Create

        [HttpPost]
        [ActionName("create-employee")]
        public ActionResult Create(EmployeeInputPageViewModel model,FormCollection collection)
        {
            try
            {
                if (TryUpdateModel(model))
                {
                    var employeedetails = new EmployeeDetails();
                    model.UpdateModel(employeedetails);
                    _employeeRepository.AddEmployee(employeedetails);
                    _userMailer.Welcome(employeedetails.EmailAddress, employeedetails.User.Username,
                                        employeedetails.User.Password).Send();

                }
                return RedirectToAction("Index");
            }
            catch
            {
                return View("Create", model);
            }
        }
        
        //
        // GET: /Employees/Edit/5
        [ActionName("edit-employee")]
        public ActionResult Edit(int id)
        {
            var employee = _employeeRepository.GetEmployee(id);
            var model = new EmployeeInputPageViewModel(_positionRepository, _departmentRepository, _employeeRepository);
            model.GetModel(employee);
            return View("Edit",model);
        }

        //
        // POST: /Employees/Edit/5

        [HttpPost]
        [ActionName("edit-employee")]
        public ActionResult Edit(int id,EmployeeInputPageViewModel model, FormCollection collection)
        {
            try
            {
                if (TryUpdateModel(model))
                {
                    var employee = _employeeRepository.GetEmployee(id);
                    model.UpdateModel(employee);
                    _employeeRepository.UpdateEmployee(employee);

                    
                }
 
                return RedirectToAction("Index");
            }
            catch
            {
                return RedirectToAction("edit-employee", "Employees", new {id = model.ID});
            }
        }


        [ActionName("delete-employee")]
        public ActionResult Delete(int id)
        {
            var employee = _employeeRepository.GetEmployee(id);
            var model = new EmployeeDeletePageViewModel
                            {
                                Id = employee.ID,
                                EmployeeName = employee.EmployeeName,
                                EmployeeNo = employee.EmployeeNo
                            };
            
            return View("Delete", model);
        }

        [ActionName("delete-employee")]
        [HttpPost]
        public ActionResult Delete(EmployeeDeletePageViewModel model)
        {
            
           
            _employeeRepository.Inactivate(model.Id);

            return RedirectToAction("Index");
        }
        

        #region Ajax Calls / Partial views
        

        [HttpPost]
        public JsonResult CheckDuplicateEmployeeNo(string empno)
        {
            var data = _employeeRepository.GetEmployeeByNo(empno);
            var ifexist = data == null;
            return new JsonResult {Data = ifexist, JsonRequestBehavior = JsonRequestBehavior.AllowGet};


        }

        [HttpPost]
        public JsonResult CheckDuplicateEditEmployeeNo(int id,string empno)
        {
            var data = _employeeRepository.GetEmployeeByIDandNo(id,empno);
            var ifexist = data == null;
            return new JsonResult { Data = ifexist, JsonRequestBehavior = JsonRequestBehavior.AllowGet };


        }


        public JsonResult EnableEmployee(int id)
        {
            _employeeRepository.Activate(id);
            return new JsonResult{JsonRequestBehavior = JsonRequestBehavior.AllowGet,Data = id};
        }
        
        public JsonResult GetEmployeeById(int? id)
        {
            if (id == null)
            {
                return new JsonResult
                {
                    Data = null,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            else
            {
                var data = _employeeRepository.GetEmployeeInfo(id.Value);

                return new JsonResult
                           {
                               Data = data,
                               JsonRequestBehavior = JsonRequestBehavior.AllowGet
                           };
            }
        }

        public PartialViewResult RenderEmployeeDisplayForm(int? id)
        {
            if (id != null && id.Value > 0)
            {
                var employeeInfo = _employeeRepository.GetEmployeeInfo(id.Value);
                var model = new EmployeeListInfoPageViewModel
                                {
                                    BonusPointBalance = employeeInfo.BonusPointBalance,
                                    DateJoined = employeeInfo.DateJoined.ToShortDateString(),
                                    Department = employeeInfo.Department,
                                    EmployeeNo = employeeInfo.EmployeeNo,
                                    Grade = employeeInfo.Grade,
                                    Id = employeeInfo.Id,
                                    Name = employeeInfo.Name,
                                    Position = employeeInfo.Position,
                                    DateConfirmed = employeeInfo.DateConfirmed.HasValue ? employeeInfo.DateConfirmed.Value.ToShortDateString() : string.Empty
                                };
                return PartialView("EmployeeDisplayForm", model);
            }
            return PartialView("EmployeeDisplayForm", new EmployeeListInfoPageViewModel());
        }

        #endregion
    }
}
