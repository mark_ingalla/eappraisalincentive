﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EAI.Model.Contracts;
using EAI.Model.DTO;
using Telerik.Web.Mvc;
using Website.ViewModels;

namespace Website.Controllers
{
    public class BonusAdjustmentsController : BaseController
    {
        private readonly IEmployeeBonusRepository _employeeBonusRepository;
        private readonly IUserRepository _userRepository;
        private readonly IEmployeeRepository _employeeRepository;
        private readonly IEmployeeAppraisalRepository _employeeAppraisalRepository;
        private readonly IAppraisalRepository _appraisalRepository;


        public BonusAdjustmentsController(IUserRepository userRepository,IEmployeeBonusRepository employeeBonusRepository,IEmployeeRepository employeeRepository) : base(userRepository)
        {
            _employeeBonusRepository = employeeBonusRepository;
            _userRepository = userRepository;
            _employeeRepository = employeeRepository;
        }

        [Authorize(Roles = "Admin")]
        public ActionResult Index()
        {

            return View();
        }


        [GridAction]
        public ActionResult SelectEmployeeBonus()
        {
            var data = _employeeBonusRepository.GetEmployeeBonusList();
            return View(new GridModel(data));
        }

        [Authorize(Roles = "Admin")]
        [ActionName("create-bonusadjustment")]
        public ActionResult CreateBonusAdjustment()
        {
            var model = new EmployeeBonusAdjustmentInputPageViewModel(_employeeRepository, _employeeBonusRepository);
            return View("CreateBonusAdjustment",model);
        }

         [ActionName("create-bonusadjustment")]
        [HttpPost]
        public ActionResult CreateBonusAdjustment(EmployeeBonusAdjustmentInputPageViewModel model)
         {
             var bonusadjustment = new EmployeeBonusDetails();
             model.UpdateModel(bonusadjustment);
             
             _employeeBonusRepository.AddBonusAdjustment(bonusadjustment);
             return RedirectToAction("Index");
         }


        [ActionName("edit-bonusadjustment")]
        [Authorize(Roles = "Admin")]
        public ActionResult EditBonusAdjustment(int id)
        {
            var employeebonusdetail = _employeeBonusRepository.GetEmployeeBonusDetail(id);
            var model = new EmployeeBonusAdjustmentInputPageViewModel(_employeeRepository, _employeeBonusRepository);
            model.GetModel(employeebonusdetail);
            return View("EditBonusAdjustment",model);
        }

        [ActionName("edit-bonusadjustment")]
        [HttpPost]
        public ActionResult EditBonusAdjustment(EmployeeBonusAdjustmentInputPageViewModel model,FormCollection formCollection)
        {
            var employeebonusdetail = _employeeBonusRepository.GetEmployeeBonusDetail(model.ID);
            model.UpdateModel(employeebonusdetail);
            _employeeBonusRepository.SaveBonusAdjustment(employeebonusdetail);

            return RedirectToAction("Index");
            //return View("EditBonusAdjustment");
        }
        [Authorize(Roles = "User")]
        [ActionName("create-bonusrequest")]
        public ActionResult CreateBonusRequest()
        {
            var model = new BonusRequestPageViewModel(_employeeRepository, _employeeBonusRepository, DateTime.Now,
                                                      this.UserId);
                            
            return View("CreateBonusRequest", model);
        }

        [ActionName("create-bonusrequest")]
        [HttpPost]
        public ActionResult CreateBonusRequest(BonusRequestPageViewModel model)
        {
            var bonusrequest = new BonusRequestDetails();
            model.UpdateModel(bonusrequest);
            bonusrequest.EmployeeId = this.UserId;

            _employeeBonusRepository.AddBonusRequest(bonusrequest);

            return View("CreateBonusRequestSuccess", model);
        }


        public ActionResult MonthlyProcessing()
        {
            var model = new MonthlyProcessingPageViewModel
                            {
                                ProcessDate = DateTime.Now
                            };
            return View(model);
        }

        
        [HttpPost]
        public ActionResult MonthlyProcessing(MonthlyProcessingPageViewModel model)
        {
            _employeeBonusRepository.ProcessMonthlyCredit(model.ProcessDate);
            return View("ProcessSucess");
        }


        [GridAction]
        public ActionResult SelectMonthlyProcessEmployees(string dateTime)
        {
            var paraDate = DateTime.Parse(dateTime);
            var data = _employeeBonusRepository.GetEmployeesForMonthlyCredit(paraDate);
            return View(new GridModel(data));
        }


        
        public PartialViewResult RenderMaxCashableLimit(int id,string transdate)
        {
            var model = new BonusRequestPageViewModel(_employeeRepository, _employeeBonusRepository, Convert.ToDateTime(transdate),
                                                      id);
            return PartialView("BonusPointRequest", model);
        }


        /// <summary>
        /// list bonus request
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = "Admin")]
        public ActionResult BonusRequestList()
        {
            return View();
        }

        [GridAction]
        public ActionResult SelectEBonusRequests()
        {
            var data = _employeeBonusRepository.GetBonusRequests();
            return View(new GridModel(data));
        }



    }
}
