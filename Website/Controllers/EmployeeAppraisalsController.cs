﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EAI.Model.Contracts;
using EAI.Model.DTO;
using EAI.Model.Lookup;
using Telerik.Web.Mvc;
using Website.Services;
using Website.ViewModels;

namespace Website.Controllers
{
    public class EmployeeAppraisalsController : BaseController
    {
        private readonly IEmployeeAppraisalRepository _employeeAppraisalRepository;
        private readonly IEmployeeRepository _employeeRepository;
        private readonly IAppraisalRepository _appraisalRepository;
        private readonly IAppraisalWorkflowService _appraisalWorkflowService;

        private readonly IUserRepository _userRepository;

        public EmployeeAppraisalsController(IAppraisalWorkflowService appraisalWorkflowService,
            IEmployeeAppraisalRepository employeeAppraisalRepository,
            IUserRepository userRepository,
            IEmployeeRepository employeeRepository,
            IAppraisalRepository appraisalRepository) : base(userRepository)
        {
            _appraisalWorkflowService = appraisalWorkflowService;
            _employeeAppraisalRepository = employeeAppraisalRepository;
            _userRepository = userRepository;
            _employeeRepository = employeeRepository;
            _appraisalRepository = appraisalRepository;
        }
        //[Authorize(Roles = "Manager")]
        public ActionResult Index()
        {
            return View();
        }

        [GridAction]
        public ActionResult SelectEmployeeAppraisal(int id)
        {
            var data = _employeeAppraisalRepository.GetEmployeeAppraisalsInfoBySupervisorId(id);
            return View(new GridModel(data));
        }

        [ActionName("create-appraisal")]
        //[Authorize(Roles = "Manager")]
        public ActionResult CreateAppraisal()
        {
            var model = new EmployeeAppraisalInputPageViewModel(this.UserId,_employeeRepository, _employeeAppraisalRepository,
                                                                _appraisalRepository);
            model.CreateNewAppraisal();
           
            return View("CreateAppraisal",model);
        }

        [ActionName("create-appraisal")]
        [HttpPost]
        public ActionResult CreateAppraisal(EmployeeAppraisalInputPageViewModel model, FormCollection formCollection)
        {
            //var viewmodel = new EmployeeAppraisalInputPageViewModel(_employeeRepository, _employeeAppraisalRepository,
            //                                                   _appraisalRepository);

            var employee = new EmployeeAppraisalDetails();
            model.UpdateModel(employee,_employeeRepository);

            employee.EmployeeAppraisalItems = model.Dimensions.Select(x => new EmployeeAppraisalItemDetails
                                                                               {
                                                                                   AppraisalId = x.AppraisalId,
                                                                                   Comments = x.Comments,
                                                                                   Rating = x.Rating,
                                                                                   ParentId = x.ParentId,
                                                                                   Items = x.Items.Select(y=>new EmployeeAppraisalItemDetails
                                                                                                                 {
                                                                                                                     AppraisalId = y.AppraisalId,
                                                                                                                     Comments = y.Comments,
                                                                                                                     Rating = y.Rating,
                                                                                                                     ParentId = y.ParentId

                                                                                                                 }).ToList()
                                                                                   
                                                                               }).ToList();

            try
            {
                _employeeAppraisalRepository.AddEmployeeAppraisal(employee);
            }
            catch (Exception ex)
            {
                
                ModelState.AddModelError("",ex.Message);
                var newmodel = new EmployeeAppraisalInputPageViewModel(this.UserId, _employeeRepository, _employeeAppraisalRepository,
                                                                _appraisalRepository);
                
                newmodel.CreateNewAppraisal();
                newmodel.OverallRating = model.OverallRating;
                newmodel.SelectedEmployeeId = model.SelectedEmployeeId;
                //newmodel.Dimensions = model.Dimensions;
                newmodel.AppraisalDate = model.AppraisalDate;
                return View("CreateAppraisal",newmodel);
            }

            
            return RedirectToAction("Index");
            //viewmodel.CreateNewAppraisal();
            //return View("CreateAppraisal", viewmodel);
        }



        [ActionName("edit-appraisal")]
        //[Authorize(Roles = "Manager")]
        public ActionResult EditAppraisal(int id)
        {
            var appraisal = _employeeAppraisalRepository.GetEmployeeAppraisal(id);
            var model = new EmployeeAppraisalInputPageViewModel(this.UserId, _employeeRepository, _employeeAppraisalRepository,
                                                                _appraisalRepository);
            model.GetModel(appraisal);
            return View(model);
        }

        [ActionName("edit-appraisal")]
        [HttpPost]
        public ActionResult EditAppraisal(string btnSubmit, int id, EmployeeAppraisalInputPageViewModel model, FormCollection formCollection)
        {
            var employeeappraisal = _employeeAppraisalRepository.GetEmployeeAppraisal(id);
            model.UpdateModel(employeeappraisal,_employeeRepository);
            _employeeAppraisalRepository.SaveEmployeeAppraisal(employeeappraisal);


            if (btnSubmit == "Acknowledge")
            {
                
                _appraisalWorkflowService.Push(employeeappraisal);
            }

            return RedirectToAction("Index");
            //return View(model);
        }


        public ActionResult ApproveAppraisal(int id)
        {
            var activeuser = _employeeAppraisalRepository.GetActiveUserForEmployeeAppraisal(id);
            if (activeuser != this.UserId)
                return View("UnauthorizedAppraisalApproval");



            var employeeappraisal = _employeeAppraisalRepository.GetEmployeeAppraisal(id);
            var employee = _employeeRepository.GetEmployee(employeeappraisal.EmployeeId);
            var model = new EmployeeAppraisalApprovalPageViewModel
                            {
                                Id = id,
                                AppraisalNo = employeeappraisal.TransactionNo,
                                EmployeeName = employee.EmployeeName,
                                Status = Enum.GetName(typeof(EmployeeAppraisalStatus), employeeappraisal.Status)
                            };
            return View(model);
        }

        [HttpPost]
        public ActionResult ApproveAppraisal(int id,EmployeeAppraisalApprovalPageViewModel model)
        {
            var employeeAppraisal = _employeeAppraisalRepository.GetEmployeeAppraisal(id);

            if (ModelState.IsValid)
            {
                model.UpdateModel(employeeAppraisal);
                _appraisalWorkflowService.Push(employeeAppraisal);    
            }

            return RedirectToAction("Index", "Home");
            //return View(model);
        }

    }
}
