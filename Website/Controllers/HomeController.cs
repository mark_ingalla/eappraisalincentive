﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EAI.Model.Contracts;
using Telerik.Web.Mvc;

namespace Website.Controllers
{
    [Authorize]
    public class HomeController : BaseController
    {
        private readonly IEmployeeAppraisalRepository _employeeAppraisalRepository;

        public HomeController(IUserRepository userRepository, IEmployeeAppraisalRepository employeeAppraisalRepository)
            : base(userRepository)
        {
            _employeeAppraisalRepository = employeeAppraisalRepository;
        }

        public ActionResult Index()
        {
            ViewBag.Message = "Welcome to ASP.NET MVC!";

            ViewBag.UserId = this.UserId;
            return View();
        }

        public ActionResult About()
        {
            return View();
        }

        public ActionResult RenderActionLink()
        {
            var data = _employeeAppraisalRepository.GetEmployeeAppraisalNeedAction(this.UserId);

            if (data.Count > 0)
                return PartialView("EmployeeAppraisalLink", data.FirstOrDefault().ID);
            return Content(string.Empty);
        }

        [GridAction]
        public ActionResult GetEmployeeAppraisalNeedAction(int id)
        {
            var data = _employeeAppraisalRepository.GetEmployeeAppraisalNeedAction(id);
            return View(new GridModel(data));
        }
    }
}
