﻿using System.Web.Mvc;
using EAI.Model.Contracts;
using EAI.Model.DTO;
using Telerik.Web.Mvc;

namespace Website.Controllers
{
    [Authorize(Roles = "Admin")]
    public class PositionsController : BaseController
    {

        private readonly IPositionRepository _positionRepository;
        private readonly IUserRepository _userRepository;

        public PositionsController(IPositionRepository positionRepository,IUserRepository userRepository) : base(userRepository)
        {
            _positionRepository = positionRepository;
            _userRepository = userRepository;
        }
        //
        // GET: /Positions/

        public ActionResult Index()
        {
            return View();
        }

        [GridAction]
        public ActionResult SelectIndex()
        {
            var data = _positionRepository.GetAllPositions();
            return View(new GridModel(data));
        }

        

        [AcceptVerbs(HttpVerbs.Post)]
        [GridAction]
        public ActionResult InsertPosition()
        {
            
            var position = new PositionDetails();
            
            if (TryUpdateModel(position))
            {
                _positionRepository.AddPosition(position);
            }
            //Rebind the grid
            return View(new GridModel(_positionRepository.GetAllPositions()));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [GridAction]
        public ActionResult SavePosition(int id)
        {
            var position = _positionRepository.GetPositionDetail(id);

            TryUpdateModel(position);
            _positionRepository.Save(position);
            return View(new GridModel(_positionRepository.GetAllPositions()));
        }


        [AcceptVerbs(HttpVerbs.Post)]
        [GridAction]
        public ActionResult DeletePosition(int id)
        {
            var position = _positionRepository.GetPositionDetail(id);
            if (position != null)
            {
                //Delete the record
                _positionRepository.Delete(id);
            }

            //Rebind the grid
            return View(new GridModel(_positionRepository.GetAllPositions()));
        }
        //
        // GET: /Positions/Details/5

        public ActionResult Details(int id)
        {
            return View();
        }

        //
        // GET: /Positions/Create

        public ActionResult Create()
        {
            return View();
        } 

        //
        // POST: /Positions/Create

        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
        
        //
        // GET: /Positions/Edit/5
 
        public ActionResult Edit(int id)
        {
            return View();
        }

        //
        // POST: /Positions/Edit/5

        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here
 
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Positions/Delete/5
 
        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /Positions/Delete/5

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here
 
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
