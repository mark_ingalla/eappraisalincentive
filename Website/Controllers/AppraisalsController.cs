﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EAI.Model.Contracts;
using EAI.Model.DTO;
using Telerik.Web.Mvc;

namespace Website.Controllers
{
    [Authorize(Roles = "Admin")]
    public class AppraisalsController : BaseController
    {

        private readonly IAppraisalRepository _appraisalRepository;
        private readonly IUserRepository _userRepository;

        public AppraisalsController(IAppraisalRepository appraisalRepository,IUserRepository userRepository ) : base(userRepository)
        {
            _appraisalRepository = appraisalRepository;
            _userRepository = userRepository;

        }


        #region Admin Setup
        

        public ActionResult Index()
        {
            return View();
        }

        [GridAction]
        public ActionResult _AppraisalHeaders()
        {
            var data = _appraisalRepository.GetAppraisalHeaders();
            return View(new GridModel(data));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [GridAction]
        public ActionResult _AppraisalHeadersInsert()
        {

            var headerDetails = new AppraisalHeaderDetails();

            if (TryUpdateModel(headerDetails))
            {
                _appraisalRepository.AddAppraisalHeader(headerDetails);
            }
            //Rebind the grid
            return View(new GridModel(_appraisalRepository.GetAppraisalHeaders()));
        }
        

        [AcceptVerbs(HttpVerbs.Post)]
        [GridAction]
        public ActionResult _AppraisalItemsInsert(int id)
        {

            var itemDetails = new AppraisalItemDetails();

            if (TryUpdateModel(itemDetails))
            {
                itemDetails.HeaderId = id;
                _appraisalRepository.AddAppraisalItem(itemDetails);
            }
            //Rebind the grid
            return View(new GridModel(_appraisalRepository.GetAppraisalItemsById(id)));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [GridAction]
        public ActionResult _AppraisalHeadersUpdate(int id)
        {
            var header = _appraisalRepository.GetAppraisalHeader(id);
            if (TryUpdateModel(header))
            {
                _appraisalRepository.SaveAppraisalHeader(header);
            }
            return View(new GridModel(_appraisalRepository.GetAppraisalHeaders()));
        }


        [AcceptVerbs(HttpVerbs.Post)]
        [GridAction]
        public ActionResult _AppraisalItemsUpdate(int id)
        {
            var item = _appraisalRepository.GetAppraisalItem(id);
            if (TryUpdateModel(item))
            {
                _appraisalRepository.SaveAppraisalItem(item);
            }
            return View(new GridModel(_appraisalRepository.GetAppraisalItemsById(item.HeaderId)));
        }




        [GridAction]
        public ActionResult _AppraisalItems(int id)
        {
            var data = _appraisalRepository.GetAppraisalItemsById(id);
            return View(new GridModel(data));
        }
        

        public JsonResult EnableDisableHeader(int id)
        {
            _appraisalRepository.EnableDisableHeader(id);
            return new JsonResult
                       {
                           JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                           Data = id
                       };
        }
        public JsonResult EnableDisableItem(int id)
        {
            _appraisalRepository.EnableDisableItem(id);
            return new JsonResult
            {
                JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                Data = id
            };
        }
        #endregion

       


    }
}
