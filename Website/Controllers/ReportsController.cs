﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using EAI.Model.Contracts;
using EAI.Model.DTO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using Telerik.Web.Mvc;

namespace Website.Controllers
{
    public class ReportsController : BaseController
    {

        private readonly IReportsRepository _reportsRepository;
        private readonly IUserRepository _userRepository;
        
        public ReportsController(IReportsRepository reportsRepository,IUserRepository userRepository) : base(userRepository)
        {
            _reportsRepository = reportsRepository;
            _userRepository = userRepository;
        }


        public ActionResult QuarterlyAppraisal(string fromDate, string toDate,string format)
        {
            var data =
                    _reportsRepository.QuarterlyAppraisalIncentives(new ReportFilter
                    {
                        FromDate = Convert.ToDateTime(fromDate),
                        ToDate = Convert.ToDateTime(toDate)
                    });
            if (data.Count <= 0) return View();
            switch (format)
            {
                case "csv":
                    
                 
                var output = new MemoryStream();
                var writer = new StreamWriter(output, Encoding.UTF8);
                writer.Write("EmployeeNo,");
                writer.Write("Employee Name,");
                writer.Write("Position,");
                writer.Write("Department,");
                writer.Write("DateJoined,");
                writer.Write("DateConfirmed,");
                writer.Write("OverallRating");
                writer.WriteLine();
                foreach (var item in data)
                {
                    writer.Write(item.EmployeeNo);
                    writer.Write(",");
                    writer.Write("\"");
                    writer.Write(item.EmployeeName);
                    writer.Write("\"");
                    writer.Write(",");
                    writer.Write("\"");
                    writer.Write(item.Position);
                    writer.Write("\"");
                    writer.Write(",");
                    writer.Write("\"");
                    writer.Write(item.Department);
                    writer.Write("\"");
                    writer.Write(",");
                    writer.Write("\"");
                    writer.Write(item.DateJoined.ToShortDateString());
                    writer.Write("\"");
                    writer.Write(",");
                    writer.Write("\"");
                    writer.Write(item.DateConfirmed.HasValue ? item.DateConfirmed.Value.ToShortDateString() : string.Empty);
                    writer.Write("\"");
                    writer.Write(",");                    
                    writer.Write(item.OverallRating);
                    writer.WriteLine();
                }
                writer.Flush();
                output.Position = 0;
            
                    return File(output, "text/comma-separated-values", "QuarertlyAppraisals.csv");

                case "pdf":
                    var document = new Document(PageSize.A4.Rotate(), 10, 10, 10, 10);
                    var pdfoutput = new MemoryStream();
                    PdfWriter.GetInstance(document, pdfoutput);
                    document.Open();
                    var dataTable = new PdfPTable(7);
                    dataTable.DefaultCell.Padding = 3;
                    dataTable.DefaultCell.BorderWidth = 2;
                    dataTable.DefaultCell.HorizontalAlignment = Element.ALIGN_CENTER;

                    dataTable.AddCell("EmployeeNo");
                    dataTable.AddCell("Employee Name");
                    dataTable.AddCell("Position");
                    dataTable.AddCell("Department");
                    dataTable.AddCell("Date Joined");
                    dataTable.AddCell("Date Confirmed");
                    dataTable.AddCell("Overall Rating");


                    dataTable.HeaderRows = 1;
                    dataTable.DefaultCell.BorderWidth = 1;

                    foreach (var rptQuarterlyAppraisal in data)
                    {
                        dataTable.AddCell(rptQuarterlyAppraisal.EmployeeNo);
                        dataTable.AddCell(rptQuarterlyAppraisal.EmployeeName);
                        dataTable.AddCell(rptQuarterlyAppraisal.Position);
                        dataTable.AddCell(rptQuarterlyAppraisal.Department);
                        dataTable.AddCell(rptQuarterlyAppraisal.DateJoined.ToString());
                        dataTable.AddCell(rptQuarterlyAppraisal.DateConfirmed.ToString());
                        dataTable.AddCell(rptQuarterlyAppraisal.OverallRating.ToString());
                    }
                    document.Add(dataTable);
                    document.Close();
                    
                    return File(pdfoutput.ToArray(), "application/pdf", "QuarertlyAppraisals.pdf");
                    
                default:
                    return View();
            }
        }

        [GridAction]
        public ActionResult QuarterlyAppraisalGrid(string fromDate,string toDate)
        {
            var data =
                _reportsRepository.QuarterlyAppraisalIncentives(new ReportFilter
                                                                    {
                                                                        FromDate = Convert.ToDateTime(fromDate),
                                                                        ToDate = Convert.ToDateTime(toDate)
                                                                    });
            return View(new GridModel(data));
        }

        public ActionResult BonusPointAdjustment(string fromDate, string toDate, string format)
        {
            var data =
                    _reportsRepository.BonusAdjustments(new ReportFilter
                    {
                        FromDate = Convert.ToDateTime(fromDate),
                        ToDate = Convert.ToDateTime(toDate)
                    });
            if (data.Count <= 0) return View();
            switch (format)
            {
                case "csv":
                    
                    
                    var output = new MemoryStream();
                    var writer = new StreamWriter(output, Encoding.UTF8);
                    writer.Write("EmployeeNo,");
                    writer.Write("EmployeeName,");
                    writer.Write("Transaction No,");
                    writer.Write("Transaction Date,");
                    writer.Write("Credit / Debit Bonus Points,");
                    writer.Write("Transaction Description,");
                    writer.Write("Updated By");
                    writer.WriteLine();
                    foreach (var item in data)
                    {
                        writer.Write(item.EmployeeNo);
                        writer.Write(",");
                        writer.Write("\"");
                        writer.Write(item.EmployeeName);
                        writer.Write("\"");
                        writer.Write(",");
                        writer.Write("\"");
                        writer.Write(item.TransactionNo);
                        writer.Write("\"");
                        writer.Write(",");
                        writer.Write("\"");
                        writer.Write(item.TransactionDate.ToShortDateString());
                        writer.Write("\"");
                        writer.Write(",");
                        writer.Write("\"");
                        writer.Write(item.CreditDebitPoints);
                        writer.Write("\"");
                        writer.Write(",");
                        writer.Write("\"");
                        writer.Write(item.TransactionDescription);
                        writer.Write("\"");
                        writer.Write(",");
                        writer.Write(item.UpdatedBy);
                        writer.WriteLine();
                    }
                    writer.Flush();
                    output.Position = 0;

                    return File(output, "text/comma-separated-values", "BonusAdjustments.csv");

                case "pdf":
                    var document = new Document(PageSize.A4.Rotate(), 10, 10, 10, 10);
                    var pdfoutput = new MemoryStream();
                    PdfWriter.GetInstance(document, pdfoutput);
                    document.Open();
                    var dataTable = new PdfPTable(7);
                    dataTable.DefaultCell.Padding = 3;
                    dataTable.DefaultCell.BorderWidth = 2;
                    dataTable.DefaultCell.HorizontalAlignment = Element.ALIGN_CENTER;

                   
                     dataTable.AddCell("EmployeeNo,");
                     dataTable.AddCell("EmployeeName,");
                     dataTable.AddCell("Transaction No,");
                     dataTable.AddCell("Transaction Date,");
                     dataTable.AddCell("Credit / Debit Bonus Points,");
                     dataTable.AddCell("Transaction Description,");
                     dataTable.AddCell("Updated By");

                    dataTable.HeaderRows = 1;
                    dataTable.DefaultCell.BorderWidth = 1;

                    foreach (var rptQuarterlyAppraisal in data)
                    {
                        dataTable.AddCell(rptQuarterlyAppraisal.EmployeeNo);
                        dataTable.AddCell(rptQuarterlyAppraisal.EmployeeName);
                        dataTable.AddCell(rptQuarterlyAppraisal.TransactionNo);
                        dataTable.AddCell(rptQuarterlyAppraisal.TransactionDate.ToString());
                        dataTable.AddCell(rptQuarterlyAppraisal.CreditDebitPoints.ToString());
                        dataTable.AddCell(rptQuarterlyAppraisal.TransactionDescription);
                        dataTable.AddCell(rptQuarterlyAppraisal.UpdatedBy);
                    }
                    document.Add(dataTable);
                    document.Close();

                    return File(pdfoutput.ToArray(), "application/pdf", "BonusAdjustments.pdf");
                default:
                    return View();
            }
        }

        [GridAction]
        public ActionResult BonusPointAdjustmentGrid(string fromDate, string toDate)
        {
            var data =
                _reportsRepository.BonusAdjustments(new ReportFilter
                {
                    FromDate = Convert.ToDateTime(fromDate),
                    ToDate = Convert.ToDateTime(toDate)
                });
            return View(new GridModel(data));
        }

        public ActionResult BonusPointRequests(string fromDate, string toDate, string format)
        {
            var data =
                    _reportsRepository.BonusPointRequests(new ReportFilter
                    {
                        FromDate = Convert.ToDateTime(fromDate),
                        ToDate = Convert.ToDateTime(toDate)
                    });
            if (data.Count <= 0) return View();
            switch (format)
            {
                case "csv":
                    
                    var output = new MemoryStream();
                    var writer = new StreamWriter(output, Encoding.UTF8);
                    writer.Write("EmployeeNo,");
                    writer.Write("EmployeeName,");
                    writer.Write("Months of Service,");
                    writer.Write("Transaction No,");
                    writer.Write("Transaction Date,");
                    writer.Write("Maximum Cashable Bonus,");
                    writer.Write("Cashout Bonus Point");
                    writer.WriteLine();
                    foreach (var item in data)
                    {
                        writer.Write(item.EmployeeNo);
                        writer.Write(",");
                        writer.Write("\"");
                        writer.Write(item.EmployeeName);
                        writer.Write("\"");
                        writer.Write(",");
                        writer.Write("\"");
                        writer.Write(item.MonthsofService);
                        writer.Write("\"");
                        writer.Write(",");
                        writer.Write("\"");
                        writer.Write(item.TransactionNo);
                        writer.Write("\"");
                        writer.Write(",");
                        writer.Write("\"");
                        writer.Write(item.TransactionDate.ToShortDateString());
                        writer.Write("\"");
                        writer.Write(",");
                        writer.Write("\"");
                        writer.Write(item.MaximumCashableBonusPoint);
                        writer.Write("\"");
                        writer.Write(",");
                        writer.Write(item.CashoutBonusPoint);
                        writer.WriteLine();
                    }
                    writer.Flush();
                    output.Position = 0;

                    return File(output, "text/comma-separated-values", "BonusPointRequests.csv");

                case "pdf":
                    var document = new Document(PageSize.A4.Rotate(), 10, 10, 10, 10);
                    var pdfoutput = new MemoryStream();
                    PdfWriter.GetInstance(document, pdfoutput);
                    document.Open();
                    var dataTable = new PdfPTable(7);
                    dataTable.DefaultCell.Padding = 3;
                    dataTable.DefaultCell.BorderWidth = 2;
                    dataTable.DefaultCell.HorizontalAlignment = Element.ALIGN_CENTER;

                          dataTable.AddCell("EmployeeNo,");
                     dataTable.AddCell("EmployeeName,");
                     dataTable.AddCell("Months of Service,");
                     dataTable.AddCell("Transaction No,");
                     dataTable.AddCell("Transaction Date,");
                     dataTable.AddCell("Maximum Cashable Bonus,");
                     dataTable.AddCell("Cashout Bonus Point");

                    dataTable.HeaderRows = 1;
                    dataTable.DefaultCell.BorderWidth = 1;

                    foreach (var rptQuarterlyAppraisal in data)
                    {
                        dataTable.AddCell(rptQuarterlyAppraisal.EmployeeNo);
                        dataTable.AddCell(rptQuarterlyAppraisal.EmployeeName);
                        dataTable.AddCell(rptQuarterlyAppraisal.MonthsofService.ToString());
                        dataTable.AddCell(rptQuarterlyAppraisal.TransactionNo);
                        dataTable.AddCell(rptQuarterlyAppraisal.TransactionDate.ToString());
                        dataTable.AddCell(rptQuarterlyAppraisal.MaximumCashableBonusPoint.ToString());
                        dataTable.AddCell(rptQuarterlyAppraisal.CashoutBonusPoint.ToString());
                    }
                    document.Add(dataTable);
                    document.Close();

                    return File(pdfoutput.ToArray(), "application/pdf", "BonusPointRequests.pdf");
                default:
                    return View();
            }
        }

        [GridAction]
        public ActionResult BonusPointRequestsGrid(string fromDate, string toDate)
        {
            var data =
                _reportsRepository.BonusPointRequests(new ReportFilter
                {
                    FromDate = Convert.ToDateTime(fromDate),
                    ToDate = Convert.ToDateTime(toDate)
                });
            return View(new GridModel(data));
        }



        public ActionResult BonusPointStatus(string fromDate, string toDate, string format)
        {
            var data =
                   _reportsRepository.BonusPointStatus(new ReportFilter
                   {
                       FromDate = Convert.ToDateTime(fromDate),
                       ToDate = Convert.ToDateTime(toDate)
                   });
            if (data.Count <= 0) return View();
            switch (format)
            {
                case "csv":
                   
                    var output = new MemoryStream();
                    var writer = new StreamWriter(output, Encoding.UTF8);
                    writer.Write("EmployeeNo,");
                    writer.Write("EmployeeName,");
                    writer.Write("Transaction No,");
                    writer.Write("Transaction Date,");
                    writer.Write("Credit / Debit Bonus Points,");
                    writer.Write("Transaction Description,");
                    writer.Write("Bonus Point Balance");
                    writer.WriteLine();
                    foreach (var item in data)
                    {
                        writer.Write(item.EmployeeNo);
                        writer.Write(",");
                        writer.Write("\"");
                        writer.Write(item.EmployeeName);
                        writer.Write("\"");
                        writer.Write(",");
                        writer.Write("\"");
                        writer.Write(item.TransactionNo);
                        writer.Write("\"");
                        writer.Write(",");
                        writer.Write("\"");
                        writer.Write(item.TransactionDate.ToShortDateString());
                        writer.Write("\"");
                        writer.Write(",");
                        writer.Write("\"");
                        writer.Write(item.CreditDebitPoints);
                        writer.Write("\"");
                        writer.Write(",");
                        writer.Write("\"");
                        writer.Write(item.TransactionDescription);
                        writer.Write(",");
                        writer.Write("\"");
                        writer.Write(item.BonusPointBalance);
                        writer.WriteLine();
                    }
                    writer.Flush();
                    output.Position = 0;

                    return File(output, "text/comma-separated-values", "BonusPointStatus.csv");

                case "pdf":
                    var document = new Document(PageSize.A4.Rotate(), 10, 10, 10, 10);
                    var pdfoutput = new MemoryStream();
                    PdfWriter.GetInstance(document, pdfoutput);
                    document.Open();
                    var dataTable = new PdfPTable(7);
                    dataTable.DefaultCell.Padding = 3;
                    dataTable.DefaultCell.BorderWidth = 2;
                    dataTable.DefaultCell.HorizontalAlignment = Element.ALIGN_CENTER;

                      dataTable.AddCell("EmployeeNo,");
                     dataTable.AddCell("EmployeeName,");
                     dataTable.AddCell("Transaction No,");
                     dataTable.AddCell("Transaction Date,");
                     dataTable.AddCell("Credit / Debit Bonus Points,");
                     dataTable.AddCell("Transaction Description,");
                     dataTable.AddCell("Bonus Point Balance");

                    dataTable.HeaderRows = 1;
                    dataTable.DefaultCell.BorderWidth = 1;

                    foreach (var rptQuarterlyAppraisal in data)
                    {
                        dataTable.AddCell(rptQuarterlyAppraisal.EmployeeNo);
                        dataTable.AddCell(rptQuarterlyAppraisal.EmployeeName);                        
                        dataTable.AddCell(rptQuarterlyAppraisal.TransactionNo);
                        dataTable.AddCell(rptQuarterlyAppraisal.TransactionDate.ToString());
                        dataTable.AddCell(rptQuarterlyAppraisal.CreditDebitPoints.ToString());
                        dataTable.AddCell(rptQuarterlyAppraisal.TransactionDescription);
                        dataTable.AddCell(rptQuarterlyAppraisal.BonusPointBalance.ToString());
                    }
                    document.Add(dataTable);
                    document.Close();

                    return File(pdfoutput.ToArray(), "application/pdf", "BonusPointStatus.pdf");
                default:
                    return View();
            }
        }

        [GridAction]
        public ActionResult BonusPointStatusGrid(string fromDate, string toDate)
        {
            var data =
                _reportsRepository.BonusPointStatus(new ReportFilter
                {
                    FromDate = Convert.ToDateTime(fromDate),
                    ToDate = Convert.ToDateTime(toDate)
                });
            return View(new GridModel(data));
        }
        public ActionResult MonthlyBonusPoint(string fromDate, string toDate, string format)
        {
            var data =
                    _reportsRepository.MontlyCreditBonusPoints(new ReportFilter
                    {
                        FromDate = Convert.ToDateTime(fromDate),
                        ToDate = Convert.ToDateTime(toDate)
                    });
            if (data.Count <= 0) return View();
            switch (format)
            {
                case "csv":
                    
                    var output = new MemoryStream();
                    var writer = new StreamWriter(output, Encoding.UTF8);
                    writer.Write("EmployeeNo,");
                    writer.Write("EmployeeName,");
                    writer.Write("Last Quarter Overall Rating,");
                    writer.Write("Transaction No,");
                    writer.Write("Transaction Date,");
                    writer.Write("Bonus Point Balance");
                    writer.WriteLine();
                    foreach (var item in data)
                    {
                        writer.Write(item.EmployeeNo);
                        writer.Write(",");
                        writer.Write("\"");
                        writer.Write(item.EmployeeName);
                        writer.Write("\"");
                        writer.Write(",");
                        writer.Write("\"");
                        writer.Write(item.LastQuarterOverallRating);
                        writer.Write("\"");
                        writer.Write(",");
                        writer.Write("\"");
                        writer.Write(item.TransactionNo);
                        writer.Write("\"");
                        writer.Write(",");
                        writer.Write("\"");
                        writer.Write(item.TransactionDate.ToShortDateString());
                        writer.Write("\"");
                        writer.Write(",");
                        writer.Write(item.BonusPointBalance);
                        writer.WriteLine();
                    }
                    writer.Flush();
                    output.Position = 0;

                    return File(output, "text/comma-separated-values", "MonthlyBonusPoint.csv");
                case "pdf":
                    var document = new Document(PageSize.A4.Rotate(), 10, 10, 10, 10);
                    var pdfoutput = new MemoryStream();
                    PdfWriter.GetInstance(document, pdfoutput);
                    document.Open();
                    var dataTable = new PdfPTable(6);
                    dataTable.DefaultCell.Padding = 3;
                    dataTable.DefaultCell.BorderWidth = 2;
                    dataTable.DefaultCell.HorizontalAlignment = Element.ALIGN_CENTER;

                       dataTable.AddCell("EmployeeNo,");
                     dataTable.AddCell("EmployeeName,");
                     dataTable.AddCell("Last Quarter Overall Rating,");
                     dataTable.AddCell("Transaction No,");
                     dataTable.AddCell("Transaction Date,");
                     dataTable.AddCell("Bonus Point Balance");

                    dataTable.HeaderRows = 1;
                    dataTable.DefaultCell.BorderWidth = 1;

                    foreach (var rptQuarterlyAppraisal in data)
                    {
                        dataTable.AddCell(rptQuarterlyAppraisal.EmployeeNo);
                        dataTable.AddCell(rptQuarterlyAppraisal.EmployeeName);
                        dataTable.AddCell(rptQuarterlyAppraisal.LastQuarterOverallRating.ToString());
                        dataTable.AddCell(rptQuarterlyAppraisal.TransactionNo);
                        dataTable.AddCell(rptQuarterlyAppraisal.TransactionDate.ToString());
                        dataTable.AddCell(rptQuarterlyAppraisal.BonusPointBalance.ToString());
                    }
                    document.Add(dataTable);
                    document.Close();

                    return File(pdfoutput.ToArray(), "application/pdf", "MonthlyBonusPoint.pdf");
                default:
                    return View();
            }
        }

        [GridAction]
        public ActionResult MonthlyBonusPointGrid(string fromDate, string toDate)
        {
            var data =
                _reportsRepository.MontlyCreditBonusPoints(new ReportFilter
                {
                    FromDate = Convert.ToDateTime(fromDate),
                    ToDate = Convert.ToDateTime(toDate)
                });
            return View(new GridModel(data));
        }

      





    }
}
