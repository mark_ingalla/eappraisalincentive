﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Website.Framework
{
    public static class HtmlExtensions
    {
        public static SelectList ToSelectList<TEnum>(this TEnum enumObj)
        {
            var values = from TEnum e in Enum.GetValues(typeof(TEnum))
                         select new { Id = (int) Enum.Parse(typeof(TEnum),e.ToString()), Name = e.ToString() };

            return new SelectList(values, "Id", "Name", enumObj);
        }

        public static SelectList ToSelectListString<TEnum>(this TEnum enumObj)
        {
            var values = from TEnum e in Enum.GetValues(typeof(TEnum))
                         select new { Id = e.ToString(), Name = e.ToString() };

            return new SelectList(values, "Id", "Name", enumObj);
        }

        public static MvcHtmlString LabelFor<TModel, TValue>(this HtmlHelper<TModel> html, Expression<Func<TModel, TValue>> expression, object htmlAttributes)
        {
            return LabelFor(html, expression, new RouteValueDictionary(htmlAttributes));
        }
        public static MvcHtmlString LabelFor<TModel, TValue>(this HtmlHelper<TModel> html, Expression<Func<TModel, TValue>> expression, IDictionary<string, object> htmlAttributes)
        {
            ModelMetadata metadata = ModelMetadata.FromLambdaExpression(expression, html.ViewData);
            string htmlFieldName = ExpressionHelper.GetExpressionText(expression);
            string labelText = metadata.DisplayName ?? metadata.PropertyName ?? htmlFieldName.Split('.').Last();
            if (String.IsNullOrEmpty(labelText))
            {
                return MvcHtmlString.Empty;
            }

            TagBuilder tag = new TagBuilder("label");
            tag.MergeAttributes(htmlAttributes);
            tag.Attributes.Add("for", html.ViewContext.ViewData.TemplateInfo.GetFullHtmlFieldId(htmlFieldName));
            tag.SetInnerText(labelText);
            return MvcHtmlString.Create(tag.ToString(TagRenderMode.Normal));
        }

        public static string GetCurrentTheme(this HtmlHelper html)
        {
            //if (html.ViewContext.HttpContext.Request.Cookies["theme"] == null)
            //if (HttpContext.Current.Request.Cookies["theme"] == null || string.IsNullOrWhiteSpace(HttpContext.Current.Request.Cookies["theme"].Value))
            //{
            //    var cookie = new HttpCookie("theme", HttpContext.Current.Request.QueryString["theme"] ?? "default");  
            //    HttpContext.Current.Response.Cookies.Add(cookie);
            //}


            if (HttpContext.Current.Request.Cookies["theme"] != null)
                return  HttpContext.Current.Request.Cookies["theme"].Value ?? "default";
            return html.ViewContext.HttpContext.Request.QueryString["theme"] ?? "default";
        }
    }
}