﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;
using EAI.Model;
using EAI.Model.Contracts;
using EAI.Model.Repositories;

namespace Website.Models
{
    public class EAIIdentity : IIdentity
    {

        private readonly IEmployeeRepository _employeeRepository;
        #region Implementation of IIdentity

        public EAIIdentity(IEmployeeRepository employeeRepository, string username)
        {
            _employeeRepository = employeeRepository;
            var employee = _employeeRepository.GetEmployeeByUserName(username);
            if (employee == null) return;
            Name = employee.UserName;
            AuthenticationType = "custom";
            IsAuthenticated = true;
        }

        public string Name { get; private set; }

        public string AuthenticationType { get; private set; }

        public bool IsAuthenticated { get; private set; }

        #endregion
    }

}