﻿using System;
using System.Security.Principal;
using System.Web;
using EAI.Model;
using EAI.Model.Contracts;
using EAI.Model.Repositories;
using Ninject;
using Ninject.Activation;

namespace Website.Models
{
    public class EAIUser : IPrincipal
    {


        private readonly IEmployeeRepository _employeeRepository = new EmployeeRepository(new EAIContext());
        public string Username { get; private set; }




        public EAIUser( string username)
        {
           // _employeeRepository = employeeRepository;
            Username = username;
            //  Username = HttpContext.Current.User.Identity.Name;
        }

        #region Implementation of IPrincipal

        public bool IsInRole(string role)
        {
            var employee = _employeeRepository.GetEmployeeByUserName(Username);

            return employee.Role == role;
        }

        public IIdentity Identity
        {
            get
            {
                var identity = new EAIIdentity(_employeeRepository,Username);

                return identity;
            }
        }

        #endregion
    }

   
}
