using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Mvc.Mailer;
using System.Net.Mail;

namespace Website.Mailers
{ 
    public interface IUserMailer
    {
				
		MailMessage Welcome(string emailto,string username,string password);
		
				
		MailMessage PasswordReset();
		
		
	}
}