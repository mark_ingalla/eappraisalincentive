using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using EAI.Model.DTO;
using Mvc.Mailer;
using System.Net.Mail;

namespace Website.Mailers
{ 
    public interface IAppraisalNotifier
    {

        MailMessage NotifyEmployee(EmployeeAppraisalMailDTO appraisalMailDTO);


        MailMessage NotifySupervisor(EmployeeAppraisalMailDTO appraisalMailDTO);


        MailMessage NotifyBusinessUnitHead(EmployeeAppraisalMailDTO appraisalMailDTO);
		
		
	}
}