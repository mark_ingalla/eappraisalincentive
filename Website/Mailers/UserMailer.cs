using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Mvc.Mailer;
using System.Net.Mail;

namespace Website.Mailers
{ 
    public class UserMailer : MailerBase, IUserMailer     
	{
		public UserMailer():
			base()
		{
			MasterName="_Layout";
		}


        public virtual MailMessage Welcome(string emailto, string username, string password)
		{
			var mailMessage = new MailMessage{Subject = "Welcome"};
			
			mailMessage.To.Add(emailto);
			ViewBag.Username = username;
            ViewBag.Password = password;
			PopulateBody(mailMessage, viewName: "Welcome");

			return mailMessage;
		}

		
		public virtual MailMessage PasswordReset()
		{
			var mailMessage = new MailMessage{Subject = "PasswordReset"};
			
			//mailMessage.To.Add("some-email@example.com");
			//ViewBag.Data = someObject;
			PopulateBody(mailMessage, viewName: "PasswordReset");

			return mailMessage;
		}

		
	}
}