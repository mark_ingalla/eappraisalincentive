using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EAI.Model.DTO;
using Mvc.Mailer;
using System.Net.Mail;

namespace Website.Mailers
{ 
    public class AppraisalNotifier : MailerBase, IAppraisalNotifier     
	{
		public AppraisalNotifier():
			base()
		{
			MasterName="_Layout";
		}


        public virtual MailMessage NotifyEmployee(EmployeeAppraisalMailDTO appraisalMailDTO)
		{
			var mailMessage = new MailMessage{Subject = "Notify Employee"};
            mailMessage.To.Add(appraisalMailDTO.ToEmail);
			//mailMessage.To.Add("some-email@example.com");
			//ViewBag.Data = someObject;
           // ViewData.Model = appraisalMailDTO;
            ViewData = new ViewDataDictionary(appraisalMailDTO);
			PopulateBody(mailMessage, viewName: "NotifyEmployee");

			return mailMessage;
		}


        public virtual MailMessage NotifySupervisor(EmployeeAppraisalMailDTO appraisalMailDTO)
		{
			var mailMessage = new MailMessage{Subject = "Notify Supervisor"};
			
			mailMessage.To.Add(appraisalMailDTO.ToEmail);
			//ViewBag.Data = someObject;
            ViewData.Model = appraisalMailDTO;
			PopulateBody(mailMessage, viewName: "NotifySupervisor");

			return mailMessage;
		}


        public virtual MailMessage NotifyBusinessUnitHead(EmployeeAppraisalMailDTO appraisalMailDTO)
		{
			var mailMessage = new MailMessage{Subject = "Notify BusinessUnitHead"};
            mailMessage.To.Add(appraisalMailDTO.ToEmail);
			//mailMessage.To.Add("some-email@example.com");
			//ViewBag.Data = someObject;
            ViewData.Model = appraisalMailDTO;
			PopulateBody(mailMessage, viewName: "NotifyBusinessUnitHead");

			return mailMessage;
		}

		
	}
}