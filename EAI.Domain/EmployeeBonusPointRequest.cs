﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EAI.Domain
{
    public class EmployeeBonusPointRequest : BaseModel
    {
        public int EmployeeBonusPointRequestId { get; set; }
        public string TransactionNo { get; set; }
        public DateTime TransactionDate { get; set; }
        public int BonusPointCashOut { get; set; }
        public virtual Employee Employee { get; set; }
        
    }
}
