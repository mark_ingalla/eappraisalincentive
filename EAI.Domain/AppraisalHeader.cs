﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EAI.Domain
{
    public class AppraisalHeader : BaseModel
    {
        public int AppraisalHeaderId { get; set; }
        public string Name { get; set; }
        public bool IsActive { get; set; }
        public int? Rank { get; set; }
        public virtual ICollection<AppraisalItem> AppraisalItems { get; set; }
    }
}
