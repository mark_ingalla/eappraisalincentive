﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EAI.Domain
{
    public class EmployeeBonusPoints : BaseModel
    {
        public int EmployeeBonusPointsId { get; set; }

        public string TransactionNo { get; set; }
        public DateTime TransactionDate { get; set; }
        public string TransactionDescription { get; set; }
        public virtual Employee Employee { get; set; }
        public decimal BonusPoints { get; set; }
        public DateTime AddedOn { get; set; }
        public bool IsSystemGenerated { get; set; }
    }
}
