﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace EAI.Domain
{
    public class EmployeeAppraisal : BaseModel
    {
        
        public int EmployeeAppraisalId { get; set; }
        [Required]
        public string AppraisalNo { get; set; }
        public DateTime AppraisalDate { get; set; }

        [Required]
        public int EmployeeId { get; set; }
        
        [ForeignKey("EmployeeId")]
        public virtual Employee Employee { get; set; }

        [Required]
        public int ImmediateSupervisorId { get; set; }
        
        [ForeignKey("ImmediateSupervisorId")]
        public virtual Employee ImmediateSupervisor { get; set; }
        public DateTime? ImmediateSupervisorSignDate { get; set; }
        public string ImmediateSupervisorComment { get; set; }

        [Required]
        public int BusinessUnitHeadId { get; set; }
        
        [ForeignKey("BusinessUnitHeadId")]
        public virtual Employee BusinessUnitHead { get; set; }
        public DateTime? BusinessUnitHeadSignDate { get; set; }
        public string BusinessUnitHeadComment { get; set; }

        public int? RevieweeId { get; set; }
        [ForeignKey("RevieweeId")]
        public virtual Employee Reviewee { get; set; }
        public DateTime? RevieweeSignDate { get; set; }
        public string RevieweeComment { get; set; }

        public int OverallRating { get; set; }

        public int Status { get; set; }

        public virtual ICollection<EmployeeAppraisalHeader> AppraisalHeaders { get; set; }
        //public virtual ICollection<EmployeeAppraisalItem> AppraisalItems { get; set; }

    }
}
