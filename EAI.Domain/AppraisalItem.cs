﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace EAI.Domain
{
    public class AppraisalItem : BaseModel
    {
        public int AppraisalItemId { get; set; }
        public string Description { get; set; }

        public int AppraisalHeaderId { get; set; }

        [ForeignKey("AppraisalHeaderId")]
        public virtual AppraisalHeader Header { get; set; }

        public int? Rank { get; set; }

        public bool IsActive { get; set; }
    }
}
