﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace EAI.Domain
{
    public class EmployeeAppraisalHeader : BaseModel
    {
        public int EmployeeAppraisalHeaderId { get; set; }
        public int AppraisalHeaderId { get; set; }
        [ForeignKey("AppraisalHeaderId")]
        public virtual AppraisalHeader AppraisalHeader { get; set; }
        public int Rating { get; set; }
        public string Comments { get; set; }

        public virtual EmployeeAppraisal EmployeeAppraisal { get; set; }

        public virtual ICollection<EmployeeAppraisalItem> AppraisalItems { get; set; }
    }
}
