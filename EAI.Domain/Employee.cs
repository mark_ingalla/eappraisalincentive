﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace EAI.Domain
{
    public class Employee : BaseModel
    {
        
        public int EmployeeId { get; set; }

        public int UserId { get; set; }
        [ForeignKey("UserId")]
        public virtual User User { get; set; }

        public string EmployeeNo { get; set; }
        public string EmployeeName { get; set; }
        //public string FirstName { get; set; }
        //public string LastName { get; set; }

        public int PositionId { get; set; }
        [ForeignKey("PositionId")]
        public virtual Position Position { get; set; }
        
        public int DepartmentId { get; set; }
        [ForeignKey("DepartmentId")]
        public virtual Department Department { get; set; }

        public DateTime DateJoined { get; set; }
        public DateTime? DateResigned { get; set; }
        public DateTime? DateConfirmed { get; set; }

        public string EmailAddress { get; set; }
        public string Remarks { get; set; }
        
        public virtual int? ImmediateSupervisorId { get; set; }
        [ForeignKey("ImmediateSupervisorId")]
        public virtual Employee ImmediateSupervisor { get; set; }
        public virtual ICollection<Employee> SupervisorChildren { get; set; }

        public virtual int? BusinessUnitHeadId { get; set; }
        [ForeignKey("BusinessUnitHeadId")]
        public virtual Employee BusinessUnitHead { get; set; }
        public virtual ICollection<Employee> BusinessUnitHeadChildren { get; set; }

        public virtual ICollection<EmployeeAppraisal> Appraisals { get; set; }

        public decimal BasicSalary { get; set; }
        public string Grade { get; set; }
        public bool IsActive { get; set; }


        public virtual ICollection<EmployeeBonusPoints> BonusPoints { get; set; }
        public virtual ICollection<EmployeeBonusPointRequest> BonusPointRequests { get; set; }

    }
}
