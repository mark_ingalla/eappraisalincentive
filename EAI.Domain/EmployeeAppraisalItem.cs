﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace EAI.Domain
{
    public class EmployeeAppraisalItem : BaseModel
    {
        public int EmployeeAppraisalItemId { get; set; }
        public int AppraisalItemId { get; set; }
        [ForeignKey("AppraisalItemId")]
        public virtual AppraisalItem AppraisalItem { get; set; }
        public int Rating { get; set; }
        public string Comments { get; set; }
        //public int HeaderId { get; set; }
        //[ForeignKey("HeaderId")]
        public virtual EmployeeAppraisalHeader Header { get; set; }
       // public virtual EmployeeAppraisal EmployeeAppraisal { get; set; }
    }
}
