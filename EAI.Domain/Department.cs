﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EAI.Domain
{
    public class Department : BaseModel
    {
        public int DepartmentId { get; set; }
        public string Name { get; set; }
    }
}
