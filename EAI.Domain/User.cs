﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EAI.Domain
{
    public class User : BaseModel
    {
        public int UserId { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public int Role { get; set; }
        public string Theme { get; set; }
    }
}
