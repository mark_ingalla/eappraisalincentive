﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EAI.Domain
{
    public class Position : BaseModel
    {
        public int PositionId { get; set; }
        public string Name { get; set; }
    }
}
