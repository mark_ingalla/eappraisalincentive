﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EAI.Domain
{
    public interface  IBaseModel
    {
        int? LastUpdatedByUserId { get; set; }
        DateTime LastUpdatedDate { get; set; }
    }

    public class BaseModel : IBaseModel
    {
        public int? LastUpdatedByUserId { get; set; }
        public virtual User LastUpdatedBy { get; set; }
        public DateTime LastUpdatedDate { get; set; }
    }
}
