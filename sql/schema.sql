/****** Object:  ForeignKey [FK_AppraisalHeaders_Users_LastUpdatedByUserId]    Script Date: 04/10/2012 07:08:38 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_AppraisalHeaders_Users_LastUpdatedByUserId]') AND parent_object_id = OBJECT_ID(N'[dbo].[AppraisalHeaders]'))
ALTER TABLE [dbo].[AppraisalHeaders] DROP CONSTRAINT [FK_AppraisalHeaders_Users_LastUpdatedByUserId]
GO
/****** Object:  ForeignKey [FK_AppraisalItems_AppraisalHeaders_AppraisalHeaderId]    Script Date: 04/10/2012 07:08:38 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_AppraisalItems_AppraisalHeaders_AppraisalHeaderId]') AND parent_object_id = OBJECT_ID(N'[dbo].[AppraisalItems]'))
ALTER TABLE [dbo].[AppraisalItems] DROP CONSTRAINT [FK_AppraisalItems_AppraisalHeaders_AppraisalHeaderId]
GO
/****** Object:  ForeignKey [FK_AppraisalItems_Users_LastUpdatedByUserId]    Script Date: 04/10/2012 07:08:38 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_AppraisalItems_Users_LastUpdatedByUserId]') AND parent_object_id = OBJECT_ID(N'[dbo].[AppraisalItems]'))
ALTER TABLE [dbo].[AppraisalItems] DROP CONSTRAINT [FK_AppraisalItems_Users_LastUpdatedByUserId]
GO
/****** Object:  ForeignKey [FK_Departments_Users_LastUpdatedByUserId]    Script Date: 04/10/2012 07:08:38 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Departments_Users_LastUpdatedByUserId]') AND parent_object_id = OBJECT_ID(N'[dbo].[Departments]'))
ALTER TABLE [dbo].[Departments] DROP CONSTRAINT [FK_Departments_Users_LastUpdatedByUserId]
GO
/****** Object:  ForeignKey [FK_EmployeeAppraisalHeaders_AppraisalHeaders_AppraisalHeaderId]    Script Date: 04/10/2012 07:08:38 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_EmployeeAppraisalHeaders_AppraisalHeaders_AppraisalHeaderId]') AND parent_object_id = OBJECT_ID(N'[dbo].[EmployeeAppraisalHeaders]'))
ALTER TABLE [dbo].[EmployeeAppraisalHeaders] DROP CONSTRAINT [FK_EmployeeAppraisalHeaders_AppraisalHeaders_AppraisalHeaderId]
GO
/****** Object:  ForeignKey [FK_EmployeeAppraisalHeaders_EmployeeAppraisals_EmployeeAppraisal_EmployeeAppraisalId]    Script Date: 04/10/2012 07:08:38 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_EmployeeAppraisalHeaders_EmployeeAppraisals_EmployeeAppraisal_EmployeeAppraisalId]') AND parent_object_id = OBJECT_ID(N'[dbo].[EmployeeAppraisalHeaders]'))
ALTER TABLE [dbo].[EmployeeAppraisalHeaders] DROP CONSTRAINT [FK_EmployeeAppraisalHeaders_EmployeeAppraisals_EmployeeAppraisal_EmployeeAppraisalId]
GO
/****** Object:  ForeignKey [FK_EmployeeAppraisalHeaders_Users_LastUpdatedByUserId]    Script Date: 04/10/2012 07:08:38 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_EmployeeAppraisalHeaders_Users_LastUpdatedByUserId]') AND parent_object_id = OBJECT_ID(N'[dbo].[EmployeeAppraisalHeaders]'))
ALTER TABLE [dbo].[EmployeeAppraisalHeaders] DROP CONSTRAINT [FK_EmployeeAppraisalHeaders_Users_LastUpdatedByUserId]
GO
/****** Object:  ForeignKey [FK_EmployeeAppraisalItems_AppraisalItems_AppraisalItemId]    Script Date: 04/10/2012 07:08:38 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_EmployeeAppraisalItems_AppraisalItems_AppraisalItemId]') AND parent_object_id = OBJECT_ID(N'[dbo].[EmployeeAppraisalItems]'))
ALTER TABLE [dbo].[EmployeeAppraisalItems] DROP CONSTRAINT [FK_EmployeeAppraisalItems_AppraisalItems_AppraisalItemId]
GO
/****** Object:  ForeignKey [FK_EmployeeAppraisalItems_EmployeeAppraisalHeaders_Header_EmployeeAppraisalHeaderId]    Script Date: 04/10/2012 07:08:38 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_EmployeeAppraisalItems_EmployeeAppraisalHeaders_Header_EmployeeAppraisalHeaderId]') AND parent_object_id = OBJECT_ID(N'[dbo].[EmployeeAppraisalItems]'))
ALTER TABLE [dbo].[EmployeeAppraisalItems] DROP CONSTRAINT [FK_EmployeeAppraisalItems_EmployeeAppraisalHeaders_Header_EmployeeAppraisalHeaderId]
GO
/****** Object:  ForeignKey [FK_EmployeeAppraisalItems_Users_LastUpdatedByUserId]    Script Date: 04/10/2012 07:08:38 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_EmployeeAppraisalItems_Users_LastUpdatedByUserId]') AND parent_object_id = OBJECT_ID(N'[dbo].[EmployeeAppraisalItems]'))
ALTER TABLE [dbo].[EmployeeAppraisalItems] DROP CONSTRAINT [FK_EmployeeAppraisalItems_Users_LastUpdatedByUserId]
GO
/****** Object:  ForeignKey [FK_EmployeeAppraisals_Employees_BusinessUnitHeadId]    Script Date: 04/10/2012 07:08:38 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_EmployeeAppraisals_Employees_BusinessUnitHeadId]') AND parent_object_id = OBJECT_ID(N'[dbo].[EmployeeAppraisals]'))
ALTER TABLE [dbo].[EmployeeAppraisals] DROP CONSTRAINT [FK_EmployeeAppraisals_Employees_BusinessUnitHeadId]
GO
/****** Object:  ForeignKey [FK_EmployeeAppraisals_Employees_EmployeeId]    Script Date: 04/10/2012 07:08:38 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_EmployeeAppraisals_Employees_EmployeeId]') AND parent_object_id = OBJECT_ID(N'[dbo].[EmployeeAppraisals]'))
ALTER TABLE [dbo].[EmployeeAppraisals] DROP CONSTRAINT [FK_EmployeeAppraisals_Employees_EmployeeId]
GO
/****** Object:  ForeignKey [FK_EmployeeAppraisals_Employees_ImmediateSupervisorId]    Script Date: 04/10/2012 07:08:38 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_EmployeeAppraisals_Employees_ImmediateSupervisorId]') AND parent_object_id = OBJECT_ID(N'[dbo].[EmployeeAppraisals]'))
ALTER TABLE [dbo].[EmployeeAppraisals] DROP CONSTRAINT [FK_EmployeeAppraisals_Employees_ImmediateSupervisorId]
GO
/****** Object:  ForeignKey [FK_EmployeeAppraisals_Employees_RevieweeId]    Script Date: 04/10/2012 07:08:38 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_EmployeeAppraisals_Employees_RevieweeId]') AND parent_object_id = OBJECT_ID(N'[dbo].[EmployeeAppraisals]'))
ALTER TABLE [dbo].[EmployeeAppraisals] DROP CONSTRAINT [FK_EmployeeAppraisals_Employees_RevieweeId]
GO
/****** Object:  ForeignKey [FK_EmployeeAppraisals_Users_LastUpdatedByUserId]    Script Date: 04/10/2012 07:08:38 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_EmployeeAppraisals_Users_LastUpdatedByUserId]') AND parent_object_id = OBJECT_ID(N'[dbo].[EmployeeAppraisals]'))
ALTER TABLE [dbo].[EmployeeAppraisals] DROP CONSTRAINT [FK_EmployeeAppraisals_Users_LastUpdatedByUserId]
GO
/****** Object:  ForeignKey [FK_EmployeeBonusPointRequests_Employees_Employee_EmployeeId]    Script Date: 04/10/2012 07:08:38 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_EmployeeBonusPointRequests_Employees_Employee_EmployeeId]') AND parent_object_id = OBJECT_ID(N'[dbo].[EmployeeBonusPointRequests]'))
ALTER TABLE [dbo].[EmployeeBonusPointRequests] DROP CONSTRAINT [FK_EmployeeBonusPointRequests_Employees_Employee_EmployeeId]
GO
/****** Object:  ForeignKey [FK_EmployeeBonusPointRequests_Users_LastUpdatedByUserId]    Script Date: 04/10/2012 07:08:38 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_EmployeeBonusPointRequests_Users_LastUpdatedByUserId]') AND parent_object_id = OBJECT_ID(N'[dbo].[EmployeeBonusPointRequests]'))
ALTER TABLE [dbo].[EmployeeBonusPointRequests] DROP CONSTRAINT [FK_EmployeeBonusPointRequests_Users_LastUpdatedByUserId]
GO
/****** Object:  ForeignKey [FK_EmployeeBonusPoints_Employees_Employee_EmployeeId]    Script Date: 04/10/2012 07:08:38 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_EmployeeBonusPoints_Employees_Employee_EmployeeId]') AND parent_object_id = OBJECT_ID(N'[dbo].[EmployeeBonusPoints]'))
ALTER TABLE [dbo].[EmployeeBonusPoints] DROP CONSTRAINT [FK_EmployeeBonusPoints_Employees_Employee_EmployeeId]
GO
/****** Object:  ForeignKey [FK_EmployeeBonusPoints_Users_LastUpdatedByUserId]    Script Date: 04/10/2012 07:08:38 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_EmployeeBonusPoints_Users_LastUpdatedByUserId]') AND parent_object_id = OBJECT_ID(N'[dbo].[EmployeeBonusPoints]'))
ALTER TABLE [dbo].[EmployeeBonusPoints] DROP CONSTRAINT [FK_EmployeeBonusPoints_Users_LastUpdatedByUserId]
GO
/****** Object:  ForeignKey [FK_Employees_Departments_DepartmentId]    Script Date: 04/10/2012 07:08:38 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Employees_Departments_DepartmentId]') AND parent_object_id = OBJECT_ID(N'[dbo].[Employees]'))
ALTER TABLE [dbo].[Employees] DROP CONSTRAINT [FK_Employees_Departments_DepartmentId]
GO
/****** Object:  ForeignKey [FK_Employees_Employees_BusinessUnitHeadId]    Script Date: 04/10/2012 07:08:38 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Employees_Employees_BusinessUnitHeadId]') AND parent_object_id = OBJECT_ID(N'[dbo].[Employees]'))
ALTER TABLE [dbo].[Employees] DROP CONSTRAINT [FK_Employees_Employees_BusinessUnitHeadId]
GO
/****** Object:  ForeignKey [FK_Employees_Employees_ImmediateSupervisorId]    Script Date: 04/10/2012 07:08:38 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Employees_Employees_ImmediateSupervisorId]') AND parent_object_id = OBJECT_ID(N'[dbo].[Employees]'))
ALTER TABLE [dbo].[Employees] DROP CONSTRAINT [FK_Employees_Employees_ImmediateSupervisorId]
GO
/****** Object:  ForeignKey [FK_Employees_Positions_PositionId]    Script Date: 04/10/2012 07:08:38 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Employees_Positions_PositionId]') AND parent_object_id = OBJECT_ID(N'[dbo].[Employees]'))
ALTER TABLE [dbo].[Employees] DROP CONSTRAINT [FK_Employees_Positions_PositionId]
GO
/****** Object:  ForeignKey [FK_Employees_Users_LastUpdatedByUserId]    Script Date: 04/10/2012 07:08:38 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Employees_Users_LastUpdatedByUserId]') AND parent_object_id = OBJECT_ID(N'[dbo].[Employees]'))
ALTER TABLE [dbo].[Employees] DROP CONSTRAINT [FK_Employees_Users_LastUpdatedByUserId]
GO
/****** Object:  ForeignKey [FK_Employees_Users_UserId]    Script Date: 04/10/2012 07:08:38 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Employees_Users_UserId]') AND parent_object_id = OBJECT_ID(N'[dbo].[Employees]'))
ALTER TABLE [dbo].[Employees] DROP CONSTRAINT [FK_Employees_Users_UserId]
GO
/****** Object:  ForeignKey [FK_Positions_Users_LastUpdatedByUserId]    Script Date: 04/10/2012 07:08:38 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Positions_Users_LastUpdatedByUserId]') AND parent_object_id = OBJECT_ID(N'[dbo].[Positions]'))
ALTER TABLE [dbo].[Positions] DROP CONSTRAINT [FK_Positions_Users_LastUpdatedByUserId]
GO
/****** Object:  ForeignKey [FK_Users_Users_LastUpdatedByUserId]    Script Date: 04/10/2012 07:08:38 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Users_Users_LastUpdatedByUserId]') AND parent_object_id = OBJECT_ID(N'[dbo].[Users]'))
ALTER TABLE [dbo].[Users] DROP CONSTRAINT [FK_Users_Users_LastUpdatedByUserId]
GO
/****** Object:  Table [dbo].[EmployeeAppraisalItems]    Script Date: 04/10/2012 07:08:38 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_EmployeeAppraisalItems_AppraisalItems_AppraisalItemId]') AND parent_object_id = OBJECT_ID(N'[dbo].[EmployeeAppraisalItems]'))
ALTER TABLE [dbo].[EmployeeAppraisalItems] DROP CONSTRAINT [FK_EmployeeAppraisalItems_AppraisalItems_AppraisalItemId]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_EmployeeAppraisalItems_EmployeeAppraisalHeaders_Header_EmployeeAppraisalHeaderId]') AND parent_object_id = OBJECT_ID(N'[dbo].[EmployeeAppraisalItems]'))
ALTER TABLE [dbo].[EmployeeAppraisalItems] DROP CONSTRAINT [FK_EmployeeAppraisalItems_EmployeeAppraisalHeaders_Header_EmployeeAppraisalHeaderId]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_EmployeeAppraisalItems_Users_LastUpdatedByUserId]') AND parent_object_id = OBJECT_ID(N'[dbo].[EmployeeAppraisalItems]'))
ALTER TABLE [dbo].[EmployeeAppraisalItems] DROP CONSTRAINT [FK_EmployeeAppraisalItems_Users_LastUpdatedByUserId]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EmployeeAppraisalItems]') AND type in (N'U'))
DROP TABLE [dbo].[EmployeeAppraisalItems]
GO
/****** Object:  Table [dbo].[EmployeeAppraisalHeaders]    Script Date: 04/10/2012 07:08:38 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_EmployeeAppraisalHeaders_AppraisalHeaders_AppraisalHeaderId]') AND parent_object_id = OBJECT_ID(N'[dbo].[EmployeeAppraisalHeaders]'))
ALTER TABLE [dbo].[EmployeeAppraisalHeaders] DROP CONSTRAINT [FK_EmployeeAppraisalHeaders_AppraisalHeaders_AppraisalHeaderId]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_EmployeeAppraisalHeaders_EmployeeAppraisals_EmployeeAppraisal_EmployeeAppraisalId]') AND parent_object_id = OBJECT_ID(N'[dbo].[EmployeeAppraisalHeaders]'))
ALTER TABLE [dbo].[EmployeeAppraisalHeaders] DROP CONSTRAINT [FK_EmployeeAppraisalHeaders_EmployeeAppraisals_EmployeeAppraisal_EmployeeAppraisalId]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_EmployeeAppraisalHeaders_Users_LastUpdatedByUserId]') AND parent_object_id = OBJECT_ID(N'[dbo].[EmployeeAppraisalHeaders]'))
ALTER TABLE [dbo].[EmployeeAppraisalHeaders] DROP CONSTRAINT [FK_EmployeeAppraisalHeaders_Users_LastUpdatedByUserId]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EmployeeAppraisalHeaders]') AND type in (N'U'))
DROP TABLE [dbo].[EmployeeAppraisalHeaders]
GO
/****** Object:  Table [dbo].[EmployeeAppraisals]    Script Date: 04/10/2012 07:08:38 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_EmployeeAppraisals_Employees_BusinessUnitHeadId]') AND parent_object_id = OBJECT_ID(N'[dbo].[EmployeeAppraisals]'))
ALTER TABLE [dbo].[EmployeeAppraisals] DROP CONSTRAINT [FK_EmployeeAppraisals_Employees_BusinessUnitHeadId]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_EmployeeAppraisals_Employees_EmployeeId]') AND parent_object_id = OBJECT_ID(N'[dbo].[EmployeeAppraisals]'))
ALTER TABLE [dbo].[EmployeeAppraisals] DROP CONSTRAINT [FK_EmployeeAppraisals_Employees_EmployeeId]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_EmployeeAppraisals_Employees_ImmediateSupervisorId]') AND parent_object_id = OBJECT_ID(N'[dbo].[EmployeeAppraisals]'))
ALTER TABLE [dbo].[EmployeeAppraisals] DROP CONSTRAINT [FK_EmployeeAppraisals_Employees_ImmediateSupervisorId]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_EmployeeAppraisals_Employees_RevieweeId]') AND parent_object_id = OBJECT_ID(N'[dbo].[EmployeeAppraisals]'))
ALTER TABLE [dbo].[EmployeeAppraisals] DROP CONSTRAINT [FK_EmployeeAppraisals_Employees_RevieweeId]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_EmployeeAppraisals_Users_LastUpdatedByUserId]') AND parent_object_id = OBJECT_ID(N'[dbo].[EmployeeAppraisals]'))
ALTER TABLE [dbo].[EmployeeAppraisals] DROP CONSTRAINT [FK_EmployeeAppraisals_Users_LastUpdatedByUserId]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EmployeeAppraisals]') AND type in (N'U'))
DROP TABLE [dbo].[EmployeeAppraisals]
GO
/****** Object:  Table [dbo].[EmployeeBonusPointRequests]    Script Date: 04/10/2012 07:08:38 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_EmployeeBonusPointRequests_Employees_Employee_EmployeeId]') AND parent_object_id = OBJECT_ID(N'[dbo].[EmployeeBonusPointRequests]'))
ALTER TABLE [dbo].[EmployeeBonusPointRequests] DROP CONSTRAINT [FK_EmployeeBonusPointRequests_Employees_Employee_EmployeeId]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_EmployeeBonusPointRequests_Users_LastUpdatedByUserId]') AND parent_object_id = OBJECT_ID(N'[dbo].[EmployeeBonusPointRequests]'))
ALTER TABLE [dbo].[EmployeeBonusPointRequests] DROP CONSTRAINT [FK_EmployeeBonusPointRequests_Users_LastUpdatedByUserId]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EmployeeBonusPointRequests]') AND type in (N'U'))
DROP TABLE [dbo].[EmployeeBonusPointRequests]
GO
/****** Object:  Table [dbo].[EmployeeBonusPoints]    Script Date: 04/10/2012 07:08:38 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_EmployeeBonusPoints_Employees_Employee_EmployeeId]') AND parent_object_id = OBJECT_ID(N'[dbo].[EmployeeBonusPoints]'))
ALTER TABLE [dbo].[EmployeeBonusPoints] DROP CONSTRAINT [FK_EmployeeBonusPoints_Employees_Employee_EmployeeId]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_EmployeeBonusPoints_Users_LastUpdatedByUserId]') AND parent_object_id = OBJECT_ID(N'[dbo].[EmployeeBonusPoints]'))
ALTER TABLE [dbo].[EmployeeBonusPoints] DROP CONSTRAINT [FK_EmployeeBonusPoints_Users_LastUpdatedByUserId]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EmployeeBonusPoints]') AND type in (N'U'))
DROP TABLE [dbo].[EmployeeBonusPoints]
GO
/****** Object:  Table [dbo].[Employees]    Script Date: 04/10/2012 07:08:38 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Employees_Departments_DepartmentId]') AND parent_object_id = OBJECT_ID(N'[dbo].[Employees]'))
ALTER TABLE [dbo].[Employees] DROP CONSTRAINT [FK_Employees_Departments_DepartmentId]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Employees_Employees_BusinessUnitHeadId]') AND parent_object_id = OBJECT_ID(N'[dbo].[Employees]'))
ALTER TABLE [dbo].[Employees] DROP CONSTRAINT [FK_Employees_Employees_BusinessUnitHeadId]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Employees_Employees_ImmediateSupervisorId]') AND parent_object_id = OBJECT_ID(N'[dbo].[Employees]'))
ALTER TABLE [dbo].[Employees] DROP CONSTRAINT [FK_Employees_Employees_ImmediateSupervisorId]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Employees_Positions_PositionId]') AND parent_object_id = OBJECT_ID(N'[dbo].[Employees]'))
ALTER TABLE [dbo].[Employees] DROP CONSTRAINT [FK_Employees_Positions_PositionId]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Employees_Users_LastUpdatedByUserId]') AND parent_object_id = OBJECT_ID(N'[dbo].[Employees]'))
ALTER TABLE [dbo].[Employees] DROP CONSTRAINT [FK_Employees_Users_LastUpdatedByUserId]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Employees_Users_UserId]') AND parent_object_id = OBJECT_ID(N'[dbo].[Employees]'))
ALTER TABLE [dbo].[Employees] DROP CONSTRAINT [FK_Employees_Users_UserId]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Employees]') AND type in (N'U'))
DROP TABLE [dbo].[Employees]
GO
/****** Object:  Table [dbo].[AppraisalItems]    Script Date: 04/10/2012 07:08:38 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_AppraisalItems_AppraisalHeaders_AppraisalHeaderId]') AND parent_object_id = OBJECT_ID(N'[dbo].[AppraisalItems]'))
ALTER TABLE [dbo].[AppraisalItems] DROP CONSTRAINT [FK_AppraisalItems_AppraisalHeaders_AppraisalHeaderId]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_AppraisalItems_Users_LastUpdatedByUserId]') AND parent_object_id = OBJECT_ID(N'[dbo].[AppraisalItems]'))
ALTER TABLE [dbo].[AppraisalItems] DROP CONSTRAINT [FK_AppraisalItems_Users_LastUpdatedByUserId]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AppraisalItems]') AND type in (N'U'))
DROP TABLE [dbo].[AppraisalItems]
GO
/****** Object:  Table [dbo].[Departments]    Script Date: 04/10/2012 07:08:38 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Departments_Users_LastUpdatedByUserId]') AND parent_object_id = OBJECT_ID(N'[dbo].[Departments]'))
ALTER TABLE [dbo].[Departments] DROP CONSTRAINT [FK_Departments_Users_LastUpdatedByUserId]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Departments]') AND type in (N'U'))
DROP TABLE [dbo].[Departments]
GO
/****** Object:  Table [dbo].[AppraisalHeaders]    Script Date: 04/10/2012 07:08:38 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_AppraisalHeaders_Users_LastUpdatedByUserId]') AND parent_object_id = OBJECT_ID(N'[dbo].[AppraisalHeaders]'))
ALTER TABLE [dbo].[AppraisalHeaders] DROP CONSTRAINT [FK_AppraisalHeaders_Users_LastUpdatedByUserId]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AppraisalHeaders]') AND type in (N'U'))
DROP TABLE [dbo].[AppraisalHeaders]
GO
/****** Object:  Table [dbo].[Positions]    Script Date: 04/10/2012 07:08:38 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Positions_Users_LastUpdatedByUserId]') AND parent_object_id = OBJECT_ID(N'[dbo].[Positions]'))
ALTER TABLE [dbo].[Positions] DROP CONSTRAINT [FK_Positions_Users_LastUpdatedByUserId]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Positions]') AND type in (N'U'))
DROP TABLE [dbo].[Positions]
GO
/****** Object:  Table [dbo].[Users]    Script Date: 04/10/2012 07:08:38 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Users_Users_LastUpdatedByUserId]') AND parent_object_id = OBJECT_ID(N'[dbo].[Users]'))
ALTER TABLE [dbo].[Users] DROP CONSTRAINT [FK_Users_Users_LastUpdatedByUserId]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Users]') AND type in (N'U'))
DROP TABLE [dbo].[Users]
GO
/****** Object:  Table [dbo].[Users]    Script Date: 04/10/2012 07:08:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Users]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Users](
	[UserId] [int] IDENTITY(1,1) NOT NULL,
	[Username] [nvarchar](max) NULL,
	[Password] [nvarchar](max) NULL,
	[Role] [int] NOT NULL,
	[Theme] [nvarchar](max) NULL,
	[LastUpdatedByUserId] [int] NULL,
	[LastUpdatedDate] [datetime] NOT NULL,
 CONSTRAINT [PK_Users] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[Positions]    Script Date: 04/10/2012 07:08:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Positions]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Positions](
	[PositionId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NULL,
	[LastUpdatedByUserId] [int] NULL,
	[LastUpdatedDate] [datetime] NOT NULL,
 CONSTRAINT [PK_Positions] PRIMARY KEY CLUSTERED 
(
	[PositionId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[AppraisalHeaders]    Script Date: 04/10/2012 07:08:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AppraisalHeaders]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[AppraisalHeaders](
	[AppraisalHeaderId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NULL,
	[IsActive] [bit] NOT NULL,
	[Rank] [int] NULL,
	[LastUpdatedByUserId] [int] NULL,
	[LastUpdatedDate] [datetime] NOT NULL,
 CONSTRAINT [PK_AppraisalHeaders] PRIMARY KEY CLUSTERED 
(
	[AppraisalHeaderId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[Departments]    Script Date: 04/10/2012 07:08:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Departments]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Departments](
	[DepartmentId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NULL,
	[LastUpdatedByUserId] [int] NULL,
	[LastUpdatedDate] [datetime] NOT NULL,
 CONSTRAINT [PK_Departments] PRIMARY KEY CLUSTERED 
(
	[DepartmentId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[AppraisalItems]    Script Date: 04/10/2012 07:08:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AppraisalItems]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[AppraisalItems](
	[AppraisalItemId] [int] IDENTITY(1,1) NOT NULL,
	[Description] [nvarchar](max) NULL,
	[AppraisalHeaderId] [int] NOT NULL,
	[Rank] [int] NULL,
	[IsActive] [bit] NOT NULL,
	[LastUpdatedByUserId] [int] NULL,
	[LastUpdatedDate] [datetime] NOT NULL,
 CONSTRAINT [PK_AppraisalItems] PRIMARY KEY CLUSTERED 
(
	[AppraisalItemId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[Employees]    Script Date: 04/10/2012 07:08:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Employees]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Employees](
	[EmployeeId] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	[EmployeeNo] [nvarchar](max) NULL,
	[EmployeeName] [nvarchar](max) NULL,
	[PositionId] [int] NOT NULL,
	[DepartmentId] [int] NOT NULL,
	[DateJoined] [datetime] NOT NULL,
	[DateResigned] [datetime] NULL,
	[DateConfirmed] [datetime] NULL,
	[EmailAddress] [nvarchar](max) NULL,
	[Remarks] [nvarchar](max) NULL,
	[ImmediateSupervisorId] [int] NULL,
	[BusinessUnitHeadId] [int] NULL,
	[BasicSalary] [decimal](18, 2) NOT NULL,
	[Grade] [nvarchar](max) NULL,
	[IsActive] [bit] NOT NULL,
	[LastUpdatedByUserId] [int] NULL,
	[LastUpdatedDate] [datetime] NOT NULL,
 CONSTRAINT [PK_Employees] PRIMARY KEY CLUSTERED 
(
	[EmployeeId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[EmployeeBonusPoints]    Script Date: 04/10/2012 07:08:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EmployeeBonusPoints]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EmployeeBonusPoints](
	[EmployeeBonusPointsId] [int] IDENTITY(1,1) NOT NULL,
	[TransactionNo] [nvarchar](max) NULL,
	[TransactionDate] [datetime] NOT NULL,
	[TransactionDescription] [nvarchar](max) NULL,
	[BonusPoints] [int] NOT NULL,
	[AddedOn] [datetime] NOT NULL,
	[IsSystemGenerated] [bit] NOT NULL,
	[LastUpdatedByUserId] [int] NULL,
	[LastUpdatedDate] [datetime] NOT NULL,
	[Employee_EmployeeId] [int] NULL,
 CONSTRAINT [PK_EmployeeBonusPoints] PRIMARY KEY CLUSTERED 
(
	[EmployeeBonusPointsId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[EmployeeBonusPointRequests]    Script Date: 04/10/2012 07:08:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EmployeeBonusPointRequests]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EmployeeBonusPointRequests](
	[EmployeeBonusPointRequestId] [int] IDENTITY(1,1) NOT NULL,
	[TransactionNo] [nvarchar](max) NULL,
	[TransactionDate] [datetime] NOT NULL,
	[BonusPointCashOut] [int] NOT NULL,
	[LastUpdatedByUserId] [int] NULL,
	[LastUpdatedDate] [datetime] NOT NULL,
	[Employee_EmployeeId] [int] NULL,
 CONSTRAINT [PK_EmployeeBonusPointRequests] PRIMARY KEY CLUSTERED 
(
	[EmployeeBonusPointRequestId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[EmployeeAppraisals]    Script Date: 04/10/2012 07:08:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EmployeeAppraisals]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EmployeeAppraisals](
	[EmployeeAppraisalId] [int] IDENTITY(1,1) NOT NULL,
	[AppraisalNo] [nvarchar](max) NOT NULL,
	[AppraisalDate] [datetime] NOT NULL,
	[EmployeeId] [int] NOT NULL,
	[ImmediateSupervisorId] [int] NOT NULL,
	[ImmediateSupervisorSignDate] [datetime] NULL,
	[ImmediateSupervisorComment] [nvarchar](max) NULL,
	[BusinessUnitHeadId] [int] NOT NULL,
	[BusinessUnitHeadSignDate] [datetime] NULL,
	[BusinessUnitHeadComment] [nvarchar](max) NULL,
	[RevieweeId] [int] NULL,
	[RevieweeSignDate] [datetime] NULL,
	[RevieweeComment] [nvarchar](max) NULL,
	[OverallRating] [int] NOT NULL,
	[Status] [int] NOT NULL,
	[LastUpdatedByUserId] [int] NULL,
	[LastUpdatedDate] [datetime] NOT NULL,
 CONSTRAINT [PK_EmployeeAppraisals] PRIMARY KEY CLUSTERED 
(
	[EmployeeAppraisalId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[EmployeeAppraisalHeaders]    Script Date: 04/10/2012 07:08:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EmployeeAppraisalHeaders]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EmployeeAppraisalHeaders](
	[EmployeeAppraisalHeaderId] [int] IDENTITY(1,1) NOT NULL,
	[AppraisalHeaderId] [int] NOT NULL,
	[Rating] [int] NOT NULL,
	[Comments] [nvarchar](max) NULL,
	[LastUpdatedByUserId] [int] NULL,
	[LastUpdatedDate] [datetime] NOT NULL,
	[EmployeeAppraisal_EmployeeAppraisalId] [int] NULL,
 CONSTRAINT [PK_EmployeeAppraisalHeaders] PRIMARY KEY CLUSTERED 
(
	[EmployeeAppraisalHeaderId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[EmployeeAppraisalItems]    Script Date: 04/10/2012 07:08:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EmployeeAppraisalItems]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EmployeeAppraisalItems](
	[EmployeeAppraisalItemId] [int] IDENTITY(1,1) NOT NULL,
	[AppraisalItemId] [int] NOT NULL,
	[Rating] [int] NOT NULL,
	[Comments] [nvarchar](max) NULL,
	[LastUpdatedByUserId] [int] NULL,
	[LastUpdatedDate] [datetime] NOT NULL,
	[Header_EmployeeAppraisalHeaderId] [int] NULL,
 CONSTRAINT [PK_EmployeeAppraisalItems] PRIMARY KEY CLUSTERED 
(
	[EmployeeAppraisalItemId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  ForeignKey [FK_AppraisalHeaders_Users_LastUpdatedByUserId]    Script Date: 04/10/2012 07:08:38 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_AppraisalHeaders_Users_LastUpdatedByUserId]') AND parent_object_id = OBJECT_ID(N'[dbo].[AppraisalHeaders]'))
ALTER TABLE [dbo].[AppraisalHeaders]  WITH CHECK ADD  CONSTRAINT [FK_AppraisalHeaders_Users_LastUpdatedByUserId] FOREIGN KEY([LastUpdatedByUserId])
REFERENCES [dbo].[Users] ([UserId])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_AppraisalHeaders_Users_LastUpdatedByUserId]') AND parent_object_id = OBJECT_ID(N'[dbo].[AppraisalHeaders]'))
ALTER TABLE [dbo].[AppraisalHeaders] CHECK CONSTRAINT [FK_AppraisalHeaders_Users_LastUpdatedByUserId]
GO
/****** Object:  ForeignKey [FK_AppraisalItems_AppraisalHeaders_AppraisalHeaderId]    Script Date: 04/10/2012 07:08:38 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_AppraisalItems_AppraisalHeaders_AppraisalHeaderId]') AND parent_object_id = OBJECT_ID(N'[dbo].[AppraisalItems]'))
ALTER TABLE [dbo].[AppraisalItems]  WITH CHECK ADD  CONSTRAINT [FK_AppraisalItems_AppraisalHeaders_AppraisalHeaderId] FOREIGN KEY([AppraisalHeaderId])
REFERENCES [dbo].[AppraisalHeaders] ([AppraisalHeaderId])
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_AppraisalItems_AppraisalHeaders_AppraisalHeaderId]') AND parent_object_id = OBJECT_ID(N'[dbo].[AppraisalItems]'))
ALTER TABLE [dbo].[AppraisalItems] CHECK CONSTRAINT [FK_AppraisalItems_AppraisalHeaders_AppraisalHeaderId]
GO
/****** Object:  ForeignKey [FK_AppraisalItems_Users_LastUpdatedByUserId]    Script Date: 04/10/2012 07:08:38 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_AppraisalItems_Users_LastUpdatedByUserId]') AND parent_object_id = OBJECT_ID(N'[dbo].[AppraisalItems]'))
ALTER TABLE [dbo].[AppraisalItems]  WITH CHECK ADD  CONSTRAINT [FK_AppraisalItems_Users_LastUpdatedByUserId] FOREIGN KEY([LastUpdatedByUserId])
REFERENCES [dbo].[Users] ([UserId])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_AppraisalItems_Users_LastUpdatedByUserId]') AND parent_object_id = OBJECT_ID(N'[dbo].[AppraisalItems]'))
ALTER TABLE [dbo].[AppraisalItems] CHECK CONSTRAINT [FK_AppraisalItems_Users_LastUpdatedByUserId]
GO
/****** Object:  ForeignKey [FK_Departments_Users_LastUpdatedByUserId]    Script Date: 04/10/2012 07:08:38 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Departments_Users_LastUpdatedByUserId]') AND parent_object_id = OBJECT_ID(N'[dbo].[Departments]'))
ALTER TABLE [dbo].[Departments]  WITH CHECK ADD  CONSTRAINT [FK_Departments_Users_LastUpdatedByUserId] FOREIGN KEY([LastUpdatedByUserId])
REFERENCES [dbo].[Users] ([UserId])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Departments_Users_LastUpdatedByUserId]') AND parent_object_id = OBJECT_ID(N'[dbo].[Departments]'))
ALTER TABLE [dbo].[Departments] CHECK CONSTRAINT [FK_Departments_Users_LastUpdatedByUserId]
GO
/****** Object:  ForeignKey [FK_EmployeeAppraisalHeaders_AppraisalHeaders_AppraisalHeaderId]    Script Date: 04/10/2012 07:08:38 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_EmployeeAppraisalHeaders_AppraisalHeaders_AppraisalHeaderId]') AND parent_object_id = OBJECT_ID(N'[dbo].[EmployeeAppraisalHeaders]'))
ALTER TABLE [dbo].[EmployeeAppraisalHeaders]  WITH CHECK ADD  CONSTRAINT [FK_EmployeeAppraisalHeaders_AppraisalHeaders_AppraisalHeaderId] FOREIGN KEY([AppraisalHeaderId])
REFERENCES [dbo].[AppraisalHeaders] ([AppraisalHeaderId])
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_EmployeeAppraisalHeaders_AppraisalHeaders_AppraisalHeaderId]') AND parent_object_id = OBJECT_ID(N'[dbo].[EmployeeAppraisalHeaders]'))
ALTER TABLE [dbo].[EmployeeAppraisalHeaders] CHECK CONSTRAINT [FK_EmployeeAppraisalHeaders_AppraisalHeaders_AppraisalHeaderId]
GO
/****** Object:  ForeignKey [FK_EmployeeAppraisalHeaders_EmployeeAppraisals_EmployeeAppraisal_EmployeeAppraisalId]    Script Date: 04/10/2012 07:08:38 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_EmployeeAppraisalHeaders_EmployeeAppraisals_EmployeeAppraisal_EmployeeAppraisalId]') AND parent_object_id = OBJECT_ID(N'[dbo].[EmployeeAppraisalHeaders]'))
ALTER TABLE [dbo].[EmployeeAppraisalHeaders]  WITH CHECK ADD  CONSTRAINT [FK_EmployeeAppraisalHeaders_EmployeeAppraisals_EmployeeAppraisal_EmployeeAppraisalId] FOREIGN KEY([EmployeeAppraisal_EmployeeAppraisalId])
REFERENCES [dbo].[EmployeeAppraisals] ([EmployeeAppraisalId])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_EmployeeAppraisalHeaders_EmployeeAppraisals_EmployeeAppraisal_EmployeeAppraisalId]') AND parent_object_id = OBJECT_ID(N'[dbo].[EmployeeAppraisalHeaders]'))
ALTER TABLE [dbo].[EmployeeAppraisalHeaders] CHECK CONSTRAINT [FK_EmployeeAppraisalHeaders_EmployeeAppraisals_EmployeeAppraisal_EmployeeAppraisalId]
GO
/****** Object:  ForeignKey [FK_EmployeeAppraisalHeaders_Users_LastUpdatedByUserId]    Script Date: 04/10/2012 07:08:38 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_EmployeeAppraisalHeaders_Users_LastUpdatedByUserId]') AND parent_object_id = OBJECT_ID(N'[dbo].[EmployeeAppraisalHeaders]'))
ALTER TABLE [dbo].[EmployeeAppraisalHeaders]  WITH CHECK ADD  CONSTRAINT [FK_EmployeeAppraisalHeaders_Users_LastUpdatedByUserId] FOREIGN KEY([LastUpdatedByUserId])
REFERENCES [dbo].[Users] ([UserId])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_EmployeeAppraisalHeaders_Users_LastUpdatedByUserId]') AND parent_object_id = OBJECT_ID(N'[dbo].[EmployeeAppraisalHeaders]'))
ALTER TABLE [dbo].[EmployeeAppraisalHeaders] CHECK CONSTRAINT [FK_EmployeeAppraisalHeaders_Users_LastUpdatedByUserId]
GO
/****** Object:  ForeignKey [FK_EmployeeAppraisalItems_AppraisalItems_AppraisalItemId]    Script Date: 04/10/2012 07:08:38 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_EmployeeAppraisalItems_AppraisalItems_AppraisalItemId]') AND parent_object_id = OBJECT_ID(N'[dbo].[EmployeeAppraisalItems]'))
ALTER TABLE [dbo].[EmployeeAppraisalItems]  WITH CHECK ADD  CONSTRAINT [FK_EmployeeAppraisalItems_AppraisalItems_AppraisalItemId] FOREIGN KEY([AppraisalItemId])
REFERENCES [dbo].[AppraisalItems] ([AppraisalItemId])
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_EmployeeAppraisalItems_AppraisalItems_AppraisalItemId]') AND parent_object_id = OBJECT_ID(N'[dbo].[EmployeeAppraisalItems]'))
ALTER TABLE [dbo].[EmployeeAppraisalItems] CHECK CONSTRAINT [FK_EmployeeAppraisalItems_AppraisalItems_AppraisalItemId]
GO
/****** Object:  ForeignKey [FK_EmployeeAppraisalItems_EmployeeAppraisalHeaders_Header_EmployeeAppraisalHeaderId]    Script Date: 04/10/2012 07:08:38 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_EmployeeAppraisalItems_EmployeeAppraisalHeaders_Header_EmployeeAppraisalHeaderId]') AND parent_object_id = OBJECT_ID(N'[dbo].[EmployeeAppraisalItems]'))
ALTER TABLE [dbo].[EmployeeAppraisalItems]  WITH CHECK ADD  CONSTRAINT [FK_EmployeeAppraisalItems_EmployeeAppraisalHeaders_Header_EmployeeAppraisalHeaderId] FOREIGN KEY([Header_EmployeeAppraisalHeaderId])
REFERENCES [dbo].[EmployeeAppraisalHeaders] ([EmployeeAppraisalHeaderId])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_EmployeeAppraisalItems_EmployeeAppraisalHeaders_Header_EmployeeAppraisalHeaderId]') AND parent_object_id = OBJECT_ID(N'[dbo].[EmployeeAppraisalItems]'))
ALTER TABLE [dbo].[EmployeeAppraisalItems] CHECK CONSTRAINT [FK_EmployeeAppraisalItems_EmployeeAppraisalHeaders_Header_EmployeeAppraisalHeaderId]
GO
/****** Object:  ForeignKey [FK_EmployeeAppraisalItems_Users_LastUpdatedByUserId]    Script Date: 04/10/2012 07:08:38 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_EmployeeAppraisalItems_Users_LastUpdatedByUserId]') AND parent_object_id = OBJECT_ID(N'[dbo].[EmployeeAppraisalItems]'))
ALTER TABLE [dbo].[EmployeeAppraisalItems]  WITH CHECK ADD  CONSTRAINT [FK_EmployeeAppraisalItems_Users_LastUpdatedByUserId] FOREIGN KEY([LastUpdatedByUserId])
REFERENCES [dbo].[Users] ([UserId])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_EmployeeAppraisalItems_Users_LastUpdatedByUserId]') AND parent_object_id = OBJECT_ID(N'[dbo].[EmployeeAppraisalItems]'))
ALTER TABLE [dbo].[EmployeeAppraisalItems] CHECK CONSTRAINT [FK_EmployeeAppraisalItems_Users_LastUpdatedByUserId]
GO
/****** Object:  ForeignKey [FK_EmployeeAppraisals_Employees_BusinessUnitHeadId]    Script Date: 04/10/2012 07:08:38 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_EmployeeAppraisals_Employees_BusinessUnitHeadId]') AND parent_object_id = OBJECT_ID(N'[dbo].[EmployeeAppraisals]'))
ALTER TABLE [dbo].[EmployeeAppraisals]  WITH CHECK ADD  CONSTRAINT [FK_EmployeeAppraisals_Employees_BusinessUnitHeadId] FOREIGN KEY([BusinessUnitHeadId])
REFERENCES [dbo].[Employees] ([EmployeeId])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_EmployeeAppraisals_Employees_BusinessUnitHeadId]') AND parent_object_id = OBJECT_ID(N'[dbo].[EmployeeAppraisals]'))
ALTER TABLE [dbo].[EmployeeAppraisals] CHECK CONSTRAINT [FK_EmployeeAppraisals_Employees_BusinessUnitHeadId]
GO
/****** Object:  ForeignKey [FK_EmployeeAppraisals_Employees_EmployeeId]    Script Date: 04/10/2012 07:08:38 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_EmployeeAppraisals_Employees_EmployeeId]') AND parent_object_id = OBJECT_ID(N'[dbo].[EmployeeAppraisals]'))
ALTER TABLE [dbo].[EmployeeAppraisals]  WITH CHECK ADD  CONSTRAINT [FK_EmployeeAppraisals_Employees_EmployeeId] FOREIGN KEY([EmployeeId])
REFERENCES [dbo].[Employees] ([EmployeeId])
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_EmployeeAppraisals_Employees_EmployeeId]') AND parent_object_id = OBJECT_ID(N'[dbo].[EmployeeAppraisals]'))
ALTER TABLE [dbo].[EmployeeAppraisals] CHECK CONSTRAINT [FK_EmployeeAppraisals_Employees_EmployeeId]
GO
/****** Object:  ForeignKey [FK_EmployeeAppraisals_Employees_ImmediateSupervisorId]    Script Date: 04/10/2012 07:08:38 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_EmployeeAppraisals_Employees_ImmediateSupervisorId]') AND parent_object_id = OBJECT_ID(N'[dbo].[EmployeeAppraisals]'))
ALTER TABLE [dbo].[EmployeeAppraisals]  WITH CHECK ADD  CONSTRAINT [FK_EmployeeAppraisals_Employees_ImmediateSupervisorId] FOREIGN KEY([ImmediateSupervisorId])
REFERENCES [dbo].[Employees] ([EmployeeId])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_EmployeeAppraisals_Employees_ImmediateSupervisorId]') AND parent_object_id = OBJECT_ID(N'[dbo].[EmployeeAppraisals]'))
ALTER TABLE [dbo].[EmployeeAppraisals] CHECK CONSTRAINT [FK_EmployeeAppraisals_Employees_ImmediateSupervisorId]
GO
/****** Object:  ForeignKey [FK_EmployeeAppraisals_Employees_RevieweeId]    Script Date: 04/10/2012 07:08:38 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_EmployeeAppraisals_Employees_RevieweeId]') AND parent_object_id = OBJECT_ID(N'[dbo].[EmployeeAppraisals]'))
ALTER TABLE [dbo].[EmployeeAppraisals]  WITH CHECK ADD  CONSTRAINT [FK_EmployeeAppraisals_Employees_RevieweeId] FOREIGN KEY([RevieweeId])
REFERENCES [dbo].[Employees] ([EmployeeId])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_EmployeeAppraisals_Employees_RevieweeId]') AND parent_object_id = OBJECT_ID(N'[dbo].[EmployeeAppraisals]'))
ALTER TABLE [dbo].[EmployeeAppraisals] CHECK CONSTRAINT [FK_EmployeeAppraisals_Employees_RevieweeId]
GO
/****** Object:  ForeignKey [FK_EmployeeAppraisals_Users_LastUpdatedByUserId]    Script Date: 04/10/2012 07:08:38 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_EmployeeAppraisals_Users_LastUpdatedByUserId]') AND parent_object_id = OBJECT_ID(N'[dbo].[EmployeeAppraisals]'))
ALTER TABLE [dbo].[EmployeeAppraisals]  WITH CHECK ADD  CONSTRAINT [FK_EmployeeAppraisals_Users_LastUpdatedByUserId] FOREIGN KEY([LastUpdatedByUserId])
REFERENCES [dbo].[Users] ([UserId])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_EmployeeAppraisals_Users_LastUpdatedByUserId]') AND parent_object_id = OBJECT_ID(N'[dbo].[EmployeeAppraisals]'))
ALTER TABLE [dbo].[EmployeeAppraisals] CHECK CONSTRAINT [FK_EmployeeAppraisals_Users_LastUpdatedByUserId]
GO
/****** Object:  ForeignKey [FK_EmployeeBonusPointRequests_Employees_Employee_EmployeeId]    Script Date: 04/10/2012 07:08:38 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_EmployeeBonusPointRequests_Employees_Employee_EmployeeId]') AND parent_object_id = OBJECT_ID(N'[dbo].[EmployeeBonusPointRequests]'))
ALTER TABLE [dbo].[EmployeeBonusPointRequests]  WITH CHECK ADD  CONSTRAINT [FK_EmployeeBonusPointRequests_Employees_Employee_EmployeeId] FOREIGN KEY([Employee_EmployeeId])
REFERENCES [dbo].[Employees] ([EmployeeId])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_EmployeeBonusPointRequests_Employees_Employee_EmployeeId]') AND parent_object_id = OBJECT_ID(N'[dbo].[EmployeeBonusPointRequests]'))
ALTER TABLE [dbo].[EmployeeBonusPointRequests] CHECK CONSTRAINT [FK_EmployeeBonusPointRequests_Employees_Employee_EmployeeId]
GO
/****** Object:  ForeignKey [FK_EmployeeBonusPointRequests_Users_LastUpdatedByUserId]    Script Date: 04/10/2012 07:08:38 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_EmployeeBonusPointRequests_Users_LastUpdatedByUserId]') AND parent_object_id = OBJECT_ID(N'[dbo].[EmployeeBonusPointRequests]'))
ALTER TABLE [dbo].[EmployeeBonusPointRequests]  WITH CHECK ADD  CONSTRAINT [FK_EmployeeBonusPointRequests_Users_LastUpdatedByUserId] FOREIGN KEY([LastUpdatedByUserId])
REFERENCES [dbo].[Users] ([UserId])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_EmployeeBonusPointRequests_Users_LastUpdatedByUserId]') AND parent_object_id = OBJECT_ID(N'[dbo].[EmployeeBonusPointRequests]'))
ALTER TABLE [dbo].[EmployeeBonusPointRequests] CHECK CONSTRAINT [FK_EmployeeBonusPointRequests_Users_LastUpdatedByUserId]
GO
/****** Object:  ForeignKey [FK_EmployeeBonusPoints_Employees_Employee_EmployeeId]    Script Date: 04/10/2012 07:08:38 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_EmployeeBonusPoints_Employees_Employee_EmployeeId]') AND parent_object_id = OBJECT_ID(N'[dbo].[EmployeeBonusPoints]'))
ALTER TABLE [dbo].[EmployeeBonusPoints]  WITH CHECK ADD  CONSTRAINT [FK_EmployeeBonusPoints_Employees_Employee_EmployeeId] FOREIGN KEY([Employee_EmployeeId])
REFERENCES [dbo].[Employees] ([EmployeeId])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_EmployeeBonusPoints_Employees_Employee_EmployeeId]') AND parent_object_id = OBJECT_ID(N'[dbo].[EmployeeBonusPoints]'))
ALTER TABLE [dbo].[EmployeeBonusPoints] CHECK CONSTRAINT [FK_EmployeeBonusPoints_Employees_Employee_EmployeeId]
GO
/****** Object:  ForeignKey [FK_EmployeeBonusPoints_Users_LastUpdatedByUserId]    Script Date: 04/10/2012 07:08:38 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_EmployeeBonusPoints_Users_LastUpdatedByUserId]') AND parent_object_id = OBJECT_ID(N'[dbo].[EmployeeBonusPoints]'))
ALTER TABLE [dbo].[EmployeeBonusPoints]  WITH CHECK ADD  CONSTRAINT [FK_EmployeeBonusPoints_Users_LastUpdatedByUserId] FOREIGN KEY([LastUpdatedByUserId])
REFERENCES [dbo].[Users] ([UserId])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_EmployeeBonusPoints_Users_LastUpdatedByUserId]') AND parent_object_id = OBJECT_ID(N'[dbo].[EmployeeBonusPoints]'))
ALTER TABLE [dbo].[EmployeeBonusPoints] CHECK CONSTRAINT [FK_EmployeeBonusPoints_Users_LastUpdatedByUserId]
GO
/****** Object:  ForeignKey [FK_Employees_Departments_DepartmentId]    Script Date: 04/10/2012 07:08:38 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Employees_Departments_DepartmentId]') AND parent_object_id = OBJECT_ID(N'[dbo].[Employees]'))
ALTER TABLE [dbo].[Employees]  WITH CHECK ADD  CONSTRAINT [FK_Employees_Departments_DepartmentId] FOREIGN KEY([DepartmentId])
REFERENCES [dbo].[Departments] ([DepartmentId])
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Employees_Departments_DepartmentId]') AND parent_object_id = OBJECT_ID(N'[dbo].[Employees]'))
ALTER TABLE [dbo].[Employees] CHECK CONSTRAINT [FK_Employees_Departments_DepartmentId]
GO
/****** Object:  ForeignKey [FK_Employees_Employees_BusinessUnitHeadId]    Script Date: 04/10/2012 07:08:38 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Employees_Employees_BusinessUnitHeadId]') AND parent_object_id = OBJECT_ID(N'[dbo].[Employees]'))
ALTER TABLE [dbo].[Employees]  WITH CHECK ADD  CONSTRAINT [FK_Employees_Employees_BusinessUnitHeadId] FOREIGN KEY([BusinessUnitHeadId])
REFERENCES [dbo].[Employees] ([EmployeeId])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Employees_Employees_BusinessUnitHeadId]') AND parent_object_id = OBJECT_ID(N'[dbo].[Employees]'))
ALTER TABLE [dbo].[Employees] CHECK CONSTRAINT [FK_Employees_Employees_BusinessUnitHeadId]
GO
/****** Object:  ForeignKey [FK_Employees_Employees_ImmediateSupervisorId]    Script Date: 04/10/2012 07:08:38 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Employees_Employees_ImmediateSupervisorId]') AND parent_object_id = OBJECT_ID(N'[dbo].[Employees]'))
ALTER TABLE [dbo].[Employees]  WITH CHECK ADD  CONSTRAINT [FK_Employees_Employees_ImmediateSupervisorId] FOREIGN KEY([ImmediateSupervisorId])
REFERENCES [dbo].[Employees] ([EmployeeId])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Employees_Employees_ImmediateSupervisorId]') AND parent_object_id = OBJECT_ID(N'[dbo].[Employees]'))
ALTER TABLE [dbo].[Employees] CHECK CONSTRAINT [FK_Employees_Employees_ImmediateSupervisorId]
GO
/****** Object:  ForeignKey [FK_Employees_Positions_PositionId]    Script Date: 04/10/2012 07:08:38 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Employees_Positions_PositionId]') AND parent_object_id = OBJECT_ID(N'[dbo].[Employees]'))
ALTER TABLE [dbo].[Employees]  WITH CHECK ADD  CONSTRAINT [FK_Employees_Positions_PositionId] FOREIGN KEY([PositionId])
REFERENCES [dbo].[Positions] ([PositionId])
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Employees_Positions_PositionId]') AND parent_object_id = OBJECT_ID(N'[dbo].[Employees]'))
ALTER TABLE [dbo].[Employees] CHECK CONSTRAINT [FK_Employees_Positions_PositionId]
GO
/****** Object:  ForeignKey [FK_Employees_Users_LastUpdatedByUserId]    Script Date: 04/10/2012 07:08:38 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Employees_Users_LastUpdatedByUserId]') AND parent_object_id = OBJECT_ID(N'[dbo].[Employees]'))
ALTER TABLE [dbo].[Employees]  WITH CHECK ADD  CONSTRAINT [FK_Employees_Users_LastUpdatedByUserId] FOREIGN KEY([LastUpdatedByUserId])
REFERENCES [dbo].[Users] ([UserId])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Employees_Users_LastUpdatedByUserId]') AND parent_object_id = OBJECT_ID(N'[dbo].[Employees]'))
ALTER TABLE [dbo].[Employees] CHECK CONSTRAINT [FK_Employees_Users_LastUpdatedByUserId]
GO
/****** Object:  ForeignKey [FK_Employees_Users_UserId]    Script Date: 04/10/2012 07:08:38 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Employees_Users_UserId]') AND parent_object_id = OBJECT_ID(N'[dbo].[Employees]'))
ALTER TABLE [dbo].[Employees]  WITH CHECK ADD  CONSTRAINT [FK_Employees_Users_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[Users] ([UserId])
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Employees_Users_UserId]') AND parent_object_id = OBJECT_ID(N'[dbo].[Employees]'))
ALTER TABLE [dbo].[Employees] CHECK CONSTRAINT [FK_Employees_Users_UserId]
GO
/****** Object:  ForeignKey [FK_Positions_Users_LastUpdatedByUserId]    Script Date: 04/10/2012 07:08:38 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Positions_Users_LastUpdatedByUserId]') AND parent_object_id = OBJECT_ID(N'[dbo].[Positions]'))
ALTER TABLE [dbo].[Positions]  WITH CHECK ADD  CONSTRAINT [FK_Positions_Users_LastUpdatedByUserId] FOREIGN KEY([LastUpdatedByUserId])
REFERENCES [dbo].[Users] ([UserId])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Positions_Users_LastUpdatedByUserId]') AND parent_object_id = OBJECT_ID(N'[dbo].[Positions]'))
ALTER TABLE [dbo].[Positions] CHECK CONSTRAINT [FK_Positions_Users_LastUpdatedByUserId]
GO
/****** Object:  ForeignKey [FK_Users_Users_LastUpdatedByUserId]    Script Date: 04/10/2012 07:08:38 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Users_Users_LastUpdatedByUserId]') AND parent_object_id = OBJECT_ID(N'[dbo].[Users]'))
ALTER TABLE [dbo].[Users]  WITH CHECK ADD  CONSTRAINT [FK_Users_Users_LastUpdatedByUserId] FOREIGN KEY([LastUpdatedByUserId])
REFERENCES [dbo].[Users] ([UserId])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Users_Users_LastUpdatedByUserId]') AND parent_object_id = OBJECT_ID(N'[dbo].[Users]'))
ALTER TABLE [dbo].[Users] CHECK CONSTRAINT [FK_Users_Users_LastUpdatedByUserId]
GO
