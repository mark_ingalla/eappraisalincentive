/****** Object:  Table [dbo].[Users]    Script Date: 03/25/2012 15:02:54 ******/
SET IDENTITY_INSERT [dbo].[Users] ON
INSERT [dbo].[Users] ([UserId], [Username], [Password], [Role], [LastUpdatedByUserId], [LastUpdatedDate]) VALUES (1, N'demo', N'demodemo', 1, NULL, CAST(0x0000A01500B65FC4 AS DateTime))
INSERT [dbo].[Users] ([UserId], [Username], [Password], [Role], [LastUpdatedByUserId], [LastUpdatedDate]) VALUES (2, N'demomanager', N'demodemo', 2, NULL, CAST(0x0000A01F00F6CC83 AS DateTime))
INSERT [dbo].[Users] ([UserId], [Username], [Password], [Role], [LastUpdatedByUserId], [LastUpdatedDate]) VALUES (3, N'demouser', N'demodemo', 3, NULL, CAST(0x0000A01F00F75012 AS DateTime))
INSERT [dbo].[Users] ([UserId], [Username], [Password], [Role], [LastUpdatedByUserId], [LastUpdatedDate]) VALUES (4, N'mdingalla', N'passwordon', 3, NULL, CAST(0x0000A019004D130B AS DateTime))
SET IDENTITY_INSERT [dbo].[Users] OFF
/****** Object:  Table [dbo].[Positions]    Script Date: 03/25/2012 15:02:54 ******/
SET IDENTITY_INSERT [dbo].[Positions] ON
INSERT [dbo].[Positions] ([PositionId], [Name], [LastUpdatedByUserId], [LastUpdatedDate]) VALUES (1, N'Hiring Manager', NULL, CAST(0x0000A01500B4492F AS DateTime))
SET IDENTITY_INSERT [dbo].[Positions] OFF
/****** Object:  Table [dbo].[AppraisalHeaders]    Script Date: 03/25/2012 15:02:54 ******/
SET IDENTITY_INSERT [dbo].[AppraisalHeaders] ON
INSERT [dbo].[AppraisalHeaders] ([AppraisalHeaderId], [Name], [LastUpdatedByUserId], [LastUpdatedDate],[IsActive]) VALUES (1, N'Work Competency', NULL, CAST(0x0000A01500C6A71F AS DateTime),'True')
INSERT [dbo].[AppraisalHeaders] ([AppraisalHeaderId], [Name], [LastUpdatedByUserId], [LastUpdatedDate],[IsActive]) VALUES (2, N'Service Focus', NULL, CAST(0x0000A01500C8198E AS DateTime),'True')
SET IDENTITY_INSERT [dbo].[AppraisalHeaders] OFF
/****** Object:  Table [dbo].[Departments]    Script Date: 03/25/2012 15:02:54 ******/
SET IDENTITY_INSERT [dbo].[Departments] ON
INSERT [dbo].[Departments] ([DepartmentId], [Name], [LastUpdatedByUserId], [LastUpdatedDate]) VALUES (1, N'Accounting', NULL, CAST(0x0000A01500B4531D AS DateTime))
INSERT [dbo].[Departments] ([DepartmentId], [Name], [LastUpdatedByUserId], [LastUpdatedDate]) VALUES (2, N'Human Resource', NULL, CAST(0x0000A01500B4590A AS DateTime))
SET IDENTITY_INSERT [dbo].[Departments] OFF
/****** Object:  Table [dbo].[AppraisalItems]    Script Date: 03/25/2012 15:02:54 ******/
SET IDENTITY_INSERT [dbo].[AppraisalItems] ON
INSERT [dbo].[AppraisalItems] ([AppraisalItemId], [Description], [AppraisalHeaderId], [LastUpdatedByUserId], [LastUpdatedDate],[IsActive]) VALUES (1, N'Job Knowledge', 1, NULL, CAST(0x0000A01500C7F3FD AS DateTime),'True')
INSERT [dbo].[AppraisalItems] ([AppraisalItemId], [Description], [AppraisalHeaderId], [LastUpdatedByUserId], [LastUpdatedDate],[IsActive]) VALUES (2, N'Actively Attends to needs of customers', 2, NULL, CAST(0x0000A01500C83678 AS DateTime),'True')
SET IDENTITY_INSERT [dbo].[AppraisalItems] OFF
/****** Object:  Table [dbo].[Employees]    Script Date: 03/25/2012 15:02:54 ******/
SET IDENTITY_INSERT [dbo].[Employees] ON
INSERT [dbo].[Employees] ([EmployeeId], [UserId], [EmployeeNo], [EmployeeName], [PositionId], [DepartmentId], [DateJoined], [DateResigned], [EmailAddress], [Remarks], [ImmediateSupervisorId], [BusinessUnitHeadId], [BasicSalary], [Grade], [LastUpdatedByUserId], [LastUpdatedDate],[IsActive]) VALUES (1, 1, N'000', N'Demo Demo', 1, 2, CAST(0x0000A01500000000 AS DateTime), NULL, N'demo@demo.com', N'demo account only', NULL, NULL, CAST(200.00 AS Decimal(18, 2)), NULL, NULL, CAST(0x0000A01500B6701D AS DateTime),'True')
INSERT [dbo].[Employees] ([EmployeeId], [UserId], [EmployeeNo], [EmployeeName], [PositionId], [DepartmentId], [DateJoined], [DateResigned], [EmailAddress], [Remarks], [ImmediateSupervisorId], [BusinessUnitHeadId], [BasicSalary], [Grade], [LastUpdatedByUserId], [LastUpdatedDate],[IsActive]) VALUES (2, 2, N'222', N'Manager Demo', 1, 1, CAST(0x0000A00300000000 AS DateTime), NULL, N'demo@demo.com', NULL, 1, 1, CAST(0.00 AS Decimal(18, 2)), N'A', NULL, CAST(0x0000A01F00F6CC83 AS DateTime),'True')
INSERT [dbo].[Employees] ([EmployeeId], [UserId], [EmployeeNo], [EmployeeName], [PositionId], [DepartmentId], [DateJoined], [DateResigned], [EmailAddress], [Remarks], [ImmediateSupervisorId], [BusinessUnitHeadId], [BasicSalary], [Grade], [LastUpdatedByUserId], [LastUpdatedDate],[IsActive]) VALUES (3, 3, N'333', N'User Demo', 1, 2, CAST(0x00009E5A00000000 AS DateTime), NULL, N'demo@demo.com', NULL, 1, 1, CAST(0.00 AS Decimal(18, 2)), N'A', NULL, CAST(0x0000A01F00F75012 AS DateTime),'True')
INSERT [dbo].[Employees] ([EmployeeId], [UserId], [EmployeeNo], [EmployeeName], [PositionId], [DepartmentId], [DateJoined], [DateResigned], [EmailAddress], [Remarks], [ImmediateSupervisorId], [BusinessUnitHeadId], [BasicSalary], [Grade], [LastUpdatedByUserId], [LastUpdatedDate],[IsActive]) VALUES (4, 4, N'123', N'Mark Dave Ingalla', 1, 1, CAST(0x00009EAB00000000 AS DateTime), NULL, N'mdingalla@gmail.com', NULL, 2, 1, CAST(0.00 AS Decimal(18, 2)), NULL, NULL, CAST(0x0000A019004D130B AS DateTime),'True')
SET IDENTITY_INSERT [dbo].[Employees] OFF
/****** Object:  Table [dbo].[EmployeeBonusPoints]    Script Date: 03/25/2012 15:02:54 ******/
/****** Object:  Table [dbo].[EmployeeBonusPointRequests]    Script Date: 03/25/2012 15:02:54 ******/
/****** Object:  Table [dbo].[EmployeeAppraisals]    Script Date: 03/25/2012 15:02:54 ******/
/****** Object:  Table [dbo].[EmployeeAppraisalHeaders]    Script Date: 03/25/2012 15:02:54 ******/
/****** Object:  Table [dbo].[EmployeeAppraisalItems]    Script Date: 03/25/2012 15:02:54 ******/
